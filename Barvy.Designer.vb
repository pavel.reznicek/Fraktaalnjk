<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class Barvy
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents _picBarva_0 As System.Windows.Forms.PictureBox
	Public CDColor As System.Windows.Forms.ColorDialog
	Friend WithEvents picBarva As Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me._picBarva_0 = New System.Windows.Forms.PictureBox
        Me.CDColor = New System.Windows.Forms.ColorDialog
        Me.picBarva = New Microsoft.VisualBasic.Compatibility.VB6.PictureBoxArray(Me.components)
        CType(Me._picBarva_0, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picBarva, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_picBarva_0
        '
        Me._picBarva_0.BackColor = System.Drawing.SystemColors.Window
        Me._picBarva_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._picBarva_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.picBarva.SetIndex(Me._picBarva_0, CType(0, Short))
        Me._picBarva_0.Location = New System.Drawing.Point(32, 16)
        Me._picBarva_0.Name = "_picBarva_0"
        Me._picBarva_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._picBarva_0.Size = New System.Drawing.Size(89, 41)
        Me._picBarva_0.TabIndex = 0
        '
        'picBarva
        '
        '
        'Barvy
        '
        Me.AutoScroll = True
        Me.Controls.Add(Me._picBarva_0)
        Me.Name = "Barvy"
        Me.Size = New System.Drawing.Size(320, 240)
        CType(Me._picBarva_0, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picBarva, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
#End Region 

End Class