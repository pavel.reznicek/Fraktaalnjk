Option Strict Off
Option Explicit On
Friend Class CComplexNumber
	
	'------------------------------------------
	' A class for dealing with complex numbers
	'------------------------------------------
	
	' The main properties
	Public Real As Double
	Public Imaginary As Double
	
	' Initialize this complex number
	' (returns Me)
	
	Function Init(ByRef Real As Double, ByRef Imaginary As Double) As CComplexNumber
		Me.Real = Real
		Me.Imaginary = Imaginary
		Init = Me
	End Function
	
	' Returns another complex number with given
	' real and imaginary parts
	
	Function Complex(ByRef Real As Double, ByRef Imaginary As Double) As CComplexNumber
		Complex = New CComplexNumber
		Complex.Real = Real
		Complex.Imaginary = Imaginary
	End Function
	
	' Add a complex number to the current one
	' (returns Me)
	
	Function Add(ByRef Other As CComplexNumber) As CComplexNumber
		Real = Real + Other.Real
		Imaginary = Imaginary + Other.Imaginary
		Add = Me
	End Function
	
	' Add a complex number to the current one
	' (returns the result CComplexNumber)
	
	Function Add2(ByRef Real As Double, ByRef Imaginary As Double) As CComplexNumber
		Add2 = New CComplexNumber
		Add2.Real = Me.Real + Real
		Add2.Imaginary = Me.Imaginary + Imaginary
	End Function
	
	Function Subtract(ByRef Other As CComplexNumber) As CComplexNumber
		Real = Real - Other.Real
		Imaginary = Imaginary - Other.Imaginary
		Subtract = Me
	End Function
	
	' Subtract a complex number from the current one
	' (returns the result CComplexNumber)
	
	Function Subtract2(ByRef Real As Double, ByRef Imaginary As Double) As CComplexNumber
		Subtract2 = New CComplexNumber
		Subtract2.Real = Me.Real - Real
		Subtract2.Imaginary = Me.Imaginary - Imaginary
	End Function
	
	
	Function Multiply(ByRef Other As CComplexNumber) As CComplexNumber
		Dim RO, R, I, IO As Double
		R = Me.Real
		I = Me.Imaginary
		RO = Other.Real
		IO = Other.Imaginary
		Real = (R * RO - I * IO)
		Imaginary = R * IO + I * RO
		Multiply = Me
	End Function
	
	' Multiple a complex number by the current one
	' (returns the result CComplexNumber)
	
	Function Multiply2(ByRef Real As Double, ByRef Imaginary As Double) As CComplexNumber
		Multiply2 = New CComplexNumber
		Multiply2.Real = (Me.Real * Real - Me.Imaginary * Imaginary)
		Multiply2.Imaginary = Me.Real * Imaginary + Me.Imaginary * Real
	End Function
	
	Function Divide(ByRef Other As CComplexNumber) As CComplexNumber
		Dim sum As Double
		' create the sum only once
		sum = Other.Real * Other.Real + Other.Imaginary * Other.Imaginary
		' evaluate the real and imaginary parts
		Me.Real = (Me.Real * Other.Real + Me.Imaginary * Other.Imaginary) / sum
		Me.Imaginary = (Me.Imaginary * Other.Real - Me.Real * Other.Imaginary) / sum
		Divide = Me
	End Function
	
	' Divide the current number by another complex number
	' (returns the result CComplexNumber)
	
	Function Divide2(ByRef Real As Double, ByRef Imaginary As Double) As CComplexNumber
		Divide2 = New CComplexNumber
		Dim sum As Double
		' create the sum only once
		sum = Real * Real + Imaginary * Imaginary
		' evaluate the real and imaginary parts
		Divide2.Real = (Me.Real * Real + Me.Imaginary * Imaginary) / sum
		Divide2.Imaginary = (Me.Imaginary * Real - Me.Real * Imaginary) / sum
	End Function
	
	' Return the textual description of a complex number
	
	Function Text() As String
		If Imaginary = 0 Then
			Text = LTrim(CStr(Real))
		ElseIf Real = 0 Then 
			Text = LTrim(CStr(Imaginary)) & "i"
		ElseIf Imaginary > 0 Then 
			Text = LTrim(CStr(Real)) & "+" & LTrim(CStr(Imaginary)) & "i"
		Else
			Text = LTrim(CStr(Real)) & LTrim(CStr(Imaginary)) & "i"
		End If
	End Function
	
	
	ReadOnly Property Absol() As Object
		Get
			'UPGRADE_WARNING: Couldn't resolve default property of object Absol. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			Absol = (Real ^ 2 + Imaginary ^ 2) ^ (1 / 2)
		End Get
	End Property
End Class