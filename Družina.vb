Option Strict Off
Option Explicit On
Friend Class Dru�ina
	Implements System.Collections.IEnumerable
	
	'local variable to hold collection
	Private mCol As Collection
    Public Function P�idej(ByVal Nov�Bod As TTrpasl�k, Optional ByRef sKey As String = "") As TTrpasl�k
        'create a new object
        Dim objNewMember As TTrpasl�k
        objNewMember = Nov�Bod


        'set the properties passed into the method
        '    objNewMember.Barva = Barva
        '    objNewMember.� = �
        '    objNewMember.Z = Z
        '    objNewMember.M = M
        '    Set objNewMember.Soused� = Soused�
        '    objNewMember.X = X
        '    objNewMember.Y = Y
        '    Set objNewMember.�ik = �ik
        '    Set objNewMember.Pic = Pic




        If Len(sKey) = 0 Then
            mCol.Add(objNewMember)
        Else
            mCol.Add(objNewMember, sKey)
        End If


        'return the object created
        P�idej = objNewMember
        objNewMember = Nothing


    End Function

    Default Public ReadOnly Property �len(ByVal vntIndexKey As Object) As TTrpasl�k
        Get
            'used when referencing an element in the collection
            'vntIndexKey contains either the Index or Key to the collection,
            'this is why it is declared as a Variant
            'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
            �len = mCol.Item(vntIndexKey)
        End Get
    End Property



    Public ReadOnly Property Po�et() As Integer
        Get
            'used when retrieving the number of elements in the
            'collection. Syntax: Debug.Print x.Count
            Po�et = mCol.Count()
        End Get
    End Property


    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return mCol.GetEnumerator
    End Function

    Public ReadOnly Property Vrstva(ByVal ��slo As Object) As Dru�ina
        Get
            Dim Trp As TTrpasl�k
            Vrstva = New Dru�ina
            For Each Trp In mCol
                If Trp.Vrstva = ��slo Then Vrstva.P�idej(Trp)
            Next Trp
        End Get
    End Property


    Public Sub Remove(ByRef vntIndexKey As Object)
        'used when removing an element from the collection
        'vntIndexKey contains either the Index or Key, which is why
        'it is declared as a Variant
        'Syntax: x.Remove(xyz)


        mCol.Remove(vntIndexKey)
    End Sub

    Public Sub New()
        MyBase.New()
        mCol = New Collection
    End Sub

    Protected Overrides Sub Finalize()
        mCol = Nothing
        System.Windows.Forms.Application.DoEvents()
        MyBase.Finalize()
    End Sub


    Public Sub NakresliV�echny()
        Dim Trp As TTrpasl�k
        For Each Trp In mCol
            'TODO: Nakreslit v�echny trpasl�ky!
        Next
    End Sub

    Public Sub P�idru�(ByVal Jin�Dru�ina As Dru�ina)
        Dim Trp As TTrpasl�k
        For Each Trp In Jin�Dru�ina
            mCol.Add(Trp)
        Next
    End Sub

    Public ReadOnly Property Obsahuje(ByVal Trp As TTrpasl�k) As Boolean
        Get
            Dim T As Object
            For Each T In mCol
                If T Is Trp Then
                    Return True
                End If
            Next
            Return False
        End Get
    End Property

End Class