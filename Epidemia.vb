Option Strict Off
Option Explicit On
Friend Class Epidemia
	
    Public Mutace As modSP.TypP�enosu
	Public Soused� As modSP.TypSoused�
	Public Po�ad�Sous As modSP.Po�ad�Soused�
    Public Po�etSous As Short
    Public SmRozptyl As modSP.TSmRozptyl
    Public PovolitP�ekr�v�n� As Boolean
    Public Po�Oh, HodOh As Object
    Public N�hHodOh As Boolean
    Public Po�etVrstev As Integer
    Public Vy�istit As Boolean
    Public Dla�dice As Boolean
    'Public VyhnoutSeNakreslen�m As Boolean


    Public Sm�rov� As Boolean
    Public Sm�ry As TOsmism�r()

    Public Linia As Boolean
    Public LinX As Object
    Public LinY As Object
    Public P�idatR�m As Boolean

    Public VracetJenPoslVr As Boolean

    Public Nav�zatNaMinulou As Boolean

    Dim pamOhniska As New Dru�ina
    Dim pamNaka�en� As New Dru�ina

    Dim Sous As TTrpasl�k
    Dim N�h As New GenerN
    Dim N�hX As New GenerN
    Dim N�hY As New GenerN
    Dim KolSous As New Dru�ina
    Dim StVrstva As New Dru�ina
    Dim NoVrstva As New Dru�ina
    Dim Trpasl�k As TTrpasl�k
    Dim EpHod As Double
    Dim ��tVr As Object
    Private mvarKolEp As KolEp






    Public Property KolEp() As KolEp
        Get
            If mvarKolEp Is Nothing Then
                mvarKolEp = New KolEp
            End If

            KolEp = mvarKolEp
        End Get
        Set(ByVal Value As KolEp)
            mvarKolEp = Value
        End Set
    End Property




    Public Property Ohniska() As Dru�ina
        Get
            Ohniska = pamOhniska
        End Get
        Set(ByVal Value As Dru�ina)
            If IsReference(Value) Then
                pamOhniska = Value
            Else
                pamOhniska = Value
            End If
        End Set
    End Property




    Public Property Naka�en�() As Dru�ina
        Get
            Naka�en� = pamNaka�en�
        End Get
        Set(ByVal Value As Dru�ina)
            pamNaka�en� = Value
        End Set
    End Property

    ReadOnly Property �PoslVrstvy() As Integer
        Get
            �PoslVrstvy = ��tVr
        End Get
    End Property

    ReadOnly Property PoslVrstva() As Dru�ina
        Get
            If Naka�en� IsNot Nothing Then
                PoslVrstva = Naka�en�.Vrstva(�PoslVrstvy - 1)
            Else
                PoslVrstva = New Dru�ina
            End If
        End Get
    End Property

    Protected Overrides Sub Finalize()
        mvarKolEp = Nothing
        MyBase.Finalize()
    End Sub




    Sub P�edp��prava()

        Dim Vy�istitMapu As Boolean = modSP.�istit And Me.Vy�istit
        Dim NechatStarouMapu As Boolean = Not Vy�istitMapu

        P�ipravMapu(NechatStarou:=NechatStarouMapu)

        �ik.Pic.Cursor = System.Windows.Forms.Cursors.WaitCursor


    End Sub


    Sub P��prava()

        If P�eru�it Then
            Exit Sub
        End If

        P�edp��prava()

        Naka�en� = New Dru�ina

        If P�idatR�m Then
            �ik.R�m.NakresliV�echny()
            If Ohniska Is Nothing Then Ohniska = New Dru�ina
            Ohniska.P�idru�(�ik.R�m)
        End If

        �ik.Dla�dice = Dla�dice

        NoVrstva = Ohniska
        Dim pamPo�Oh, pamMaxHod As Object
        Dim I As Integer
        If NoVrstva Is Nothing OrElse (NoVrstva.Po�et = 0) OrElse P�idatR�m Then
            ' v�roby n�hodn�ho po�tu ohnisek
            N�h.Smallest = 1
            N�h.Largest = 50
            pamPo�Oh = IIf(IsNothing(Po�Oh), N�h.Value, Po�Oh)
            ' nastaven� gener�toru hodnoty
            pamMaxHod = IIf(IsNothing(HodOh), 400, HodOh)
            N�h.Smallest = -pamMaxHod
            N�h.Largest = pamMaxHod
            ' nastaven� gener�tor� polohy
            N�hX.Largest = �ik.���ka - 1
            N�hY.Largest = �ik.V��ka - 1
            If NoVrstva Is Nothing Then NoVrstva = New Dru�ina
            For I = 1 To pamPo�Oh
                Trpasl�k = �ik(N�hX.Value, N�hY.Value)
                NoVrstva.P�idej(Trpasl�k)
                Trpasl�k.Hodnota = IIf(N�hHodOh, N�h.Value, HodOh)
                Trpasl�k.Barva = EpFas(Trpasl�k.Hodnota)
                Trpasl�k.Nakreslen = True
                Trpasl�k.Vrstva = 0
                Naka�en�(Trpasl�k)
            Next
        Else
            For I = 1 To NoVrstva.Po�et
                Trpasl�k = NoVrstva(I)
                If Not Trpasl�k Is Nothing Then
                    Trpasl�k.Nakreslen = True
                    Trpasl�k.Vrstva = 0
                    Naka�en�(Trpasl�k)
                End If
            Next
        End If
        For Each Trpasl�k In NoVrstva
            Dim St�ela As TOsmism�r = N�hoda(1, 2, 3, 4, 5, 6, 7, 8)
            If SmRozptyl < 0 Then
                Trpasl�k.P�vSmEp = St�ela
            Else
                Trpasl�k.SmEp = St�ela
            End If
        Next
        ��tVr = 1
    End Sub


    Sub Spus�()
        If Linia Then
            Spus�Linii()
            Exit Sub
        End If
        P��prava()
        Do
            Vrstva()
            If ��tVr Mod 50 = 0 Then Z�loha()
        Loop Until NoVrstva Is Nothing _
            OrElse NoVrstva.Po�et = 0 _
            OrElse (Po�etVrstev > 0 AndAlso ��tVr >= Po�etVrstev) _
            OrElse P�eru�it
        If VracetJenPoslVr Then Naka�en� = NoVrstva
        NoVrstva = Nothing
        StVrstva = Nothing
    End Sub

    Sub Vrstva()
        StVrstva = NoVrstva
        NoVrstva = New Dru�ina

        ��tVr += 1

        Dim I As Integer
        For I = 1 To StVrstva.Po�et
            Trpasl�k = StVrstva(I)
            Sousedstv�()
            If P�eru�it Then Exit For
        Next
        �ik.Pic.Refresh()
        If ��tVr Mod 16 = 0 Then System.Windows.Forms.Application.DoEvents()
    End Sub

    Sub Sousedstv�()
        If Trpasl�k Is Nothing Then Exit Sub
        �ik.Dla�dice = Me.Dla�dice
        If Not Sm�rov� Then
            KolSous = Trpasl�k.Soused�(Po�ad�Sous, Soused�, Po�etSous, SmRozptyl, PovolitP�ekr�v�n�)
        Else
            KolSous = Trpasl�k.Soused�ZeSm�r�(Po�ad�Sous, Po�etSous, PovolitP�ekr�v�n�, Sm�ry)
        End If
        EpHod = Trpasl�k.Hodnota
        Static ��tSous As Integer
        ��tSous = ��tSous + 1
        If ��tSous Mod 16 = 0 Then
            System.Windows.Forms.Application.DoEvents()
            ��tSous = 0
        End If
        Dim J As Integer
        For J = 1 To KolSous.Po�et
            Sous = KolSous(J)
            If Not Sous Is Nothing Then
                If (Not Sous.Nakreslen) Or PovolitP�ekr�v�n� Then
                    If NoVrstva Is Nothing Then NoVrstva = New Dru�ina
                    NoVrstva.P�idej(Sous)
                    N�h.Smallest = -25
                    N�h.Largest = 25
                    Select Case Mutace
                        Case modSP.TypP�enosu.tpN�hodn�Zm�na
                            Sous.Hodnota = EpHod + N�h.Value
                        Case modSP.TypP�enosu.tpP��r�stek
                            Sous.Hodnota = EpHod + 1
                        Case modSP.TypP�enosu.tpV�t��P��r�stek
                            Sous.Hodnota = EpHod + 5
                        Case modSP.TypP�enosu.tpRostouc�P��r�stek
                            Sous.Hodnota = EpHod + EpHod
                        Case modSP.TypP�enosu.tpBezeZm�ny
                            Sous.Hodnota = EpHod
                        Case Else
                            ' nech�me n�hodnou zm�nu
                            Sous.Hodnota = EpHod + N�h.Value
                    End Select
                    Sous.Barva = EpFas(EpHod)
                    Sous.Nakreslen = True
                    Sous.Vrstva = Trpasl�k.Vrstva + 1
                    Naka�en�(Sous)
                End If
            End If
        Next
    End Sub

    Function EpFas(ByRef Hod As Double) As Object
        EpFas = Fasis(Hod, 1, Color.White, Color.Blue) ', Color.Green, Color.Blue)
    End Function


    Sub Naka�en�(ByRef Trp As TTrpasl�k)
        If Not VracetJenPoslVr Then Naka�en�.P�idej(Trp)
    End Sub



    Sub Spus�Linii()

        If P�eru�it Then Exit Sub

        �ik.Dla�dice = Me.Dla�dice

        Dim Hodnota, X, Y, D�lka As Object

        X = LinX
        Y = LinY
        Hodnota = HodOh
        D�lka = Po�etVrstev

        Dim StBod As TTrpasl�k
        Dim NoBod As TTrpasl�k
        Dim N�h As New GenerN
        Dim N�hX As New GenerN
        Dim N�hY As New GenerN
        Dim I, J As Integer
        Dim �sp�ch As Boolean
        Dim St�ela As Object
        Dim KolSous As Dru�ina
        Dim Trpasl�k As TTrpasl�k
        P�ipravMapu(Not frmV�b�r.chbVy�istit.Checked)
        Naka�en� = New Dru�ina
        N�hX.Largest = �ik.���ka
        N�hY.Largest = �ik.V��ka
        If IsNothing(X) Then X = N�hX.Value
        If IsNothing(Y) Then Y = N�hY.Value
        NoBod = �ik(X, Y)
        If NoBod Is Nothing Then
            MsgBox("Za��tek linie je mimo plochu!", MsgBoxStyle.Exclamation, "Nejdu d�l")
            P�eru�it = True
            Exit Sub
        End If
        N�h.Smallest = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black)
        N�h.Largest = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White)
        NoBod.Barva = N�h.Value
        N�h.Smallest = 1
        N�h.Largest = 8
        For I = 1 To D�lka
            StBod = NoBod
            NoBod = Nothing
znovu:
            KolSous = StBod.Soused�(modSP.Po�ad�Soused�.psN�hodn�, modSP.TypSoused�.tsV�ichni, 0, TSmRozptyl.sr360, PovolitP�ekr�v�n�)
            �sp�ch = False
            For J = 1 To KolSous.Po�et
                Trpasl�k = KolSous(J)
                �sp�ch = False
                St�ela = N�h.Value
                '            If Not Trpasl�k Is Nothing Then
                If Not Trpasl�k.Nakreslen Then
                    NoBod = Trpasl�k
                    '                    NoBod.SmEp = J
                    �sp�ch = True
                    Exit For
                End If
                '            Else
                '                GoTo znovu
                '            End If
            Next
            If Not �sp�ch Then
                KolSous = StBod.Soused�(modSP.Po�ad�Soused�.psN�hodn�, modSP.TypSoused�.tsV�ichni, modSP.V�b�rSoused�.vsV�ichni, TSmRozptyl.srP�v360, True)
                If KolSous.Po�et > 0 Then
                    KolSous.�len(1).Nakreslen = False
                    GoTo znovu
                Else ' jsme na kraji, nem��eme d�l
                    P�eru�it = True
                End If
            End If
            'Debug.Assert �sp�ch
            If NoBod IsNot Nothing Then
                NoBod.Hodnota = StBod.Hodnota
                NoBod.P�vSmEp = StBod.P�vSmEp
                'NoBod.Barva = EpFas(NoBod.Hodnota)
                NoBod.Barva = StBod.Barva
                NoBod.Nakreslen = True
                Naka�en�.P�idej(NoBod)
            End If
            �ik.Pic.Refresh()
            If I Mod 16 = 0 Then System.Windows.Forms.Application.DoEvents()
            If P�eru�it Then
                'P�eru�it = False
                Exit For
            End If
        Next
    End Sub

    Public Sub New()
        MyBase.New()
        Po�ad�Sous = modSP.Po�ad�Soused�.psN�hodn�
        SmRozptyl = modSP.TSmRozptyl.sr360
        N�hHodOh = True
        Vy�istit = True
        'VyhnoutSeNakreslen�m = True ' False
        VracetJenPoslVr = True
    End Sub
End Class