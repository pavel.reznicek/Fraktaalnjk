Option Strict Off
Option Explicit On
Friend Class GenerN
	
	' Deklarace vlastnost�, kter� budou ur�ovat rozsah
	Public Smallest As Integer
	Public Largest As Integer
	
	' V�sledn� pseudon�hodn� ��slo
	Public ReadOnly Property Value() As Object
		Get
            Value = Int(Rnd() * (Largest - Smallest + 1)) + Smallest
		End Get
	End Property
	
    Public Sub New()
        MyBase.New()
        ' Inicialisace n�hodn� posloupnosti
        Randomize()
    End Sub
End Class