Option Strict Off
Option Explicit On
Friend Class KolEp
	Implements System.Collections.IEnumerable
	
	'local variable to hold collection
	Private mCol As Collection
	Public Function Add(ByRef Ep As Epidemia, Optional ByRef sKey As String = "") As Epidemia
		'create a new object
		Dim objNewMember As Epidemia
		'Set objNewMember = New Epidemia
		objNewMember = Ep
		
		
		'set the properties passed into the method
		'    Set objNewMember.Ohniska = Ohniska
		'    objNewMember.VracetJenPoslVr = VracetJenPoslVr
		'    objNewMember.P�idatR�m = P�idatR�m
		'    Set objNewMember.LinY = LinY
		'    Set objNewMember.LinX = LinX
		'    objNewMember.Linia = Linia
		'    Set objNewMember.Sm�ry = Sm�ry
		'    objNewMember.Sm�rov� = Sm�rov�
		'    Set objNewMember.Naka�en� = Naka�en�
		'    objNewMember.VyhnoutSeNakreslen�m = VyhnoutSeNakreslen�m
		'    objNewMember.Dla�dice = Dla�dice
		'    objNewMember.Vy�istit = Vy�istit
		'    Set objNewMember.Po�etVrstev = Po�etVrstev
		'    Set objNewMember.N�hHodOh = N�hHodOh
		'    Set objNewMember.HodOh = HodOh
		'    Set objNewMember.Po�Oh = Po�Oh
		'    objNewMember.PovolitP�ekr�v�n� = PovolitP�ekr�v�n�
		'    Set objNewMember.SmRozptyl = SmRozptyl
		'    Set objNewMember.Po�etSous = Po�etSous
		'    Set objNewMember.Po�ad�Sous = Po�ad�Sous
		'    Set objNewMember.Soused� = Soused�
		'    Set objNewMember.Mutace = Mutace
		'    Set objNewMember.KolEp = KolEp
		
		
		
		
		If Len(sKey) = 0 Then
			mCol.Add(objNewMember)
		Else
			mCol.Add(objNewMember, sKey)
		End If
		
		
		'return the object created
		Add = objNewMember
        objNewMember = Nothing
		
		
	End Function
	
	
	
	
	Public Default Property Item(ByVal Index As Integer) As Epidemia
		Get
			'used when referencing an element in the collection
			'vntIndexKey contains either the Index or Key to the collection,
			'this is why it is declared as a Variant
			'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
			Item = mCol.Item(Index)
		End Get
		Set(ByVal Value As Epidemia)
            If IsReference(Value) Then
                'used when referencing an element in the collection
                'vntIndexKey contains either the Index or Key to the collection,
                'this is why it is declared as a Variant
                'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
                If mCol.Count() > 0 Then
                    If Index > mCol.Count() Then
                        '        mCol.Add Nosi�, , Index
                        '        mCol.Remove Index + 1
                        mCol.Add(Value)
                    Else
                        CopyEp(Value, mCol.Item(Index))
                    End If
                End If
            Else
                'used when referencing an element in the collection
                'vntIndexKey contains either the Index or Key to the collection,
                'this is why it is declared as a Variant
                'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
                mCol.Remove(Index)
                If Index > mCol.Count() Then
                    mCol.Add(Value)
                Else
                    mCol.Add(Value, , Index)
                End If
            End If
		End Set
	End Property
	
	
	
	Public ReadOnly Property Count() As Integer
		Get
			'used when retrieving the number of elements in the
			'collection. Syntax: Debug.Print x.Count
			Count = mCol.Count()
		End Get
	End Property
	
	

	Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        Return mCol.GetEnumerator
	End Function
	
	Public Sub CopyEp(ByRef Kterou As Epidemia, ByRef DoKter� As Epidemia)
		Dim Ep As Epidemia
		Ep = DoKter�
		With Kterou
			Ep.Dla�dice = .Dla�dice
            Ep.HodOh = .HodOh
			Ep.KolEp = .KolEp
			Ep.Linia = .Linia
            Ep.LinX = .LinX
            Ep.LinY = .LinY
			Ep.Mutace = .Mutace
			Ep.N�hHodOh = .N�hHodOh
			Ep.Naka�en� = .Naka�en�
			Ep.Nav�zatNaMinulou = .Nav�zatNaMinulou
			Ep.Ohniska = .Ohniska
            Ep.Po�etSous = .Po�etSous
			Ep.Po�etVrstev = .Po�etVrstev
            Ep.Po�Oh = .Po�Oh
			Ep.Po�ad�Sous = .Po�ad�Sous
			Ep.PovolitP�ekr�v�n� = .PovolitP�ekr�v�n�
			Ep.P�idatR�m = .P�idatR�m
			Ep.Sm�rov� = .Sm�rov�
            Ep.Sm�ry = .Sm�ry
            Ep.SmRozptyl = .SmRozptyl
			Ep.Soused� = .Soused�
			Ep.VracetJenPoslVr = .VracetJenPoslVr
			Ep.Vy�istit = .Vy�istit
            'Ep.VyhnoutSeNakreslen�m = .VyhnoutSeNakreslen�m
		End With
	End Sub
	
	
	Public Sub Remove(ByRef vntIndexKey As Object)
		'used when removing an element from the collection
		'vntIndexKey contains either the Index or Key, which is why
		'it is declared as a Variant
		'Syntax: x.Remove(xyz)
		
		
		mCol.Remove(vntIndexKey)
	End Sub
	
	
    Public Sub New()
        MyBase.New()
        mCol = New Collection
    End Sub
	
    Protected Overrides Sub Finalize()
        mCol = Nothing
        MyBase.Finalize()
    End Sub
End Class