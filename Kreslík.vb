﻿Public Class Kreslík
    Inherits PictureBox

    Private pamZásobník As Bitmap

    Public ReadOnly Property Zásobník() As Bitmap
        Get
            Return pamZásobník
        End Get
    End Property

    Private Sub Kreslík_ClientSizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ClientSizeChanged

        If Zásobník.Width < ClientSize.Width _
        OrElse Zásobník.Height < ClientSize.Height Then
            Dim NovýZásobník As Bitmap = _
                New Bitmap(Me.ClientSize.Width, Me.ClientSize.Height)
            Dim Gr As Graphics = Graphics.FromImage(NovýZásobník)
            Gr.DrawImageUnscaled(Zásobník, New Point(0, 0))
            pamZásobník = NovýZásobník
        End If
    End Sub

    Private Sub Kreslík_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        Dim Gr As Graphics = e.Graphics
        Gr.DrawImageUnscaled(Zásobník, New Point(0, 0))
    End Sub

    Public Property Pixel(ByVal X As Integer, ByVal Y As Integer) As Color
        Get
            Return Zásobník.GetPixel(X, Y)
        End Get
        Set(ByVal TaBarva As Color)
            Zásobník.SetPixel(X, Y, TaBarva)
            'Refresh()
        End Set
    End Property

    Public Sub Vyčisti()
        Dim Gr As Graphics = Graphics.FromImage(Zásobník)
        Dim Štětec As Brush = Brushes.White
        Gr.FillRectangle(Štětec, 0, 0, Zásobník.Width, Zásobník.Height)
        Refresh()
    End Sub

    Public Sub New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        pamZásobník = New Bitmap(ClientSize.Width, ClientSize.Height)
    End Sub
End Class
