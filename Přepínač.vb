Option Strict Off
Option Explicit On
Public Class P�ep�na�
    Inherits System.Windows.Forms.UserControl

    Public Shadows Event Click As EventHandler '(ByVal sender As Object, ByVal e As System.EventArgs)
    Public Event Zm�na As EventHandler

    Private Sub P�ep�na�_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        chb�krt.SetBounds(0, 0, MyBase.ClientRectangle.Width, MyBase.ClientRectangle.Height)
    End Sub

    Private Sub P�ep�na�_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        chb�krt.SetBounds(0, 0, MyBase.ClientRectangle.Width, MyBase.ClientRectangle.Height)
    End Sub


    Public Property Caption() As String
        Get
            Dim N�pis As String
            N�pis = chb�krt.Text
            Return N�pis
        End Get
        Set(ByVal Value As String)
            chb�krt.Text = Value
        End Set
    End Property


    Public Property Hodnota() As Boolean
        Get
            Hodnota = chb�krt.Checked
        End Get
        Set(ByVal Value As Boolean)
            chb�krt.Checked = Value
        End Set
    End Property


    Public Property ToolTipText() As String
        Get
            ToolTipText = ToolTip1.GetToolTip(chb�krt)
        End Get
        Set(ByVal Value As String)
            ToolTip1.SetToolTip(chb�krt, Value)
        End Set
    End Property


    Public Shadows Property Enabled() As Boolean
        Get
            Enabled = chb�krt.Enabled
        End Get
        Set(ByVal Value As Boolean)
            chb�krt.Enabled = Value
        End Set
    End Property

    Private Sub chb�krt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles chb�krt.Click
        RaiseEvent Click(Me, e)
    End Sub

    Private Sub chb�krt_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chb�krt.CheckedChanged
        RaiseEvent Zm�na(Me, e)
    End Sub
End Class