<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Směrovka
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim _přepSm_0 As Fraktálník.Přepínač
        Dim _přepSm_1 As Fraktálník.Přepínač
        Dim _přepSm_2 As Fraktálník.Přepínač
        Dim _přepSm_3 As Fraktálník.Přepínač
        Dim _přepSm_4 As Fraktálník.Přepínač
        Dim _přepSm_5 As Fraktálník.Přepínač
        Dim _přepSm_6 As Fraktálník.Přepínač
        Dim _přepSm_7 As Fraktálník.Přepínač
        Me.přepVšechnySm = New System.Windows.Forms.CheckBox
        _přepSm_0 = New Fraktálník.Přepínač
        _přepSm_1 = New Fraktálník.Přepínač
        _přepSm_2 = New Fraktálník.Přepínač
        _přepSm_3 = New Fraktálník.Přepínač
        _přepSm_4 = New Fraktálník.Přepínač
        _přepSm_5 = New Fraktálník.Přepínač
        _přepSm_6 = New Fraktálník.Přepínač
        _přepSm_7 = New Fraktálník.Přepínač
        Me.SuspendLayout()
        '
        '_přepSm_0
        '
        _přepSm_0.Caption = ""
        _přepSm_0.Hodnota = False
        _přepSm_0.Location = New System.Drawing.Point(35, 3)
        _přepSm_0.Name = "_přepSm_0"
        _přepSm_0.Size = New System.Drawing.Size(16, 17)
        _přepSm_0.TabIndex = 56
        _přepSm_0.ToolTipText = ""
        AddHandler _přepSm_0.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_0.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_1
        '
        _přepSm_1.Caption = ""
        _přepSm_1.Hodnota = False
        _přepSm_1.Location = New System.Drawing.Point(35, 19)
        _přepSm_1.Name = "_přepSm_1"
        _přepSm_1.Size = New System.Drawing.Size(16, 17)
        _přepSm_1.TabIndex = 57
        _přepSm_1.ToolTipText = ""
        AddHandler _přepSm_1.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_1.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_2
        '
        _přepSm_2.Caption = ""
        _přepSm_2.Hodnota = False
        _přepSm_2.Location = New System.Drawing.Point(35, 35)
        _přepSm_2.Name = "_přepSm_2"
        _přepSm_2.Size = New System.Drawing.Size(16, 17)
        _přepSm_2.TabIndex = 58
        _přepSm_2.ToolTipText = ""
        AddHandler _přepSm_2.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_2.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_3
        '
        _přepSm_3.Caption = ""
        _přepSm_3.Hodnota = False
        _přepSm_3.Location = New System.Drawing.Point(19, 35)
        _přepSm_3.Name = "_přepSm_3"
        _přepSm_3.Size = New System.Drawing.Size(16, 17)
        _přepSm_3.TabIndex = 59
        _přepSm_3.ToolTipText = ""
        AddHandler _přepSm_3.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_3.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_4
        '
        _přepSm_4.Caption = ""
        _přepSm_4.Hodnota = False
        _přepSm_4.Location = New System.Drawing.Point(3, 35)
        _přepSm_4.Name = "_přepSm_4"
        _přepSm_4.Size = New System.Drawing.Size(16, 17)
        _přepSm_4.TabIndex = 60
        _přepSm_4.ToolTipText = ""
        AddHandler _přepSm_4.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_4.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_5
        '
        _přepSm_5.Caption = ""
        _přepSm_5.Hodnota = False
        _přepSm_5.Location = New System.Drawing.Point(3, 19)
        _přepSm_5.Name = "_přepSm_5"
        _přepSm_5.Size = New System.Drawing.Size(16, 17)
        _přepSm_5.TabIndex = 61
        _přepSm_5.ToolTipText = ""
        AddHandler _přepSm_5.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_5.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_6
        '
        _přepSm_6.Caption = ""
        _přepSm_6.Hodnota = False
        _přepSm_6.Location = New System.Drawing.Point(3, 3)
        _přepSm_6.Name = "_přepSm_6"
        _přepSm_6.Size = New System.Drawing.Size(16, 17)
        _přepSm_6.TabIndex = 62
        _přepSm_6.ToolTipText = ""
        AddHandler _přepSm_6.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_6.Změna, AddressOf Me.přepSm_Změna
        '
        '_přepSm_7
        '
        _přepSm_7.Caption = ""
        _přepSm_7.Hodnota = False
        _přepSm_7.Location = New System.Drawing.Point(19, 3)
        _přepSm_7.Name = "_přepSm_7"
        _přepSm_7.Size = New System.Drawing.Size(16, 17)
        _přepSm_7.TabIndex = 63
        _přepSm_7.ToolTipText = ""
        AddHandler _přepSm_7.Click, AddressOf Me.přepSm_Click
        AddHandler _přepSm_7.Změna, AddressOf Me.přepSm_Změna
        '
        'přepVšechnySm
        '
        Me.přepVšechnySm.BackColor = System.Drawing.SystemColors.Control
        Me.přepVšechnySm.Cursor = System.Windows.Forms.Cursors.Default
        Me.přepVšechnySm.ForeColor = System.Drawing.SystemColors.ControlText
        Me.přepVšechnySm.Location = New System.Drawing.Point(19, 19)
        Me.přepVšechnySm.Name = "přepVšechnySm"
        Me.přepVšechnySm.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.přepVšechnySm.Size = New System.Drawing.Size(14, 17)
        Me.přepVšechnySm.TabIndex = 64
        Me.přepVšechnySm.UseVisualStyleBackColor = False
        '
        'Směrovka
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.přepVšechnySm)
        Me.Controls.Add(_přepSm_0)
        Me.Controls.Add(_přepSm_1)
        Me.Controls.Add(_přepSm_2)
        Me.Controls.Add(_přepSm_3)
        Me.Controls.Add(_přepSm_4)
        Me.Controls.Add(_přepSm_5)
        Me.Controls.Add(_přepSm_6)
        Me.Controls.Add(_přepSm_7)
        Me.Name = "Směrovka"
        Me.Size = New System.Drawing.Size(51, 52)
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents přepVšechnySm As System.Windows.Forms.CheckBox

End Class
