﻿<System.ComponentModel.DefaultEvent("Click")> _
Public Class Směrovka
    Public Shadows Event Click( _
        ByRef Sender As Object, _
        ByVal e As System.EventArgs, _
        ByVal Index As Short)
    Public Event Změna( _
        ByRef Sender As Object, _
        ByVal e As System.EventArgs, _
        ByVal Index As Short)

    Private WithEvents Seznam As New ArrayList(8)

    Private Sub Směrovka_Load( _
      ByVal sender As Object, _
      ByVal e As System.EventArgs) Handles Me.Load
        Dim Ovl As Control
        For I As Integer = 0 To 7
            Ovl = Me.Controls("_přepSm_" & I)
            Seznam.Add(Ovl)
        Next

    End Sub

    Private Sub přepVšechnySm_CheckStateChanged( _
      ByVal eventSender As System.Object, _
      ByVal eventArgs As System.EventArgs) _
      Handles přepVšechnySm.CheckStateChanged
        Dim Přep As Přepínač
        Dim PůvHod As Short
        PůvHod = přepVšechnySm.CheckState
        For Each Přep In Seznam
            If přepVšechnySm.CheckState = 1 Then
                Přep.Hodnota = True
            ElseIf přepVšechnySm.CheckState = 0 Then
                Přep.Hodnota = False
            End If
            přepVšechnySm.CheckState = PůvHod
        Next Přep
    End Sub

    Property VšechnySm() As Short
        Get
            Dim Přep As Přepínač
            Dim Poč As Integer
            For Each Přep In Seznam
                If Přep.Hodnota Then Poč = Poč + 1
            Next Přep
            Select Case Poč
                Case 0
                    VšechnySm = 0
                Case 8
                    VšechnySm = 1
                Case 1 To 7
                    VšechnySm = 2
            End Select
        End Get
        Set(ByVal ZdaVšechny As Short)
            Select Case ZdaVšechny
                Case 0 To 1
                    For Each Přep As Přepínač In Seznam
                        Přep.Hodnota = CBool(ZdaVšechny)
                    Next
                Case Else
                    ' Nic nenastavovat - nevíme, co
            End Select
            ZjistiVšSm()
        End Set
    End Property

    Sub ZjistiVšSm()
        přepVšechnySm.CheckState = VšechnySm
    End Sub


    Private Sub přepSm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim Index As Short = Seznam.IndexOf(sender)
        ZjistiVšSm()
        RaiseEvent Click(Me, e, Index)
    End Sub

    Private Sub přepSm_Změna(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim Index As Short = Seznam.IndexOf(sender)
        ZjistiVšSm()
        RaiseEvent Změna(Me, e, Index)
    End Sub

    Public Property Hodnota(ByVal Index As Byte) As Boolean
        Get
            Return CType(Seznam(Index), Přepínač).Hodnota
        End Get
        Set(ByVal value As Boolean)
            CType(Seznam(Index), Přepínač).Hodnota = value
        End Set
    End Property

End Class
