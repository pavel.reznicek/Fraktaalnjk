Option Strict Off
Option Explicit On
Imports System.Math
Friend Class TTrpasl�k

    'local variable(s) to hold property value(s)
    Private mvarBarva As Integer 'local copy
    Private mvar� As Byte 'local copy
    Private mvarZ As Byte 'local copy
    Private mvarM As Byte 'local copy
    Private mvarPropojenci As Dru�ina 'local copy
    Private mvarX As Short 'local copy
    Private mvarY As Short 'local copy
    'local variable(s) to hold property value(s)
    Private mvar�ik As �ikTrpasl�k� 'local copy
    Private pamSmEp As TOsmism�r 'jak�m sm�rem p�i�la n�kaza

    Public Pic As Kresl�k


    Public Property Hodnota() As Double
        Get
            Hodnota = Mapa(mvarX, mvarY).Hodnota
        End Get
        Set(ByVal Value As Double)
            Mapa(mvarX, mvarY).Hodnota = Value
        End Set
    End Property

    Public Property Vrstva() As Int64
        Get
            Vrstva = Mapa(mvarX, mvarY).Vrstva
        End Get
        Set(ByVal Value As Int64)
            Mapa(mvarX, mvarY).Vrstva = Value
        End Set
    End Property

    Public Property Nakreslen() As Boolean
        Get
            Nakreslen = Mapa(mvarX, mvarY).Nakreslen
        End Get
        Set(ByVal Value As Boolean)
            Mapa(mvarX, mvarY).Nakreslen = Value
        End Set
    End Property



    Public Property �ik() As �ikTrpasl�k�
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.�ik
            �ik = mvar�ik
        End Get
        Set(ByVal Value As �ikTrpasl�k�)
            'used when assigning an Object to the property, on the left side of a Set statement.
            'Syntax: Set x.�ik = Form1
            mvar�ik = Value
        End Set
    End Property





    Public Property Y() As Short
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.Y
            Y = mvarY
        End Get
        Set(ByVal Value As Short)
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.Y = 5
            mvarY = Value
        End Set
    End Property





    Public Property X() As Short
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.X
            X = mvarX
        End Get
        Set(ByVal Value As Short)
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.X = 5
            mvarX = Value
        End Set
    End Property



    'Public ReadOnly Property Soused�( _
    '  Optional ByVal Po�ad� As modSP.Po�ad�Soused� = _
    '    modSP.Po�ad�Soused�.psN�hodn�, _
    '  Optional ByVal Jac�Sous As modSP.TypSoused� = 0, _
    '  Optional ByVal Po�etN�h As Short = 0, _
    '  Optional ByVal SmRozptyl As modSP.TSmRozptyl = 0, _
    '  Optional ByVal PovolitP�ekr�v�n� As Boolean = False, _
    '  Optional ByVal P�idatPropojence As Boolean = False) As Dru�ina
    '    Get
    '        Dim Sous As TTrpasl�k
    '        Dim �Sous As Short
    '        Dim smSous() As Short
    '        Dim I, J As Integer
    '        Dim V�sledek As New Dru�ina
    '        ' Typ soused� tsN�hodn�N�strann��iN�ro�n� 
    '        ' p�evedeme na n�strann� nebo n�ro�n�
    '        If Jac�Sous = TypSoused�.tsN�hodn�N�strann��iN�ro�n� Then
    '            Jac�Sous = Dvojn�hoda( _
    '                TypSoused�.tsN�strann�, _
    '                TypSoused�.tsN�ro�n�, 0.3)
    '        ElseIf Jac�Sous = TypSoused�.tsSp�eN�strann� Then
    '            Jac�Sous = Dvojn�hoda( _
    '                TypSoused�.tsN�strann�, _
    '                TypSoused�.tsN�ro�n�, 0.7)
    '        End If
    '        If Jac�Sous = modSP.TypSoused�.tsV�ichni Then
    '            ReDim smSous(7)
    '        Else
    '            ReDim smSous(3)
    '        End If
    '        Dim Obsazeno, JeVMez�ch As Boolean
    '        Dim N�hodn�k As New GenerN
    '        Dim St�ela As Short
    '        Dim MaxPo�Rozpt As Object
    '        Dim N�hPo� As New GenerN
    '        Select Case Po�ad�
    '            Case modSP.Po�ad�Soused�.psPravideln�
    '                For I = LBound(smSous) To UBound(smSous)
    '                    smSous(I) = I
    '                Next
    '            Case modSP.Po�ad�Soused�.psN�hodn�, modSP.Po�ad�Soused�.psN�hodn�SVynech�n�m
    '                N�hodn�k.Smallest = LBound(smSous)
    '                N�hodn�k.Largest = UBound(smSous)
    '                ' sta�en� zadan�ho po�tu na nejvy��� mo�n� po�et soused�
    '                If Po�etN�h > UBound(smSous) + 1 Then Po�etN�h = UBound(smSous) + 1
    '                ' sta�en� zadan�ho po�tu na nejv. mo�. po�. sous. v dan�m sm�rov�m rozptylu
    '                If (Abs(SmRozptyl) > 0) And (Po�etN�h > 2 * Abs(SmRozptyl) + 1) Then
    '                    Po�etN�h = 2 * Abs(SmRozptyl) + 1
    '                End If
    '                ' pole sm�r� o n�hodn�m �i zadan�m po�tu prvk�
    '                If Po�ad� = Po�ad�Soused�.psN�hodn�SVynech�n�m Then
    '                    If Po�etN�h > 0 Then
    '                        ReDim smSous(Po�etN�h - 1)
    '                    Else
    '                        ReDim smSous(N�hodn�k.Value)
    '                    End If
    '                End If
    '                ' sm�rov� rozptyl plat� jen v n�hodn�m po�ad� bez vynech�n�
    '                If Po�ad� = modSP.Po�ad�Soused�.psN�hodn� And Abs(SmRozptyl) > 0 Then
    '                    MaxPo�Rozpt = 2 * Abs(SmRozptyl) + 1
    '                    N�hPo�.Smallest = 0
    '                    If MaxPo�Rozpt > Po�etN�h Then
    '                        N�hPo�.Largest = Po�etN�h - 1
    '                    Else
    '                        N�hPo�.Largest = MaxPo�Rozpt - 1
    '                    End If
    '                    ReDim smSous(N�hPo�.Value)
    '                End If
    '                ' nam�ch�n� sm�r�
    '                N�hodn�k.Smallest = 1
    '                If Jac�Sous = TypSoused�.tsV�ichni Then
    '                    N�hodn�k.Largest = 8
    '                Else
    '                    N�hodn�k.Largest = 4
    '                End If
    '                For I = LBound(smSous) To UBound(smSous)
    '                    ' hled� se je�t� neobsazen� sm�r
    '                    Do
    '                        Obsazeno = False
    '                        St�ela = N�hodn�k.Value
    '                        ' proch�z�me: je u� n�h. sm�r pou�it?
    '                        For J = LBound(smSous) To I
    '                            If smSous(J) = St�ela Then
    '                                Obsazeno = True
    '                                Exit For
    '                            End If
    '                        Next
    '                        ' je�t� jestli je ve sm�rov�m rozptylu
    '                        If Abs(SmRozptyl) > 0 Then
    '                            JeVMez�ch = JeSm�rVMez�ch(St�ela, _
    '                                SmEp(Jac�Sous), Abs(SmRozptyl)) ', Jac�Sous)
    '                        Else
    '                            JeVMez�ch = True
    '                        End If
    '                    Loop While Obsazeno Or Not JeVMez�ch ' dokud sm�r nevyhovuje
    '                    ' u� vyhovuje:
    '                    smSous(I) = St�ela
    '                Next
    '        End Select
    '        ' uplatn�n� vybran�ch sm�r�
    '        For �Sous = LBound(smSous) To UBound(smSous)
    '            Sous = Nothing
    '            Select Case Jac�Sous
    '                Case modSP.TypSoused�.tsN�strann� ' standard
    '                    Sous = �ik(X + XSlN�str(smSous(�Sous)), Y + YSlN�str(smSous(�Sous)))
    '                Case modSP.TypSoused�.tsN�ro�n� ' jen n�ro�n�
    '                    Sous = �ik(X + XSlN�r(smSous(�Sous)), Y + YSlN�r(smSous(�Sous)))
    '                Case modSP.TypSoused�.tsV�ichni ' n�strann� i n�ro�n�
    '                    Sous = �ik(X + Sm�r(smSous(�Sous)).X, Y + Sm�r(smSous(�Sous)).Y)
    '            End Select
    '            If Sous IsNot Nothing Then
    '                If Not Sous.Nakreslen Or PovolitP�ekr�v�n� Then
    '                    V�sledek.P�idej(Sous)
    '                    Sous.Vrstva = Me.Vrstva + 1
    '                    If SmRozptyl > 0 Then
    '                        Sous.SmEp(Jac�Sous) = SmEp(Jac�Sous)
    '                    Else
    '                        Sous.SmEp(Jac�Sous) = smSous(�Sous)
    '                    End If
    '                    Sous.P�vSmEp(Jac�Sous) = Me.P�vSmEp(Jac�Sous)
    '                End If
    '            End If
    '        Next
    '        Dim Trp As TTrpasl�k
    '        If P�idatPropojence Then
    '            If Not mvarPropojenci Is Nothing Then
    '                For Each Trp In mvarPropojenci
    '                    Trp.Vrstva = Me.Vrstva + 1
    '                    V�sledek.P�idej(Trp)
    '                Next Trp
    '            End If
    '        End If
    '        Soused� = V�sledek
    '    End Get
    'End Property

    'Public ReadOnly Property Soused�ZeSm�r�(ByVal Po�ad� As modSP.Po�ad�Soused�, ByVal Po�etN�h As Object, ByVal PovolitP�ekr�v�n� As Boolean, ByVal ParamArray Sm�ry() As Object) As Dru�ina
    '    Get
    '        Dim Sous As TTrpasl�k
    '        Dim �Sous As Short
    '        Dim smSous() As Short
    '        Dim UprSm�ry() As Object 
    '        Dim I, J As Integer
    '        Dim V�sledek As New Dru�ina
    '        ' Do pole parametr� Sm�ry() m��e p�ij�t pole v polo�ce 0, 
    '        ' kter� pot�ebujeme pou��t. Upraven� sm�ry 
    '        ' obsahuj� bu�to cel� Sm�ry() nebo Sm�ry(0).
    '        If IsArray(Sm�ry(0)) Then
    '            UprSm�ry = Sm�ry(0)
    '        Else
    '            UprSm�ry = Sm�ry
    '        End If

    '        ReDim smSous(UBound(UprSm�ry))
    '        Dim Obsazeno As Boolean
    '        Dim N�hodn�k As New GenerN
    '        Dim St�ela As Short
    '        Dim JeZad�n As Boolean
    '        Select Case Po�ad�
    '            Case modSP.Po�ad�Soused�.psPravideln�
    '                For I = LBound(smSous) To UBound(smSous)
    '                    smSous(I) = UprSm�ry(I) ' kopie 1:1
    '                Next
    '            Case modSP.Po�ad�Soused�.psN�hodn�, modSP.Po�ad�Soused�.psN�hodn�SVynech�n�m
    '                N�hodn�k.Smallest = LBound(smSous)
    '                N�hodn�k.Largest = UBound(smSous)
    '                ' sta�en� zadan�ho po�tu na nejvy��� mo�n� po�et soused�
    '                If Po�etN�h > smSous.Length Then Po�etN�h = smSous.Length
    '                ' pole sm�r� o n�hodn�m �i zadan�m po�tu prvk�
    '                If Po�ad� = Po�ad�Soused�.psN�hodn�SVynech�n�m Then
    '                    If Po�etN�h > 0 Then ' Je zad�n pevn� po�et n�hodn�ch soused�
    '                        ReDim smSous(Po�etN�h - 1)
    '                    Else ' Po�etN�h = 0 => n�hodn� po�et soused�
    '                        ReDim smSous(N�hodn�k.Value)
    '                    End If
    '                End If
    '                ' nam�ch�n� sm�r�
    '                N�hodn�k.Smallest = 1
    '                N�hodn�k.Largest = 8
    '                For I = LBound(smSous) To UBound(smSous)
    '                    ' hled� se je�t� neobsazen� a vyhovuj�c� sm�r
    '                    Do
    '                        St�ela = N�hodn�k.Value
    '                        Obsazeno = False
    '                        ' proch�z�me: je u� n�h. sm�r pou�it?
    '                        For J = LBound(smSous) To I
    '                            If smSous(J) = St�ela Then
    '                                Obsazeno = True
    '                            End If
    '                        Next
    '                        ' proch�z�me: je n�h. sm�r mezi zadan�mi?
    '                        JeZad�n = False
    '                        For J = LBound(UprSm�ry) To UBound(UprSm�ry)
    '                            If St�ela = UprSm�ry(J) Then
    '                                JeZad�n = True
    '                                Exit For
    '                            End If
    '                        Next
    '                    Loop While Obsazeno Or Not JeZad�n ' dokud sm�r nevyhovuje
    '                    ' u� vyhovuje:
    '                    smSous(I) = St�ela
    '                Next
    '        End Select
    '        ' uplatn�n� vybran�ch sm�r�
    '        For �Sous = LBound(smSous) To UBound(smSous)
    '            Sous = �ik(X + XSlOsm(smSous(�Sous)), _
    '                       Y + YSlOsm(smSous(�Sous)))
    '            If Sous IsNot Nothing Then
    '                If Not Sous.Nakreslen Or PovolitP�ekr�v�n� Then
    '                    Sous.Vrstva = Me.Vrstva + 1
    '                    V�sledek.P�idej(Sous)
    '                    Sous.SmEp(modSP.TypSoused�.tsV�ichni) = smSous(�Sous)
    '                    Sous.P�vSmEp = Me.P�vSmEp
    '                End If
    '            End If
    '        Next
    '        Dim Trp As TTrpasl�k
    '        If mvarPropojenci IsNot Nothing Then
    '            For Each Trp In mvarPropojenci
    '                Trp.Vrstva = Me.Vrstva + 1
    '                V�sledek.P�idej(Trp)
    '            Next Trp
    '        End If
    '        Soused�ZeSm�r� = V�sledek
    '    End Get
    'End Property

    Public ReadOnly Property Soused�( _
      Optional ByVal Po�ad� As modSP.Po�ad�Soused� = _
        modSP.Po�ad�Soused�.psN�hodn�, _
      Optional ByVal Jac�Sous As modSP.TypSoused� = 0, _
      Optional ByVal Po�etVybran�ch As Short = 0, _
      Optional ByVal SmRozptyl As modSP.TSmRozptyl = 0, _
      Optional ByVal PovolitP�ekr�v�n� As Boolean = False, _
      Optional ByVal P�idatPropojence As Boolean = False) As Dru�ina
        Get
            Dim Sm�ryKSoused�m As TOsmism�r() = _
                VyberSm�ryKSoused�mPodlePo�tu( _
                    VyberSm�ryKSoused�mPodleNakreslen�( _
                        VyberSm�ryKSoused�mPodleRozptylu( _
                            VyberSm�ryKSoused�mPodleTypu( _
                                Se�a�Sm�ryKSoused�mPodlePo�ad�( _
                                    V�echnyOsmism�ry, Po�ad� _
                                ), _
                                Jac�Sous), _
                            IIf(SmRozptyl >= 0, SmEp, P�vSmEp), _
                            SmRozptyl _
                            ), _
                        PovolitP�ekr�v�n� _
                        ), _
                    Po�etVybran�ch _
                )
            Soused� = New Dru�ina
            Dim Nov� As TTrpasl�k
            For Each Sm As TOsmism�r In Sm�ryKSoused�m
                Nov� = Soused�.P�idej(SousedZeSm�ru(Sm), Sm.ToString)
                If Nov� IsNot Nothing Then
                    If SmRozptyl >= 0 Then
                        Nov�.SmEp = Sm
                    Else
                        Nov�.P�vSmEp = P�vSmEp
                    End If
                End If
            Next
        End Get
    End Property

    Public ReadOnly Property Soused�ZeSm�r�( _
      ByVal Po�ad� As modSP.Po�ad�Soused�, _
      ByVal Po�etVybran�ch As Short, _
      ByVal PovolitP�ekr�v�n� As Boolean, _
      ByVal Sm�ry As TOsmism�r()) As Dru�ina
        Get
            'If Sm�ry.Length = 1 AndAlso TypeOf Sm�ry(0) Is Object() Then
            '  Sm�ry = Sm�ry(0)
            'End If

            Dim Sm�ryKSoused�mPodlePo�ad� As TOsmism�r()
            Dim Sm�ryKSoused�mPodleNakreslen� As TOsmism�r()
            Dim Sm�ryKSoused�mPodlePo�tu As TOsmism�r()
            Dim Sm�ryKSoused�m As TOsmism�r()
            Sm�ryKSoused�mPodlePo�ad� = Se�a�Sm�ryKSoused�mPodlePo�ad�(Sm�ry, Po�ad�)
            Sm�ryKSoused�mPodleNakreslen� = VyberSm�ryKSoused�mPodleNakreslen�(Sm�ryKSoused�mPodlePo�ad�, PovolitP�ekr�v�n�)
            Sm�ryKSoused�mPodlePo�tu = VyberSm�ryKSoused�mPodlePo�tu(Sm�ryKSoused�mPodleNakreslen�, Po�etVybran�ch)
            Sm�ryKSoused�m = Sm�ryKSoused�mPodlePo�tu


            Soused�ZeSm�r� = New Dru�ina
            For Each Sm As TOsmism�r In Sm�ryKSoused�m
                Soused�ZeSm�r�.P�idej(SousedZeSm�ru(Sm), Sm.ToString)
            Next
        End Get
    End Property


    Public Function VyberSm�ryKSoused�mPodleNakreslen�( _
      ByVal Vstupn�Sm�ry As TOsmism�r(), _
      ByVal PovolitP�ekr�v�n� As Boolean) As TOsmism�r()
        Dim V�sledek As TOsmism�r() = {}
        Dim Soused As TTrpasl�k
        For Each Sm As TOsmism�r In Vstupn�Sm�ry
            Soused = SousedZeSm�ru(Sm)
            ' Pokud soused nen� nakreslen nebo pokud se nemus�me vyh�bat nakreslen�m
            If Soused IsNot Nothing Then
                If Not Soused.Nakreslen Or PovolitP�ekr�v�n� Then
                    ReDim Preserve V�sledek(UBound(V�sledek) + 1)
                    V�sledek(UBound(V�sledek)) = Sm
                End If
            End If
        Next
        Return V�sledek
    End Function

    Public Function SousedZeSm�ru(ByVal Sm As TOsmism�r) As TTrpasl�k
        Return mvar�ik(mvarX + Sm�r(Sm).X, mvarY + Sm�r(Sm).Y)
    End Function

    Public Property Propojenci() As Dru�ina
        Get
            Propojenci = mvarPropojenci
        End Get
        Set(ByVal Value As Dru�ina)
            mvarPropojenci = Value
        End Set
    End Property


    '
    'Public Sub Epidemia(ByVal EpBar As Long, Optional Vy�a�Co As dru�ina)
    '    Dim Sous As Variant, N�h As New GenerN
    '    Dim KolSous As New dru�ina
    '    Dim Nov� As Boolean
    '    If Vy�a�Co Is Nothing Then
    '        Set Vy�a�Co = New dru�ina
    '    End If
    '    Vy�a�Co.Add Me
    '    N�h.Smallest = -1
    '    N�h.Largest = 1
    '    EpBar = EpBar + N�h
    '    Barva = EpBar
    '    EpBar = Barva
    '    Set KolSous = Soused�
    '    For Each Sous In KolSous
    '        DoEvents
    '        Nov� = Not InCol(Vy�a�Co, Sous)
    '        If Nov� Then
    '            Sous.Epidemia EpBar, Vy�a�Co
    '        End If
    '    Next
    'End Sub




    Public Property BarSl(ByVal Slo�ka As modSP.Slo�kyBarvy) As Byte
        Get
            'used when retrieving value of a property, on the right side of an assignment.
            'Syntax: Debug.Print X.M
            'M = mvarM
            Select Case Slo�ka
                Case Slo�kyBarvy.sb�erven�
                    Return �erven�(Barva)
                Case Slo�kyBarvy.sbModr�
                    Return Modr�(Barva)
                Case Slo�kyBarvy.sbZelen�
                    Return Zelen�(Barva)
                Case Else
                    Return 0
            End Select
        End Get
        Set(ByVal Value As Byte) 'barevn� slo�ka
            'used when assigning a value to the property, on the left side of an assignment.
            'Syntax: X.M = 5
            If Value < 0 Then Value = 0
            If Value > 255 Then Value = 255
            Dim Slo�ky(2) As Short
            Dim I As Object
            For I = 0 To 2
                If I = Slo�ka Then
                    Slo�ky(I) = Value
                Else
                    Slo�ky(I) = BarSl(mvarBarva)
                End If
            Next
            'Pic.PSet(New Point[](mvarX, mvarY), RGB(Slo�ky(0), Slo�ky(1), Slo�ky(2)))
            'Dim Gr As Drawing.Graphics = Graphics.FromImage(Pic.Image)
            Dim BarvaRGB As Color = Color.FromArgb(Slo�ky(0), Slo�ky(1), Slo�ky(2))
            'Dim Pero As Pen = New Pen(BarvaRGB)
            'Gr.DrawLine(Pero, mvarX, mvarY, mvarX + 1, mvarY + 1)
            Pic.Pixel(mvarX, mvarY) = BarvaRGB
            'mvarM = vData
        End Set
    End Property




    Public Property Barva() As Integer
        Get
            Barva = Drawing.ColorTranslator.ToOle(Pic.Pixel(mvarX, mvarY))
        End Get
        Set(ByVal Value As Integer)
            Dim BarvaDotNet As Drawing.Color = _
                Drawing.ColorTranslator.FromOle(Value)
            Pic.Pixel(mvarX, mvarY) = BarvaDotNet
            'Pic.Refresh() ' hod� se p�i podrobn�m sledov�n� postupu
        End Set
    End Property


    Public Property SmEp() As TOsmism�r
        Get
            SmEp = Mapa(mvarX, mvarY).SmEp
        End Get
        Set(ByVal Value As TOsmism�r)
            Mapa(mvarX, mvarY).SmEp = �Sm(Value)
        End Set
    End Property


    Public Property P�vSmEp() As TOsmism�r
        Get
            P�vSmEp = pamSmEp
        End Get
        Set(ByVal Value As TOsmism�r)
            pamSmEp = �Sm(Value)
        End Set
    End Property


    Public Sub Nastav�ZM(ByVal �e As Integer, ByVal Ze As Integer, ByVal Mo As Integer)
        'used when assigning a value to the property, on the left side of an assignment.
        'Syntax: X.Barva = 5
        'mvarBarva = vData
        If �e < 0 Then �e = 0
        If Ze < 0 Then Ze = 0
        If Mo < 0 Then Mo = 0
        If �e > 255 Then �e = 255
        If Ze > 255 Then Ze = 255
        If Mo > 255 Then Mo = 255
        Dim BarvaRGB As Color = Color.FromArgb(&HFF, �e, Ze, Mo)
        Me.Barva = BarvaRGB.ToArgb
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class