Option Strict Off
Option Explicit On
Friend Class Barvy
	Inherits System.Windows.Forms.UserControl
	
    Public Event Zm�naPo�tu(ByVal Sender As System.Object, ByVal e As System.EventArgs)
    Dim pamPo�et As Short
    Dim pamVid�tBarev As Short

    Public Property Vid�tBarev() As Short
        Get
            Return pamVid�tBarev
        End Get
        Set(ByVal Value As Short)
            If Value >= 1 Then
                pamVid�tBarev = Value
            Else
                pamVid�tBarev = 1
            End If
            Rozest�i()
        End Set
    End Property

    Public Property Po�et() As Short
        Get
            Po�et = picBarva.Count
        End Get
        Set(ByVal Value As Short)
            If Value = 0 Then Value = 1
            Dim I As Short
            Dim Obr As System.Windows.Forms.PictureBox
            For I = Value To picBarva.Count - 1
                picBarva(I).Image = Nothing
                picBarva.Unload(I)
            Next
            On Error Resume Next
            For I = picBarva.Count To Value - 1
                picBarva.Load(I)
            Next
            On Error GoTo 0

            ' Te� obarv�me v�ecky obr�zky
            For I = 0 To picBarva.Count - 1
                Barva(I) = DejBarvu(I)
            Next

            pamPo�et = Value
            Rozest�i()
            SaveSetting("Frakt�ln�k", "Barvy", "Po�et", CStr(Value))
            RaiseEvent Zm�naPo�tu(Me, Nothing)
        End Set
    End Property


    Public Property Barva(ByVal Index As Short) As Integer
        Get
            'Barva = GetSetting("Frakt�ln�k", "Barvy", "Barva " & Index, CStr(System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White)))
            Barva = System.Drawing.ColorTranslator.ToOle(picBarva(Index).BackColor)
        End Get
        Set(ByVal Value As Integer)
            picBarva(Index).BackColor = System.Drawing.ColorTranslator.FromOle(Value)
            SaveSetting("Frakt�ln�k", "Barvy", "Barva " & Index, CStr(Value))
        End Set
    End Property

    Private Function DejBarvu(ByRef Index As Short) As Object
        DejBarvu = GetSetting("Frakt�ln�k", "Barvy", "Barva " & Index, System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White).ToString)
    End Function

    Private Sub picBarva_MouseUp(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.MouseEventArgs) Handles picBarva.MouseUp
        Dim Index As Short = picBarva.GetIndex(eventSender)
        If eventArgs.Button = Windows.Forms.MouseButtons.Left Then
            picBarva(Index).BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
            CDColor.Color = picBarva(Index).BackColor
            On Error GoTo vypadni
            CDColor.ShowDialog()
            Barva(Index) = System.Drawing.ColorTranslator.ToOle(CDColor.Color)
        ElseIf eventArgs.Button = Windows.Forms.MouseButtons.Right Then

        End If
vypadni:
        picBarva(Index).BorderStyle = System.Windows.Forms.BorderStyle.None
    End Sub

    Private Sub Rozest�i()
        Static Vypadni As Boolean
        Dim ZrovnaVr�ek, JednotlV��, ��� As Short
        If Vypadni Then Exit Sub
        Vypadni = True
        JednotlV�� = ClientRectangle.Height \ IIf(Vid�tBarev = 0, 1, Vid�tBarev)
        Height = ((Height - ClientRectangle.Height) + (ClientRectangle.Height \ JednotlV��) * JednotlV��)
        Vypadni = False
        Dim Obr As System.Windows.Forms.PictureBox
        For Each Obr In picBarva
            ZrovnaVr�ek = picBarva.GetIndex(Obr) * JednotlV��
            ��� = ClientRectangle.Width
            Obr.SetBounds(0, ZrovnaVr�ek, ���, JednotlV��)
            Obr.Visible = True
        Next Obr
    End Sub

    Private Sub Barvy_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Rozest�i()
    End Sub

    Private Sub Barvy_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        Rozest�i()
    End Sub

End Class