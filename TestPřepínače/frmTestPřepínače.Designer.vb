﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestPřepínače
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.přepTest_0 = New TestPřepínače.Přepínač()
        Me.přepTest = New TestPřepínače.PřepínačArray(Me.components)
        CType(Me.přepTest, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'přepTest_0
        '
        Me.přepTest_0.Caption = "Přepínač"
        Me.přepTest_0.Hodnota = False
        Me.přepTest_0.Location = New System.Drawing.Point(20, 21)
        Me.přepTest_0.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.přepTest_0.Name = "přepTest_0"
        Me.přepTest_0.Size = New System.Drawing.Size(112, 21)
        Me.přepTest_0.TabIndex = 0
        Me.přepTest_0.ToolTipText = ""
        '
        'frmTestPřepínače
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(389, 327)
        Me.Controls.Add(Me.přepTest_0)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmTestPřepínače"
        Me.Text = "Test přepínače"
        CType(Me.přepTest, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents přepTest As TestPřepínače.PřepínačArray
    Friend WithEvents přepTest_0 As TestPřepínače.Přepínač

End Class
