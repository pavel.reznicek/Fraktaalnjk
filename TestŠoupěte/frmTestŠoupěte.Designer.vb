﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTestŠoupěte
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Šoupě1 = New TestŠoupěte.Šoupě
        Me.Šoupě2 = New TestŠoupěte.Šoupě
        Me.Šoupě3 = New TestŠoupěte.Šoupě
        Me.SuspendLayout()
        '
        'Šoupě1
        '
        Me.Šoupě1.Hodnota = 20
        Me.Šoupě1.Location = New System.Drawing.Point(12, 12)
        Me.Šoupě1.Max = CType(100, Short)
        Me.Šoupě1.Min = CType(0, Short)
        Me.Šoupě1.Name = "Šoupě1"
        Me.Šoupě1.Nápis = "První šoupě"
        Me.Šoupě1.Nápověda = ""
        Me.Šoupě1.ŠířkaTxt = 65
        Me.Šoupě1.Size = New System.Drawing.Size(203, 20)
        Me.Šoupě1.TabIndex = 0
        '
        'Šoupě2
        '
        Me.Šoupě2.Hodnota = 50
        Me.Šoupě2.Location = New System.Drawing.Point(12, 38)
        Me.Šoupě2.Max = CType(100, Short)
        Me.Šoupě2.Min = CType(0, Short)
        Me.Šoupě2.Name = "Šoupě2"
        Me.Šoupě2.Nápis = "Druhé šoupě"
        Me.Šoupě2.Nápověda = ""
        Me.Šoupě2.ŠířkaTxt = 70
        Me.Šoupě2.Size = New System.Drawing.Size(203, 20)
        Me.Šoupě2.TabIndex = 1
        '
        'Šoupě3
        '
        Me.Šoupě3.Hodnota = 10000
        Me.Šoupě3.Location = New System.Drawing.Point(12, 64)
        Me.Šoupě3.Max = CType(10000, Short)
        Me.Šoupě3.Min = CType(0, Short)
        Me.Šoupě3.Name = "Šoupě3"
        Me.Šoupě3.Nápis = "Třetí šoupě"
        Me.Šoupě3.Nápověda = ""
        Me.Šoupě3.ŠířkaTxt = 65
        Me.Šoupě3.Size = New System.Drawing.Size(203, 20)
        Me.Šoupě3.TabIndex = 2
        '
        'frmTestŠoupěte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.Šoupě3)
        Me.Controls.Add(Me.Šoupě2)
        Me.Controls.Add(Me.Šoupě1)
        Me.Name = "frmTestŠoupěte"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Šoupě1 As TestŠoupěte.Šoupě
    Friend WithEvents Šoupě2 As TestŠoupěte.Šoupě
    Friend WithEvents Šoupě3 As TestŠoupěte.Šoupě

End Class
