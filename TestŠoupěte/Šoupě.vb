Option Strict Off
Option Explicit On
Friend Class �oup�
	Inherits System.Windows.Forms.UserControl
    Private Sub txt��slo_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs)
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        Select Case KeyAscii
            Case Asc("0") To Asc("9")
                ' pust�me
            Case Else
                ' nepust�me
                KeyAscii = 0
        End Select
        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub
	
	
    <System.ComponentModel.Category("Behavior")> Property Max() As Short
        Get
            'UPGRADE_ISSUE: VBControlExtender method udPosun.Max was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            Max = udPosun.Maximum
        End Get
        Set(ByVal Value As Short)
            'UPGRADE_ISSUE: VBControlExtender method udPosun.Max was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            udPosun.Maximum = Value
        End Set
    End Property

    <System.ComponentModel.Category("Behavior")> Property Min() As Short
        Get
            Min = udPosun.Minimum
        End Get
        Set(ByVal Value As Short)
            udPosun.Minimum = Value
        End Set
    End Property
	
	
    <System.ComponentModel.Category("Behavior")> Shadows Property Enabled() As Boolean
        Get
            Enabled = udPosun.Enabled
        End Get
        Set(ByVal Value As Boolean)
            udPosun.Enabled = Value
            'txt��slo.Enabled = Value
        End Set
    End Property
	
	
    <System.ComponentModel.Category("Data")> Property Hodnota() As Double
        Get
            'UPGRADE_ISSUE: VBControlExtender method udPosun.Value was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            Hodnota = udPosun.Value
        End Get
        Set(ByVal Value As Double)
            If Value > Max Then Value = Max
            'UPGRADE_ISSUE: VBControlExtender method udPosun.Value was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="076C26E5-B7A9-4E77-B69C-B4448DF39E58"'
            udPosun.Value = Value
        End Set
    End Property


    Private pam���kaTxt As Integer
    <System.ComponentModel.Category("Appearance")> _
    <System.ComponentModel.DefaultValue(40)> _
    Public Property ���kaTxt() As Integer
        Get
            Return pam���kaTxt
        End Get
        Set(ByVal value As Integer)
            pam���kaTxt = value
            Rozm��()
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> Public Property N�pis() As String
        Get
            Return lblN�pis.Text
        End Get
        Set(ByVal Value As String)
            lblN�pis.Text = Value
        End Set
    End Property

    <System.ComponentModel.Category("Appearance")> Public Property N�pov�da() As String
        Get
            Return TT.GetToolTip(Me)
        End Get
        Set(ByVal value As String)
            TT.SetToolTip(Me, value)
            TT.SetToolTip(lblN�pis, value)
            TT.SetToolTip(udPosun, value)
        End Set
    End Property

    Private Sub Rozm��()
        lblN�pis.Location = ClientRectangle.Location
        lblN�pis.Width = pam���kaTxt
        lblN�pis.Height = ClientSize.Height
        udPosun.Left = pam���kaTxt
        udPosun.Width = Me.ClientSize.Width - pam���kaTxt
    End Sub

    Private Sub �oup�_Layout(ByVal sender As Object, ByVal e As System.Windows.Forms.LayoutEventArgs) Handles Me.Layout
        Rozm��()
    End Sub
	
	Private Sub �oup�_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        Rozm��()
    End Sub

End Class