<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmEpidemia
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents UkažVýběr As System.Windows.Forms.CheckBox
	Public CDOpen As System.Windows.Forms.OpenFileDialog
	Public CDSave As System.Windows.Forms.SaveFileDialog
	Public WithEvents ilEp As System.Windows.Forms.ImageList
	Public WithEvents tlZavřít As System.Windows.Forms.Button
	Public WithEvents tlZastavit As System.Windows.Forms.Button
	Public WithEvents tlSpustit As System.Windows.Forms.Button
	Public WithEvents tlUložit As System.Windows.Forms.Button
	Public WithEvents tlOtevřít As System.Windows.Forms.Button
	Public WithEvents tlSmazat As System.Windows.Forms.Button
	Public WithEvents tlZměnit As System.Windows.Forms.Button
	Public WithEvents tlDozadu As System.Windows.Forms.Button
	Public WithEvents tlDopředu As System.Windows.Forms.Button
	Public WithEvents tlNáhY As System.Windows.Forms.Button
	Public WithEvents tlNáhX As System.Windows.Forms.Button
	Public WithEvents tlStředY As System.Windows.Forms.Button
	Public WithEvents tlStředX As System.Windows.Forms.Button
	Public WithEvents šouLinX As Šoupě
	Public WithEvents přepLinia As Přepínač
	Public WithEvents šouLinY As Šoupě
	Public WithEvents fraLinia As System.Windows.Forms.GroupBox
    Public WithEvents přepSměrová As Přepínač
    Public WithEvents fraSměrová As System.Windows.Forms.GroupBox
    Public WithEvents cmbMutace As System.Windows.Forms.ComboBox
    Public WithEvents fraMutace As System.Windows.Forms.GroupBox
    Public WithEvents přepÚplNáhPočSous As Přepínač
    Public WithEvents přepNáhPočSous As Přepínač
    Public WithEvents přepDržetSměr As Přepínač
    Public WithEvents _cmbSmRozptyl_0 As System.Windows.Forms.ComboBox
    Public WithEvents šouPočetVybranýchSous As Šoupě
    Public WithEvents cmbPořadíSousedů As System.Windows.Forms.ComboBox
    Public WithEvents cmbSousedé As System.Windows.Forms.ComboBox
    Public WithEvents _cmbSmRozptyl_1 As System.Windows.Forms.ComboBox
    Public WithEvents lblSmRozptyl As System.Windows.Forms.Label
    Public WithEvents lblPořadíSousedů As System.Windows.Forms.Label
    Public WithEvents lblSousedé As System.Windows.Forms.Label
    Public WithEvents fraSousedé As System.Windows.Forms.GroupBox
    Public WithEvents přepPoužítOhniska As Přepínač
    Public WithEvents přepPředatVše As Přepínač
    Public WithEvents přepNavázatNaMinulou As Přepínač
    Public WithEvents přepNáhHodOh As Přepínač
    Public WithEvents šouHodOh As Šoupě
    Public WithEvents přepNáhPočOh As Přepínač
    Public WithEvents šouPočetOh As Šoupě
    Public WithEvents fraOhniska As System.Windows.Forms.GroupBox
    Public WithEvents lvEp As System.Windows.Forms.ListView
    Public WithEvents přepVyčistit As Přepínač
    Public WithEvents přepDlaždice As Přepínač
    Public WithEvents přepPřekrývání As Přepínač
    Public WithEvents šouPočetVrstev As Šoupě
    Public WithEvents fraRůzné As System.Windows.Forms.GroupBox
    Public WithEvents tlPřidat As System.Windows.Forms.Button
    Public WithEvents cmbSmRozptyl As Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray
    Public WithEvents přepSm As PřepínačArray
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEpidemia))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tlSmazat = New System.Windows.Forms.Button()
        Me.tlZměnit = New System.Windows.Forms.Button()
        Me.tlDozadu = New System.Windows.Forms.Button()
        Me.tlDopředu = New System.Windows.Forms.Button()
        Me.šouPočetVybranýchSous = New Fraktálník.Šoupě()
        Me.šouPočetVrstev = New Fraktálník.Šoupě()
        Me.tlPřidat = New System.Windows.Forms.Button()
        Me.UkažVýběr = New System.Windows.Forms.CheckBox()
        Me.CDOpen = New System.Windows.Forms.OpenFileDialog()
        Me.CDSave = New System.Windows.Forms.SaveFileDialog()
        Me.ilEp = New System.Windows.Forms.ImageList(Me.components)
        Me.tlZavřít = New System.Windows.Forms.Button()
        Me.tlZastavit = New System.Windows.Forms.Button()
        Me.tlSpustit = New System.Windows.Forms.Button()
        Me.tlUložit = New System.Windows.Forms.Button()
        Me.tlOtevřít = New System.Windows.Forms.Button()
        Me.fraLinia = New System.Windows.Forms.GroupBox()
        Me.tlNáhY = New System.Windows.Forms.Button()
        Me.tlNáhX = New System.Windows.Forms.Button()
        Me.tlStředY = New System.Windows.Forms.Button()
        Me.tlStředX = New System.Windows.Forms.Button()
        Me.šouLinX = New Fraktálník.Šoupě()
        Me.přepLinia = New Fraktálník.Přepínač()
        Me.šouLinY = New Fraktálník.Šoupě()
        Me.fraSměrová = New System.Windows.Forms.GroupBox()
        Me.Směrovka = New Fraktálník.Směrovka()
        Me.přepSměrová = New Fraktálník.Přepínač()
        Me.fraMutace = New System.Windows.Forms.GroupBox()
        Me.cmbMutace = New System.Windows.Forms.ComboBox()
        Me.fraSousedé = New System.Windows.Forms.GroupBox()
        Me.přepÚplNáhPočSous = New Fraktálník.Přepínač()
        Me.přepNáhPočSous = New Fraktálník.Přepínač()
        Me.přepDržetSměr = New Fraktálník.Přepínač()
        Me._cmbSmRozptyl_0 = New System.Windows.Forms.ComboBox()
        Me.cmbPořadíSousedů = New System.Windows.Forms.ComboBox()
        Me.cmbSousedé = New System.Windows.Forms.ComboBox()
        Me._cmbSmRozptyl_1 = New System.Windows.Forms.ComboBox()
        Me.lblSmRozptyl = New System.Windows.Forms.Label()
        Me.lblPořadíSousedů = New System.Windows.Forms.Label()
        Me.lblSousedé = New System.Windows.Forms.Label()
        Me.fraOhniska = New System.Windows.Forms.GroupBox()
        Me.přepPřidatRám = New Fraktálník.Přepínač()
        Me.přepPoužítOhniska = New Fraktálník.Přepínač()
        Me.přepPředatVše = New Fraktálník.Přepínač()
        Me.přepNavázatNaMinulou = New Fraktálník.Přepínač()
        Me.přepNáhHodOh = New Fraktálník.Přepínač()
        Me.šouHodOh = New Fraktálník.Šoupě()
        Me.přepNáhPočOh = New Fraktálník.Přepínač()
        Me.šouPočetOh = New Fraktálník.Šoupě()
        Me.lvEp = New System.Windows.Forms.ListView()
        Me.fraRůzné = New System.Windows.Forms.GroupBox()
        Me.přepVyčistit = New Fraktálník.Přepínač()
        Me.přepDlaždice = New Fraktálník.Přepínač()
        Me.přepPřekrývání = New Fraktálník.Přepínač()
        Me.cmbSmRozptyl = New Microsoft.VisualBasic.Compatibility.VB6.ComboBoxArray(Me.components)
        Me.přepSm = New Fraktálník.PřepínačArray(Me.components)
        Me.fraLinia.SuspendLayout()
        Me.fraSměrová.SuspendLayout()
        Me.fraMutace.SuspendLayout()
        Me.fraSousedé.SuspendLayout()
        Me.fraOhniska.SuspendLayout()
        Me.fraRůzné.SuspendLayout()
        CType(Me.cmbSmRozptyl, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.přepSm, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tlSmazat
        '
        Me.tlSmazat.BackColor = System.Drawing.SystemColors.Control
        Me.tlSmazat.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlSmazat.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.tlSmazat.Font = New System.Drawing.Font("Symbol", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.tlSmazat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlSmazat.Location = New System.Drawing.Point(728, 168)
        Me.tlSmazat.Name = "tlSmazat"
        Me.tlSmazat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlSmazat.Size = New System.Drawing.Size(33, 33)
        Me.tlSmazat.TabIndex = 46
        Me.tlSmazat.Text = "´"
        Me.ToolTip1.SetToolTip(Me.tlSmazat, "Smazat")
        Me.tlSmazat.UseVisualStyleBackColor = False
        '
        'tlZměnit
        '
        Me.tlZměnit.BackColor = System.Drawing.SystemColors.Control
        Me.tlZměnit.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlZměnit.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.tlZměnit.Font = New System.Drawing.Font("Symbol", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.tlZměnit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlZměnit.Location = New System.Drawing.Point(760, 136)
        Me.tlZměnit.Name = "tlZměnit"
        Me.tlZměnit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlZměnit.Size = New System.Drawing.Size(33, 33)
        Me.tlZměnit.TabIndex = 44
        Me.tlZměnit.Text = "ż"
        Me.ToolTip1.SetToolTip(Me.tlZměnit, "Změnit")
        Me.tlZměnit.UseVisualStyleBackColor = False
        '
        'tlDozadu
        '
        Me.tlDozadu.BackColor = System.Drawing.SystemColors.Control
        Me.tlDozadu.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlDozadu.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.tlDozadu.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.tlDozadu.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlDozadu.Location = New System.Drawing.Point(688, 168)
        Me.tlDozadu.Name = "tlDozadu"
        Me.tlDozadu.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlDozadu.Size = New System.Drawing.Size(33, 33)
        Me.tlDozadu.TabIndex = 43
        Me.tlDozadu.Text = "↓"
        Me.ToolTip1.SetToolTip(Me.tlDozadu, "Upozadit")
        Me.tlDozadu.UseVisualStyleBackColor = False
        '
        'tlDopředu
        '
        Me.tlDopředu.BackColor = System.Drawing.SystemColors.Control
        Me.tlDopředu.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlDopředu.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.tlDopředu.Font = New System.Drawing.Font("Times New Roman", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.tlDopředu.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlDopředu.Location = New System.Drawing.Point(688, 136)
        Me.tlDopředu.Name = "tlDopředu"
        Me.tlDopředu.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlDopředu.Size = New System.Drawing.Size(33, 33)
        Me.tlDopředu.TabIndex = 42
        Me.tlDopředu.Text = "↑"
        Me.ToolTip1.SetToolTip(Me.tlDopředu, "Upřednostnit")
        Me.tlDopředu.UseVisualStyleBackColor = False
        '
        'šouPočetVybranýchSous
        '
        Me.šouPočetVybranýchSous.Hodnota = 0.0R
        Me.šouPočetVybranýchSous.Location = New System.Drawing.Point(8, 160)
        Me.šouPočetVybranýchSous.Max = CType(8, Short)
        Me.šouPočetVybranýchSous.Min = CType(0, Short)
        Me.šouPočetVybranýchSous.Name = "šouPočetVybranýchSous"
        Me.šouPočetVybranýchSous.Nápis = "Počet vybr."
        Me.šouPočetVybranýchSous.Nápověda = ""
        Me.šouPočetVybranýchSous.ŠířkaTxt = 70
        Me.šouPočetVybranýchSous.Size = New System.Drawing.Size(129, 20)
        Me.šouPočetVybranýchSous.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.šouPočetVybranýchSous, "0 => žádné nevynechat")
        '
        'šouPočetVrstev
        '
        Me.šouPočetVrstev.Hodnota = 0.0R
        Me.šouPočetVrstev.Location = New System.Drawing.Point(6, 14)
        Me.šouPočetVrstev.Max = CType(32767, Short)
        Me.šouPočetVrstev.Min = CType(0, Short)
        Me.šouPočetVrstev.Name = "šouPočetVrstev"
        Me.šouPočetVrstev.Nápis = "Počet vrstev"
        Me.šouPočetVrstev.Nápověda = ""
        Me.šouPočetVrstev.ŠířkaTxt = 70
        Me.šouPočetVrstev.Size = New System.Drawing.Size(137, 20)
        Me.šouPočetVrstev.TabIndex = 21
        Me.ToolTip1.SetToolTip(Me.šouPočetVrstev, "0 => neomezený počet")
        '
        'tlPřidat
        '
        Me.tlPřidat.BackColor = System.Drawing.SystemColors.Control
        Me.tlPřidat.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlPřidat.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.tlPřidat.Font = New System.Drawing.Font("Symbol", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me.tlPřidat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlPřidat.Location = New System.Drawing.Point(728, 136)
        Me.tlPřidat.Name = "tlPřidat"
        Me.tlPřidat.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlPřidat.Size = New System.Drawing.Size(33, 33)
        Me.tlPřidat.TabIndex = 45
        Me.tlPřidat.Text = "+"
        Me.ToolTip1.SetToolTip(Me.tlPřidat, "Přidat")
        Me.tlPřidat.UseVisualStyleBackColor = False
        '
        'UkažVýběr
        '
        Me.UkažVýběr.BackColor = System.Drawing.SystemColors.Control
        Me.UkažVýběr.Cursor = System.Windows.Forms.Cursors.Default
        Me.UkažVýběr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.UkažVýběr.Location = New System.Drawing.Point(800, 72)
        Me.UkažVýběr.Name = "UkažVýběr"
        Me.UkažVýběr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.UkažVýběr.Size = New System.Drawing.Size(73, 17)
        Me.UkažVýběr.TabIndex = 58
        Me.UkažVýběr.Text = "Uk. výběr"
        Me.UkažVýběr.UseVisualStyleBackColor = False
        '
        'ilEp
        '
        Me.ilEp.ImageStream = CType(resources.GetObject("ilEp.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ilEp.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ilEp.Images.SetKeyName(0, "")
        '
        'tlZavřít
        '
        Me.tlZavřít.BackColor = System.Drawing.SystemColors.Control
        Me.tlZavřít.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlZavřít.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.tlZavřít.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.tlZavřít.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlZavřít.Location = New System.Drawing.Point(800, 176)
        Me.tlZavřít.Name = "tlZavřít"
        Me.tlZavřít.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlZavřít.Size = New System.Drawing.Size(73, 25)
        Me.tlZavřít.TabIndex = 51
        Me.tlZavřít.Text = "Zavřít"
        Me.tlZavřít.UseVisualStyleBackColor = False
        '
        'tlZastavit
        '
        Me.tlZastavit.BackColor = System.Drawing.SystemColors.Control
        Me.tlZastavit.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlZastavit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.tlZastavit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlZastavit.Location = New System.Drawing.Point(800, 128)
        Me.tlZastavit.Name = "tlZastavit"
        Me.tlZastavit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlZastavit.Size = New System.Drawing.Size(73, 25)
        Me.tlZastavit.TabIndex = 50
        Me.tlZastavit.Text = "Zastavit"
        Me.tlZastavit.UseVisualStyleBackColor = False
        '
        'tlSpustit
        '
        Me.tlSpustit.BackColor = System.Drawing.SystemColors.Control
        Me.tlSpustit.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlSpustit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.tlSpustit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlSpustit.Location = New System.Drawing.Point(800, 96)
        Me.tlSpustit.Name = "tlSpustit"
        Me.tlSpustit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlSpustit.Size = New System.Drawing.Size(73, 25)
        Me.tlSpustit.TabIndex = 49
        Me.tlSpustit.Text = "Spustit"
        Me.tlSpustit.UseVisualStyleBackColor = False
        '
        'tlUložit
        '
        Me.tlUložit.BackColor = System.Drawing.SystemColors.Control
        Me.tlUložit.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlUložit.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.tlUložit.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlUložit.Location = New System.Drawing.Point(800, 40)
        Me.tlUložit.Name = "tlUložit"
        Me.tlUložit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlUložit.Size = New System.Drawing.Size(73, 25)
        Me.tlUložit.TabIndex = 48
        Me.tlUložit.Text = "&Uložit"
        Me.tlUložit.UseVisualStyleBackColor = False
        '
        'tlOtevřít
        '
        Me.tlOtevřít.BackColor = System.Drawing.SystemColors.Control
        Me.tlOtevřít.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlOtevřít.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.tlOtevřít.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlOtevřít.Location = New System.Drawing.Point(800, 8)
        Me.tlOtevřít.Name = "tlOtevřít"
        Me.tlOtevřít.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlOtevřít.Size = New System.Drawing.Size(73, 25)
        Me.tlOtevřít.TabIndex = 47
        Me.tlOtevřít.Text = "&Otevřít"
        Me.tlOtevřít.UseVisualStyleBackColor = False
        '
        'fraLinia
        '
        Me.fraLinia.BackColor = System.Drawing.SystemColors.Control
        Me.fraLinia.Controls.Add(Me.tlNáhY)
        Me.fraLinia.Controls.Add(Me.tlNáhX)
        Me.fraLinia.Controls.Add(Me.tlStředY)
        Me.fraLinia.Controls.Add(Me.tlStředX)
        Me.fraLinia.Controls.Add(Me.šouLinX)
        Me.fraLinia.Controls.Add(Me.přepLinia)
        Me.fraLinia.Controls.Add(Me.šouLinY)
        Me.fraLinia.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraLinia.Location = New System.Drawing.Point(616, 7)
        Me.fraLinia.Name = "fraLinia"
        Me.fraLinia.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraLinia.Size = New System.Drawing.Size(177, 113)
        Me.fraLinia.TabIndex = 36
        Me.fraLinia.TabStop = False
        '
        'tlNáhY
        '
        Me.tlNáhY.BackColor = System.Drawing.SystemColors.Control
        Me.tlNáhY.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlNáhY.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlNáhY.Location = New System.Drawing.Point(112, 88)
        Me.tlNáhY.Name = "tlNáhY"
        Me.tlNáhY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlNáhY.Size = New System.Drawing.Size(57, 22)
        Me.tlNáhY.TabIndex = 57
        Me.tlNáhY.Text = "Náhoda"
        Me.tlNáhY.UseVisualStyleBackColor = False
        '
        'tlNáhX
        '
        Me.tlNáhX.BackColor = System.Drawing.SystemColors.Control
        Me.tlNáhX.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlNáhX.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlNáhX.Location = New System.Drawing.Point(112, 40)
        Me.tlNáhX.Name = "tlNáhX"
        Me.tlNáhX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlNáhX.Size = New System.Drawing.Size(57, 22)
        Me.tlNáhX.TabIndex = 56
        Me.tlNáhX.Text = "Náhoda"
        Me.tlNáhX.UseVisualStyleBackColor = False
        '
        'tlStředY
        '
        Me.tlStředY.BackColor = System.Drawing.SystemColors.Control
        Me.tlStředY.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlStředY.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlStředY.Location = New System.Drawing.Point(128, 64)
        Me.tlStředY.Name = "tlStředY"
        Me.tlStředY.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlStředY.Size = New System.Drawing.Size(41, 22)
        Me.tlStředY.TabIndex = 41
        Me.tlStředY.Text = "Střed"
        Me.tlStředY.UseVisualStyleBackColor = False
        '
        'tlStředX
        '
        Me.tlStředX.BackColor = System.Drawing.SystemColors.Control
        Me.tlStředX.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlStředX.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlStředX.Location = New System.Drawing.Point(128, 16)
        Me.tlStředX.Name = "tlStředX"
        Me.tlStředX.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlStředX.Size = New System.Drawing.Size(41, 22)
        Me.tlStředX.TabIndex = 40
        Me.tlStředX.Text = "Střed"
        Me.tlStředX.UseVisualStyleBackColor = False
        '
        'šouLinX
        '
        Me.šouLinX.Hodnota = 0.0R
        Me.šouLinX.Location = New System.Drawing.Point(8, 16)
        Me.šouLinX.Max = CType(32767, Short)
        Me.šouLinX.Min = CType(0, Short)
        Me.šouLinX.Name = "šouLinX"
        Me.šouLinX.Nápis = "Levobok"
        Me.šouLinX.Nápověda = ""
        Me.šouLinX.ŠířkaTxt = 70
        Me.šouLinX.Size = New System.Drawing.Size(113, 20)
        Me.šouLinX.TabIndex = 38
        '
        'přepLinia
        '
        Me.přepLinia.Caption = "Linia"
        Me.přepLinia.Hodnota = False
        Me.přepLinia.Location = New System.Drawing.Point(8, 0)
        Me.přepLinia.Name = "přepLinia"
        Me.přepLinia.Size = New System.Drawing.Size(48, 17)
        Me.přepLinia.TabIndex = 37
        Me.přepLinia.ToolTipText = ""
        '
        'šouLinY
        '
        Me.šouLinY.Hodnota = 0.0R
        Me.šouLinY.Location = New System.Drawing.Point(8, 64)
        Me.šouLinY.Max = CType(32767, Short)
        Me.šouLinY.Min = CType(0, Short)
        Me.šouLinY.Name = "šouLinY"
        Me.šouLinY.Nápis = "Vršek"
        Me.šouLinY.Nápověda = ""
        Me.šouLinY.ŠířkaTxt = 70
        Me.šouLinY.Size = New System.Drawing.Size(113, 20)
        Me.šouLinY.TabIndex = 39
        '
        'fraSměrová
        '
        Me.fraSměrová.BackColor = System.Drawing.SystemColors.Control
        Me.fraSměrová.Controls.Add(Me.Směrovka)
        Me.fraSměrová.Controls.Add(Me.přepSměrová)
        Me.fraSměrová.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraSměrová.Location = New System.Drawing.Point(616, 128)
        Me.fraSměrová.Name = "fraSměrová"
        Me.fraSměrová.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSměrová.Size = New System.Drawing.Size(62, 73)
        Me.fraSměrová.TabIndex = 26
        Me.fraSměrová.TabStop = False
        '
        'Směrovka
        '
        Me.Směrovka.Location = New System.Drawing.Point(6, 17)
        Me.Směrovka.Name = "Směrovka"
        Me.Směrovka.Size = New System.Drawing.Size(51, 52)
        Me.Směrovka.TabIndex = 28
        Me.Směrovka.VšechnySm = CType(0, Short)
        '
        'přepSměrová
        '
        Me.přepSměrová.Caption = "Směrová"
        Me.přepSměrová.Hodnota = False
        Me.přepSměrová.Location = New System.Drawing.Point(0, 0)
        Me.přepSměrová.Name = "přepSměrová"
        Me.přepSměrová.Size = New System.Drawing.Size(68, 17)
        Me.přepSměrová.TabIndex = 27
        Me.přepSměrová.ToolTipText = ""
        '
        'fraMutace
        '
        Me.fraMutace.BackColor = System.Drawing.SystemColors.Control
        Me.fraMutace.Controls.Add(Me.cmbMutace)
        Me.fraMutace.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraMutace.Location = New System.Drawing.Point(456, 152)
        Me.fraMutace.Name = "fraMutace"
        Me.fraMutace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraMutace.Size = New System.Drawing.Size(153, 49)
        Me.fraMutace.TabIndex = 15
        Me.fraMutace.TabStop = False
        Me.fraMutace.Text = "Mutace"
        '
        'cmbMutace
        '
        Me.cmbMutace.BackColor = System.Drawing.SystemColors.Window
        Me.cmbMutace.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbMutace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbMutace.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbMutace.Location = New System.Drawing.Point(8, 16)
        Me.cmbMutace.Name = "cmbMutace"
        Me.cmbMutace.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbMutace.Size = New System.Drawing.Size(137, 21)
        Me.cmbMutace.TabIndex = 16
        '
        'fraSousedé
        '
        Me.fraSousedé.BackColor = System.Drawing.SystemColors.Control
        Me.fraSousedé.Controls.Add(Me.přepÚplNáhPočSous)
        Me.fraSousedé.Controls.Add(Me.přepNáhPočSous)
        Me.fraSousedé.Controls.Add(Me.přepDržetSměr)
        Me.fraSousedé.Controls.Add(Me._cmbSmRozptyl_0)
        Me.fraSousedé.Controls.Add(Me.šouPočetVybranýchSous)
        Me.fraSousedé.Controls.Add(Me.cmbPořadíSousedů)
        Me.fraSousedé.Controls.Add(Me.cmbSousedé)
        Me.fraSousedé.Controls.Add(Me._cmbSmRozptyl_1)
        Me.fraSousedé.Controls.Add(Me.lblSmRozptyl)
        Me.fraSousedé.Controls.Add(Me.lblPořadíSousedů)
        Me.fraSousedé.Controls.Add(Me.lblSousedé)
        Me.fraSousedé.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraSousedé.Location = New System.Drawing.Point(216, 8)
        Me.fraSousedé.Name = "fraSousedé"
        Me.fraSousedé.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraSousedé.Size = New System.Drawing.Size(233, 193)
        Me.fraSousedé.TabIndex = 8
        Me.fraSousedé.TabStop = False
        Me.fraSousedé.Text = "Sousedé"
        '
        'přepÚplNáhPočSous
        '
        Me.přepÚplNáhPočSous.Caption = "Úpl. náh. p."
        Me.přepÚplNáhPočSous.Hodnota = False
        Me.přepÚplNáhPočSous.Location = New System.Drawing.Point(144, 168)
        Me.přepÚplNáhPočSous.Name = "přepÚplNáhPočSous"
        Me.přepÚplNáhPočSous.Size = New System.Drawing.Size(81, 17)
        Me.přepÚplNáhPočSous.TabIndex = 53
        Me.přepÚplNáhPočSous.ToolTipText = ""
        '
        'přepNáhPočSous
        '
        Me.přepNáhPočSous.Caption = "Náh. poč."
        Me.přepNáhPočSous.Hodnota = False
        Me.přepNáhPočSous.Location = New System.Drawing.Point(144, 152)
        Me.přepNáhPočSous.Name = "přepNáhPočSous"
        Me.přepNáhPočSous.Size = New System.Drawing.Size(81, 17)
        Me.přepNáhPočSous.TabIndex = 52
        Me.přepNáhPočSous.ToolTipText = ""
        '
        'přepDržetSměr
        '
        Me.přepDržetSměr.Caption = "Držet směr"
        Me.přepDržetSměr.Hodnota = False
        Me.přepDržetSměr.Location = New System.Drawing.Point(8, 136)
        Me.přepDržetSměr.Name = "přepDržetSměr"
        Me.přepDržetSměr.Size = New System.Drawing.Size(153, 17)
        Me.přepDržetSměr.TabIndex = 14
        Me.přepDržetSměr.ToolTipText = ""
        '
        '_cmbSmRozptyl_0
        '
        Me._cmbSmRozptyl_0.BackColor = System.Drawing.SystemColors.Window
        Me._cmbSmRozptyl_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._cmbSmRozptyl_0.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._cmbSmRozptyl_0.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbSmRozptyl.SetIndex(Me._cmbSmRozptyl_0, CType(0, Short))
        Me._cmbSmRozptyl_0.Location = New System.Drawing.Point(8, 112)
        Me._cmbSmRozptyl_0.Name = "_cmbSmRozptyl_0"
        Me._cmbSmRozptyl_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._cmbSmRozptyl_0.Size = New System.Drawing.Size(217, 21)
        Me._cmbSmRozptyl_0.TabIndex = 12
        '
        'cmbPořadíSousedů
        '
        Me.cmbPořadíSousedů.BackColor = System.Drawing.SystemColors.Window
        Me.cmbPořadíSousedů.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbPořadíSousedů.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbPořadíSousedů.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbPořadíSousedů.Location = New System.Drawing.Point(8, 72)
        Me.cmbPořadíSousedů.Name = "cmbPořadíSousedů"
        Me.cmbPořadíSousedů.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbPořadíSousedů.Size = New System.Drawing.Size(217, 21)
        Me.cmbPořadíSousedů.TabIndex = 10
        '
        'cmbSousedé
        '
        Me.cmbSousedé.BackColor = System.Drawing.SystemColors.Window
        Me.cmbSousedé.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmbSousedé.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmbSousedé.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbSousedé.Location = New System.Drawing.Point(8, 32)
        Me.cmbSousedé.Name = "cmbSousedé"
        Me.cmbSousedé.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmbSousedé.Size = New System.Drawing.Size(217, 21)
        Me.cmbSousedé.TabIndex = 9
        '
        '_cmbSmRozptyl_1
        '
        Me._cmbSmRozptyl_1.BackColor = System.Drawing.SystemColors.Window
        Me._cmbSmRozptyl_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._cmbSmRozptyl_1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._cmbSmRozptyl_1.ForeColor = System.Drawing.SystemColors.WindowText
        Me.cmbSmRozptyl.SetIndex(Me._cmbSmRozptyl_1, CType(1, Short))
        Me._cmbSmRozptyl_1.Location = New System.Drawing.Point(8, 112)
        Me._cmbSmRozptyl_1.Name = "_cmbSmRozptyl_1"
        Me._cmbSmRozptyl_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._cmbSmRozptyl_1.Size = New System.Drawing.Size(217, 21)
        Me._cmbSmRozptyl_1.TabIndex = 13
        '
        'lblSmRozptyl
        '
        Me.lblSmRozptyl.BackColor = System.Drawing.SystemColors.Control
        Me.lblSmRozptyl.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSmRozptyl.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSmRozptyl.Location = New System.Drawing.Point(8, 96)
        Me.lblSmRozptyl.Name = "lblSmRozptyl"
        Me.lblSmRozptyl.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSmRozptyl.Size = New System.Drawing.Size(121, 17)
        Me.lblSmRozptyl.TabIndex = 19
        Me.lblSmRozptyl.Text = "Rozptyl směru:"
        '
        'lblPořadíSousedů
        '
        Me.lblPořadíSousedů.BackColor = System.Drawing.SystemColors.Control
        Me.lblPořadíSousedů.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPořadíSousedů.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPořadíSousedů.Location = New System.Drawing.Point(8, 56)
        Me.lblPořadíSousedů.Name = "lblPořadíSousedů"
        Me.lblPořadíSousedů.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPořadíSousedů.Size = New System.Drawing.Size(121, 17)
        Me.lblPořadíSousedů.TabIndex = 18
        Me.lblPořadíSousedů.Text = "Pořadí sousedů:"
        '
        'lblSousedé
        '
        Me.lblSousedé.BackColor = System.Drawing.SystemColors.Control
        Me.lblSousedé.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSousedé.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSousedé.Location = New System.Drawing.Point(8, 16)
        Me.lblSousedé.Name = "lblSousedé"
        Me.lblSousedé.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSousedé.Size = New System.Drawing.Size(121, 17)
        Me.lblSousedé.TabIndex = 17
        Me.lblSousedé.Text = "Výběr sousedů:"
        '
        'fraOhniska
        '
        Me.fraOhniska.BackColor = System.Drawing.SystemColors.Control
        Me.fraOhniska.Controls.Add(Me.přepPřidatRám)
        Me.fraOhniska.Controls.Add(Me.přepPoužítOhniska)
        Me.fraOhniska.Controls.Add(Me.přepPředatVše)
        Me.fraOhniska.Controls.Add(Me.přepNavázatNaMinulou)
        Me.fraOhniska.Controls.Add(Me.přepNáhHodOh)
        Me.fraOhniska.Controls.Add(Me.šouHodOh)
        Me.fraOhniska.Controls.Add(Me.přepNáhPočOh)
        Me.fraOhniska.Controls.Add(Me.šouPočetOh)
        Me.fraOhniska.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraOhniska.Location = New System.Drawing.Point(8, 8)
        Me.fraOhniska.Name = "fraOhniska"
        Me.fraOhniska.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraOhniska.Size = New System.Drawing.Size(201, 193)
        Me.fraOhniska.TabIndex = 1
        Me.fraOhniska.TabStop = False
        Me.fraOhniska.Text = "Ohniska"
        '
        'přepPřidatRám
        '
        Me.přepPřidatRám.Caption = "Přidat rám"
        Me.přepPřidatRám.Hodnota = False
        Me.přepPřidatRám.Location = New System.Drawing.Point(109, 160)
        Me.přepPřidatRám.Name = "přepPřidatRám"
        Me.přepPřidatRám.Size = New System.Drawing.Size(84, 17)
        Me.přepPřidatRám.TabIndex = 55
        Me.přepPřidatRám.ToolTipText = ""
        '
        'přepPoužítOhniska
        '
        Me.přepPoužítOhniska.Caption = "Použít ohniska"
        Me.přepPoužítOhniska.Hodnota = False
        Me.přepPoužítOhniska.Location = New System.Drawing.Point(8, 40)
        Me.přepPoužítOhniska.Name = "přepPoužítOhniska"
        Me.přepPoužítOhniska.Size = New System.Drawing.Size(185, 17)
        Me.přepPoužítOhniska.TabIndex = 54
        Me.přepPoužítOhniska.ToolTipText = ""
        '
        'přepPředatVše
        '
        Me.přepPředatVše.Caption = "Předat vše"
        Me.přepPředatVše.Hodnota = False
        Me.přepPředatVše.Location = New System.Drawing.Point(8, 160)
        Me.přepPředatVše.Name = "přepPředatVše"
        Me.přepPředatVše.Size = New System.Drawing.Size(185, 17)
        Me.přepPředatVše.TabIndex = 7
        Me.přepPředatVše.ToolTipText = ""
        '
        'přepNavázatNaMinulou
        '
        Me.přepNavázatNaMinulou.Caption = "Navázat na minulou epidemii"
        Me.přepNavázatNaMinulou.Hodnota = False
        Me.přepNavázatNaMinulou.Location = New System.Drawing.Point(8, 16)
        Me.přepNavázatNaMinulou.Name = "přepNavázatNaMinulou"
        Me.přepNavázatNaMinulou.Size = New System.Drawing.Size(177, 17)
        Me.přepNavázatNaMinulou.TabIndex = 6
        Me.přepNavázatNaMinulou.ToolTipText = ""
        '
        'přepNáhHodOh
        '
        Me.přepNáhHodOh.Caption = "Náhodná hodnota ohnisek"
        Me.přepNáhHodOh.Hodnota = False
        Me.přepNáhHodOh.Location = New System.Drawing.Point(24, 136)
        Me.přepNáhHodOh.Name = "přepNáhHodOh"
        Me.přepNáhHodOh.Size = New System.Drawing.Size(169, 17)
        Me.přepNáhHodOh.TabIndex = 5
        Me.přepNáhHodOh.ToolTipText = ""
        '
        'šouHodOh
        '
        Me.šouHodOh.Hodnota = 0.0R
        Me.šouHodOh.Location = New System.Drawing.Point(24, 112)
        Me.šouHodOh.Max = CType(100, Short)
        Me.šouHodOh.Min = CType(0, Short)
        Me.šouHodOh.Name = "šouHodOh"
        Me.šouHodOh.Nápis = "Hodnota"
        Me.šouHodOh.Nápověda = ""
        Me.šouHodOh.ŠířkaTxt = 70
        Me.šouHodOh.Size = New System.Drawing.Size(169, 20)
        Me.šouHodOh.TabIndex = 4
        '
        'přepNáhPočOh
        '
        Me.přepNáhPočOh.Caption = "Náhodný počet ohnisek"
        Me.přepNáhPočOh.Hodnota = False
        Me.přepNáhPočOh.Location = New System.Drawing.Point(24, 88)
        Me.přepNáhPočOh.Name = "přepNáhPočOh"
        Me.přepNáhPočOh.Size = New System.Drawing.Size(169, 17)
        Me.přepNáhPočOh.TabIndex = 3
        Me.přepNáhPočOh.ToolTipText = ""
        '
        'šouPočetOh
        '
        Me.šouPočetOh.Hodnota = 0.0R
        Me.šouPočetOh.Location = New System.Drawing.Point(24, 64)
        Me.šouPočetOh.Max = CType(100, Short)
        Me.šouPočetOh.Min = CType(0, Short)
        Me.šouPočetOh.Name = "šouPočetOh"
        Me.šouPočetOh.Nápis = "Počet"
        Me.šouPočetOh.Nápověda = ""
        Me.šouPočetOh.ŠířkaTxt = 70
        Me.šouPočetOh.Size = New System.Drawing.Size(169, 20)
        Me.šouPočetOh.TabIndex = 2
        '
        'lvEp
        '
        Me.lvEp.BackColor = System.Drawing.SystemColors.Window
        Me.lvEp.Font = New System.Drawing.Font("Times New Roman", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(238, Byte))
        Me.lvEp.ForeColor = System.Drawing.SystemColors.WindowText
        Me.lvEp.FullRowSelect = True
        Me.lvEp.HideSelection = False
        Me.lvEp.LargeImageList = Me.ilEp
        Me.lvEp.Location = New System.Drawing.Point(8, 208)
        Me.lvEp.Name = "lvEp"
        Me.lvEp.Size = New System.Drawing.Size(865, 273)
        Me.lvEp.SmallImageList = Me.ilEp
        Me.lvEp.TabIndex = 0
        Me.lvEp.UseCompatibleStateImageBehavior = False
        Me.lvEp.View = System.Windows.Forms.View.Details
        '
        'fraRůzné
        '
        Me.fraRůzné.BackColor = System.Drawing.SystemColors.Control
        Me.fraRůzné.Controls.Add(Me.přepVyčistit)
        Me.fraRůzné.Controls.Add(Me.přepDlaždice)
        Me.fraRůzné.Controls.Add(Me.přepPřekrývání)
        Me.fraRůzné.Controls.Add(Me.šouPočetVrstev)
        Me.fraRůzné.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraRůzné.Location = New System.Drawing.Point(456, 8)
        Me.fraRůzné.Name = "fraRůzné"
        Me.fraRůzné.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraRůzné.Size = New System.Drawing.Size(153, 137)
        Me.fraRůzné.TabIndex = 20
        Me.fraRůzné.TabStop = False
        Me.fraRůzné.Text = "Různé"
        '
        'přepVyčistit
        '
        Me.přepVyčistit.Caption = "Vyčistit"
        Me.přepVyčistit.Hodnota = False
        Me.přepVyčistit.Location = New System.Drawing.Point(6, 86)
        Me.přepVyčistit.Name = "přepVyčistit"
        Me.přepVyčistit.Size = New System.Drawing.Size(137, 17)
        Me.přepVyčistit.TabIndex = 25
        Me.přepVyčistit.ToolTipText = ""
        '
        'přepDlaždice
        '
        Me.přepDlaždice.Caption = "Dlaždice"
        Me.přepDlaždice.Hodnota = False
        Me.přepDlaždice.Location = New System.Drawing.Point(6, 63)
        Me.přepDlaždice.Name = "přepDlaždice"
        Me.přepDlaždice.Size = New System.Drawing.Size(137, 17)
        Me.přepDlaždice.TabIndex = 24
        Me.přepDlaždice.ToolTipText = ""
        '
        'přepPřekrývání
        '
        Me.přepPřekrývání.Caption = "Povolit překrývání"
        Me.přepPřekrývání.Hodnota = False
        Me.přepPřekrývání.Location = New System.Drawing.Point(6, 40)
        Me.přepPřekrývání.Name = "přepPřekrývání"
        Me.přepPřekrývání.Size = New System.Drawing.Size(137, 17)
        Me.přepPřekrývání.TabIndex = 22
        Me.přepPřekrývání.ToolTipText = ""
        '
        'cmbSmRozptyl
        '
        '
        'přepSm
        '
        '
        'frmEpidemia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.tlZavřít
        Me.ClientSize = New System.Drawing.Size(881, 489)
        Me.Controls.Add(Me.UkažVýběr)
        Me.Controls.Add(Me.tlZavřít)
        Me.Controls.Add(Me.tlZastavit)
        Me.Controls.Add(Me.tlSpustit)
        Me.Controls.Add(Me.tlUložit)
        Me.Controls.Add(Me.tlOtevřít)
        Me.Controls.Add(Me.tlSmazat)
        Me.Controls.Add(Me.tlZměnit)
        Me.Controls.Add(Me.tlDozadu)
        Me.Controls.Add(Me.tlDopředu)
        Me.Controls.Add(Me.fraLinia)
        Me.Controls.Add(Me.fraSměrová)
        Me.Controls.Add(Me.fraMutace)
        Me.Controls.Add(Me.fraSousedé)
        Me.Controls.Add(Me.fraOhniska)
        Me.Controls.Add(Me.lvEp)
        Me.Controls.Add(Me.fraRůzné)
        Me.Controls.Add(Me.tlPřidat)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.Name = "frmEpidemia"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Epidemia"
        Me.fraLinia.ResumeLayout(False)
        Me.fraSměrová.ResumeLayout(False)
        Me.fraMutace.ResumeLayout(False)
        Me.fraSousedé.ResumeLayout(False)
        Me.fraOhniska.ResumeLayout(False)
        Me.fraRůzné.ResumeLayout(False)
        CType(Me.cmbSmRozptyl, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.přepSm, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Směrovka As Fraktálník.Směrovka
    Friend WithEvents přepPřidatRám As Fraktálník.Přepínač
#End Region 
End Class