Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmEpidemia
	Inherits System.Windows.Forms.Form
	
	Dim SouslEp() As VlEp
	Dim KolEp As New KolEp
	
    Private Sub cmbSmRozptyl_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSmRozptyl.SelectedIndexChanged
        Dim Index As Short = cmbSmRozptyl.GetIndex(eventSender)
        NastavMaxPo�Sous()
    End Sub
	
    Private Sub cmbSoused�_SelectedIndexChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmbSoused�.SelectedIndexChanged
        Select Case cmbSoused�.SelectedIndex
            Case modSP.TypSoused�.tsN�strann�, modSP.TypSoused�.tsN�ro�n�
                cmbSmRozptyl(1).Visible = True
                cmbSmRozptyl(0).Visible = False
            Case Else
                cmbSmRozptyl(0).Visible = True
                cmbSmRozptyl(1).Visible = False
        End Select
        NastavMaxPo�Sous()
    End Sub
	
	Private Sub frmEpidemia_KeyDown(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
		Dim KeyCode As Short = eventArgs.KeyCode
        If KeyCode = System.Windows.Forms.Keys.F11 Then
            Uka�V�b�r.Checked = Not Uka�V�b�r.Checked
        ElseIf KeyCode = System.Windows.Forms.Keys.F12 Then
            Uka�Epidemii = Not Uka�Epidemii
        End If
	End Sub
	
	Private Sub frmEpidemia_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		With lvEp.Columns
            .Insert(0, "", "Typ", -2)
            .Insert(1, "", "Sm�rov�:", -2)
            .Insert(2, "", "Nav. na minulou", -2)
            .Insert(3, "", "P�edat v�e", -2)
            .Insert(4, "", "P�idat r�m", -2)
            .Insert(5, "", "Po�. ohnisek", -2)
            .Insert(6, "", "Hod. oh.", -2)
            .Insert(7, "", "N�h. hod. oh.", -2)
            .Insert(8, "", "Soused�", -2)
            .Insert(9, "", "Po�ad� sous.", -2)
            .Insert(10, "", "Sm. rozptyl", -2)
            .Insert(11, "", "Po�. sous.", -2)
            .Insert(12, "", "Mutace", -2)
            .Insert(13, "", "Po�. vr.", -2)
            .Insert(14, "", "Povolit p�ekr�v�n�", -2)
            .Insert(15, "", "Dla�dice", -2)
            .Insert(16, "", "Vy�istit", -2)
        End With
		Dim Hl As System.Windows.Forms.ColumnHeader
		For	Each Hl In lvEp.Columns
            Hl.Width = (DejNastaven�("EpSl" & Hl.Index, Hl.Width))
		Next Hl
		With cmbSoused�
			.Items.Add("n�strann�")
			.Items.Add("n�ro�n�")
			.Items.Add("n�strann� �i n�ro�n�")
			.Items.Add("sp�e n�strann�")
			.Items.Add("v�ichni")
			.SelectedIndex = 4
		End With
		With cmbPo�ad�Soused�
			.Items.Add("pravideln�")
            .Items.Add("n�hodn�")
            .SelectedIndex = 1
		End With
		With cmbSmRozptyl(0)
			.Items.Add("0�")
			.Items.Add("90�")
			.Items.Add("180�")
			.Items.Add("270�")
			.Items.Add("360�")
            .SelectedIndex = 4
		End With
		With cmbSmRozptyl(1)
			.Items.Add("0�")
			.Items.Add("180�")
			.Items.Add("360�")
			.SelectedIndex = 2
		End With
		With cmbMutace
			.Items.Add("n�hodn� zm�na")
			.Items.Add("beze zm�ny")
			.Items.Add("p��r�stek")
			.Items.Add("v�t�� p��r�stek")
			.Items.Add("rostouc� p��r�stek")
			.SelectedIndex = 0
		End With
		tlSt�edX_Click(tlSt�edX, New System.EventArgs())
		tlSt�edY_Click(tlSt�edY, New System.EventArgs())
        Uka�ProLinii(p�epLinia.Hodnota)
        Uka�ProSm�rovou(p�epSm�rov�.Hodnota)
	End Sub
	
	Private Sub frmEpidemia_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		Select Case UnloadMode
			Case System.Windows.Forms.CloseReason.UserClosing
				Cancel = 1
				Me.Hide()
		End Select ' 0   The user chose the Close command from the Control menu on the form.
		'Case vbFormCode  '1   The Unload statement is invoked from code.
		'Case vbAppWindows  '  2   The current Microsoft Windows operating environment session is ending.
		'Case vbAppTaskManager '   3   The Microsoft Windows Task Manager is closing the application.
		'Case vbFormMDIForm  ' 4
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub frmEpidemia_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
		Dim Hl As System.Windows.Forms.ColumnHeader
		For	Each Hl In lvEp.Columns
            Ulo�Nastaven�("EpSl" & Hl.Index, CStr(Hl.Width))
		Next Hl
	End Sub
	
	
	Property Soused�() As modSP.TypSoused�
		Get
			Soused� = cmbSoused�.SelectedIndex
		End Get
		Set(ByVal Value As modSP.TypSoused�)
			cmbSoused�.SelectedIndex = Value
		End Set
	End Property
	
	
    Property Rozptyl() As modSP.TSmRozptyl
        Get
            Select Case Soused�
                Case modSP.TypSoused�.tsN�strann�, modSP.TypSoused�.tsN�ro�n�
                    Select Case cmbSmRozptyl(1).SelectedIndex
                        Case 0
                            Rozptyl = modSP.TSmRozptyl.sr0
                        Case 1
                            Rozptyl = modSP.TSmRozptyl.sr180
                        Case Else
                            Rozptyl = modSP.TSmRozptyl.sr360
                    End Select
                Case Else
                    Select Case cmbSmRozptyl(0).SelectedIndex
                        Case 0
                            Rozptyl = modSP.TSmRozptyl.sr0
                        Case 1
                            Rozptyl = modSP.TSmRozptyl.sr90
                        Case 2
                            Rozptyl = modSP.TSmRozptyl.sr180
                        Case 3
                            Rozptyl = modSP.TSmRozptyl.sr270
                        Case Else
                            Rozptyl = modSP.TSmRozptyl.sr360
                    End Select
            End Select
            Rozptyl = Rozptyl * IIf(p�epDr�etSm�r.Hodnota, -1, 1)
        End Get
        Set(ByVal Value As modSP.TSmRozptyl)
            Select Case Soused�
                Case modSP.TypSoused�.tsN�strann�, modSP.TypSoused�.tsN�ro�n�
                    cmbSmRozptyl(0).Visible = False
                    With cmbSmRozptyl(1)
                        .Visible = True
                        Select Case Value
                            Case modSP.TSmRozptyl.sr0, modSP.TSmRozptyl.srP�v0
                                .SelectedIndex = 0
                            Case modSP.TSmRozptyl.sr180, modSP.TSmRozptyl.srP�v180
                                .SelectedIndex = 1
                            Case modSP.TSmRozptyl.sr360, modSP.TSmRozptyl.srP�v360
                                .SelectedIndex = 2
                        End Select
                    End With
                Case Else
                    cmbSmRozptyl(1).Visible = False
                    With cmbSmRozptyl(0)
                        .Visible = True
                        Select Case Value
                            Case modSP.TSmRozptyl.sr0, _
                                 modSP.TSmRozptyl.srP�v0
                                .SelectedIndex = 0
                            Case modSP.TSmRozptyl.sr90, _
                                 modSP.TSmRozptyl.srP�v90
                                .SelectedIndex = 1
                            Case modSP.TSmRozptyl.sr180, _
                                 modSP.TSmRozptyl.srP�v180
                                .SelectedIndex = 2
                            Case modSP.TSmRozptyl.sr270, _
                                 modSP.TSmRozptyl.srP�v270
                                .SelectedIndex = 3
                            Case modSP.TSmRozptyl.sr360, _
                                 modSP.TSmRozptyl.srP�v360
                                .SelectedIndex = 4
                        End Select
                    End With
                    If Value < 0 Then p�epDr�etSm�r.Hodnota = True
            End Select
        End Set
    End Property

    ReadOnly Property Plat�LinSm( _
        ByVal Co As String, _
        ByVal Ep As Epidemia, _
        Optional ByVal Lin As Boolean = False, _
        Optional ByVal Sm As Boolean = False, _
        Optional ByVal Oby� As Boolean = True _
    ) As String
        Get
            If Ep.Linia And Not Lin Then
                Plat�LinSm = ""
            ElseIf Ep.Sm�rov� And Not Sm Then
                Plat�LinSm = ""
            ElseIf Not (Ep.Linia Or Ep.Sm�rov�) And Not Oby� Then
                Plat�LinSm = ""
            Else
                Plat�LinSm = Co
            End If
        End Get
    End Property

    ReadOnly Property �etSm�r�(ByVal Sm�ry As Object) As String
        Get
            Dim I As Integer
            Dim Sm As Object
            �etSm�r� = ""
            Try
                If IsArray(Sm�ry(0)) Then
                    Sm = Sm�ry(0)
                Else
                    Sm = Sm�ry
                End If
                For I = 0 To UBound(Sm)
                    �etSm�r� &= Sm(I)
                    If I < UBound(Sm) Then
                        �etSm�r� &= ","
                    End If
                Next
            Catch
                �etSm�r� = "nezad�n ��dn� sm�r!"
            End Try
        End Get
    End Property


    Property Sm�ry() As TOsmism�r()
        Get
            Dim Sm() As TOsmism�r = {}
            Dim I As Integer
            For I = 0 To 7
                If Sm�rovka.Hodnota(I) Then
                    Array.Resize(Sm, Sm.Length + 1)
                    Sm(UBound(Sm)) = I + 1 ' sm�ry jsou ��slov�ny od 1
                End If
            Next
            Return Sm
        End Get
        Set(ByVal Value As TOsmism�r())
            Dim Lin As Boolean
            Lin = p�epLinia.Hodnota
            Sm�rovka.V�echnySm = False
            p�epLinia.Hodnota = Lin
            If IsNothing(Value) Then Exit Property
            For I As Integer = LBound(Value) To UBound(Value)
                Sm�rovka.Hodnota(Value(I) - 1) = True
            Next
        End Set
    End Property

    ReadOnly Property Vybr�no() As Boolean
        Get
            Vybr�no = Not (lvEp.FocusedItem Is Nothing)
        End Get
    End Property

    Sub NastavMaxPo�Sous()
        Select Case Soused�
            Case modSP.TypSoused�.tsN�ro�n�, modSP.TypSoused�.tsN�strann�
                �ouPo�etVybran�chSous.Max = (System.Math.Abs(Rozptyl) - 1) \ 2 + 1
            Case modSP.TypSoused�.tsN�hodn�N�strann��iN�ro�n�
                Select Case System.Math.Abs(Rozptyl)
                    Case modSP.TSmRozptyl.sr0
                        �ouPo�etVybran�chSous.Max = 1
                    Case modSP.TSmRozptyl.sr90
                        �ouPo�etVybran�chSous.Max = 2
                    Case modSP.TSmRozptyl.sr180
                        �ouPo�etVybran�chSous.Max = 3
                    Case modSP.TSmRozptyl.sr270, modSP.TSmRozptyl.sr360
                        �ouPo�etVybran�chSous.Max = 4
                End Select
            Case Else
                �ouPo�etVybran�chSous.Max = System.Math.Abs(Rozptyl)
        End Select
    End Sub

    Private Sub lvEp_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles lvEp.Click
        P�e�ti()
    End Sub

    Private Sub lvEp_ItemClick(ByVal Item As System.Windows.Forms.ListViewItem)
        P�e�ti()
    End Sub

    Private Sub p�epLinia_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles p�epLinia.Click
        Zm�naP�epLinia()
    End Sub

    Private Sub p�epLinia_Zm�na(ByVal sender As Object, ByVal e As System.EventArgs) Handles p�epLinia.Zm�na
        Zm�naP�epLinia()
    End Sub

    Private Sub Zm�naP�epLinia()
        If p�epLinia.Hodnota Then p�epSm�rov�.Hodnota = False
        Uka�ProLinii(p�epLinia.Hodnota)
    End Sub


    Private Sub p�epN�hPo�Sous_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles p�epN�hPo�Sous.Click
        p�ep�plN�hPo�Sous.Enabled = p�epN�hPo�Sous.Hodnota
        �ouPo�etVybran�chSous.Enabled = Not (p�epN�hPo�Sous.Hodnota AndAlso p�ep�plN�hPo�Sous.Hodnota)
        If Not p�epN�hPo�Sous.Hodnota Then p�ep�plN�hPo�Sous.Hodnota = False
    End Sub

    Private Sub p�epSm_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles p�epSm.Click
        Dim Index As Short = p�epSm.GetIndex(eventSender)
        Dim P�ep As P�ep�na�
        Dim N�coZa�krtnuto As Boolean
        For Each P�ep In p�epSm
            If P�ep.Hodnota Then N�coZa�krtnuto = True
        Next P�ep
        p�epSm�rov�.Hodnota = N�coZa�krtnuto
    End Sub

    Private Sub p�epSm�rov�_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles p�epSm�rov�.Click
        Zm�naP�epSm�rov�()
    End Sub

    Private Sub p�epSm�rov�_Zm�na(ByVal sender As Object, ByVal e As System.EventArgs) Handles p�epSm�rov�.Zm�na
        Zm�naP�epSm�rov�()
    End Sub

    Private Sub Zm�naP�epSm�rov�()
        If p�epSm�rov�.Hodnota Then p�epLinia.Hodnota = False
        Uka�ProSm�rovou(p�epSm�rov�.Hodnota)
    End Sub

    Private Sub p�ep�plN�hPo�Sous_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles p�ep�plN�hPo�Sous.Click
        �ouPo�etVybran�chSous.Enabled = Not p�ep�plN�hPo�Sous.Hodnota
    End Sub


    Private Sub tlDop�edu_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlDop�edu.Click
        Dim UlEp As Epidemia ' ulo�en� epidemie
        Dim Ul�� As System.Windows.Forms.ListViewItem ' ulo�en� ��dek
        Dim Index0, Index1 As Integer

        If Not Vybr�no Then Exit Sub

        Ul�� = lvEp.FocusedItem

        Index0 = Ul��.Index
        Index1 = Index0 + 1

        If Index0 = 0 Then Exit Sub

        UlEp = KolEp(Index1)

        KolEp(Index1) = KolEp(Index1 - 1)
        KolEp(Index1 - 1) = UlEp

        Dim No�� As System.Windows.Forms.ListViewItem
        No�� = lvEp.Items.Insert(Index0 - 1, Ul��.Text, Ul��.ImageIndex)
        Dim Pol As System.Windows.Forms.ListViewItem.ListViewSubItem
        ' Nov� ��dek u� obsahuje 1 podpolo�ku p�idanou 
        ' p�i tvorb� ��dku (polo�ky)!
        For P As Integer = 1 To Ul��.SubItems.Count - 1
            Pol = Ul��.SubItems(P)
            No��.SubItems.Add(Pol.Text)
        Next P
        lvEp.Items.RemoveAt(Index0 + 1)
        lvEp.FocusedItem = lvEp.Items.Item(Index0 - 1)
        P�e�ti()
    End Sub

    Private Sub tlDozadu_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlDozadu.Click
        Dim UlEp As Epidemia ' ulo�en� epidemie
        Dim Ul�� As System.Windows.Forms.ListViewItem ' ulo�en� ��dek
        Dim Index0, Index1 As Integer

        If Not Vybr�no Then Exit Sub

        Ul�� = lvEp.FocusedItem

        Index0 = Ul��.Index
        Index1 = Index0 + 1

        If Index0 = lvEp.Items.Count - 1 Then Exit Sub

        ' Prohod�me epidemie
        UlEp = KolEp(Index1)
        KolEp(Index1) = KolEp(Index1 + 1)
        KolEp(Index1 + 1) = UlEp

        Dim No�� As System.Windows.Forms.ListViewItem
        No�� = lvEp.Items.Insert(Index0 + 2, Ul��.Text, Ul��.ImageIndex)
        ' Nov� ��dek u� obsahuje jednu podpolo�ku p�idanou 
        ' p�i vytvo�en� ��dku (polo�ky)!
        Dim Pol As System.Windows.Forms.ListViewItem.ListViewSubItem
        For P As Integer = 1 To Ul��.SubItems.Count - 1
            Pol = Ul��.SubItems(P)
            No��.SubItems.Add(Pol.Text)
        Next P
        lvEp.Items.RemoveAt(Index0)
        lvEp.FocusedItem = lvEp.Items.Item(Index0 + 1)
        P�e�ti()
    End Sub

    Private Sub tlN�hX_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlN�hX.Click
        Dim N�h As New GenerN
        N�h.Largest = �ik.Pic.ClientSize.Width - 1
        �ouLinX.Hodnota = N�h.Value
    End Sub

    Private Sub tlN�hY_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlN�hY.Click
        Dim N�h As New GenerN
        N�h.Largest = �ik.Pic.ClientSize.Width - 1
        �ouLinY.Hodnota = N�h.Value
    End Sub

    Private Sub tlP�idat_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlP�idat.Click
        P�idej()
        'P�e�ti
    End Sub

    Sub P�idej()
        Zapi�(-1)
    End Sub

    Sub Zapi�(ByRef Index As Object)
        Dim Nov� As Boolean
        Nov� = (Index = -1)
        Dim Ep As New Epidemia
        With Ep
            .Linia = CBool(p�epLinia.Hodnota)
            .Sm�rov� = CBool(p�epSm�rov�.Hodnota)
            If .Sm�rov� Then .Sm�ry = Sm�ry
            .Nav�zatNaMinulou = p�epNav�zatNaMinulou.Hodnota
            .VracetJenPoslVr = Not p�epP�edatV�e.Hodnota
            If p�epPou��tOhniska.Hodnota Or p�epLinia.Hodnota Then
                If Not p�epN�hPo�Oh.Hodnota Then
                    .Po�Oh = �ouPo�etOh.Hodnota
                End If
                .N�hHodOh = p�epN�hHodOh.Hodnota
                .HodOh = �ouHodOh.Hodnota
            Else
                .Po�Oh = Nothing
                .N�hHodOh = False
                .HodOh = Nothing
            End If
            .Soused� = Soused�
            .Po�ad�Sous = cmbPo�ad�Soused�.SelectedIndex
            .SmRozptyl = Rozptyl
            If p�epN�hPo�Sous.Hodnota And p�ep�plN�hPo�Sous.Hodnota Then
                .Po�etSous = 0
            ElseIf p�epN�hPo�Sous.Hodnota Then
                .Po�etSous = -�ouPo�etVybran�chSous.Hodnota
            Else
                .Po�etSous = �ouPo�etVybran�chSous.Hodnota
            End If
            .Mutace = cmbMutace.SelectedIndex
            .Po�etVrstev = CInt(�ouPo�etVrstev.Hodnota)
            .PovolitP�ekr�v�n� = p�epP�ekr�v�n�.Hodnota
            '.VyhnoutSeNakreslen�m = p�epVyhnoutSeNakr.Hodnota
            .Dla�dice = p�epDla�dice.Hodnota
            .Vy�istit = p�epVy�istit.Hodnota
            .LinX = �ouLinX.Hodnota
            .LinY = �ouLinY.Hodnota
            .P�idatR�m = p�epP�idatR�m.Hodnota
        End With

        If Nov� Then
            KolEp.Add(Ep)
        Else
            KolEp(Index + 1) = Ep
        End If

        Napi���dek(Ep, Index)
    End Sub

    Sub Napi���dek(ByRef Ep As Epidemia, _
                   Optional ByRef Index As Integer = -1)
        Dim Nov� As Boolean
        Nov� = Index = -1
        Dim It As System.Windows.Forms.ListViewItem
        If Nov� Then
            It = lvEp.Items.Add( _
                IIf(Ep.Linia, "Linia", "Epidemia"), 0)
        Else
            It = lvEp.Items.Item(Index)
            It.Text = IIf(Ep.Linia, "Linia", "Epidemia")
        End If
        With It.SubItems
            .Clear()
            ' Epidemia/Linia
            .Add(IIf(Ep.Linia, "Linia", "Epidemia")) ' 0
            ' Sm�rov�?
            If Ep.Sm�rov� Then
                .Add(�etSm�r�(Ep.Sm�ry)) ' 1
            Else
                .Add("ne") ' 1
            End If
            ' Nav�zat na minulou epidemii?
            .Add(Plat�LinSm(Co:=AnoNe(Ep.Nav�zatNaMinulou), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 2
            ' Vracet jen posledn� vrstvu?
            .Add(Plat�LinSm(Co:=AnoNe(Not Ep.VracetJenPoslVr), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 3
            .Add(Plat�LinSm(Co:=AnoNe(Ep.P�idatR�m), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 4
            ' Pokud je zadan� hodnota ohniska:
            If Ep.HodOh IsNot Nothing Then
                ' Po�et ohnisek, nezad�n => n�hodn�
                .Add(Plat�LinSm( _
                    Co:=IIf(IsNothing(Ep.Po�Oh), _
                    "n�hodn�", Ep.Po�Oh), _
                    Ep:=Ep, Lin:=False, Sm:=True)) ' 5
                ' Hodnota ohniska
                .Add(Plat�LinSm(Co:=Ep.HodOh, _
                    Ep:=Ep, Lin:=True, Sm:=True)) ' 6
                ' N�hodn� hodnota ohniska?
                .Add(Plat�LinSm(Co:=AnoNe(Ep.N�hHodOh), _
                    Ep:=Ep, Lin:=True, Sm:=True)) ' 7
            Else ' Pokud nen� zadan� hodnota ohniska:
                ' Po�et ohnisek
                .Add("") ' 5
                ' Hodnota ohniska
                .Add("") ' 6
                ' N�hodn� hodnota ohniska?
                .Add("") ' 7
            End If
            ' Typ soused�
            .Add(Plat�LinSm(Co:=VB6.GetItemString( _
                cmbSoused�, Ep.Soused�), _
                Ep:=Ep, Lin:=False, Sm:=False)) ' 8
            ' Po�ad� soused�
            .Add(Plat�LinSm(Co:=VB6.GetItemString( _
                cmbPo�ad�Soused�, Ep.Po�ad�Sous), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 9
            ' Sm�rov� rozptyl, voln� nebo d�d�n�
            .Add(Plat�LinSm(Co:=Choose( _
                System.Math.Abs(Ep.SmRozptyl), _
                "0�", "", "90�", "", "180�", "", "270�", "360�") _
                & IIf(Ep.SmRozptyl < 0, " od p�v. sm.", ""), _
                Ep:=Ep, Lin:=False, Sm:=False)) ' 10
            ' Po�et soused�
            .Add(Plat�LinSm(VB.Switch( _
                Ep.Po�etSous > 0, _
                    "p�esn� " & Ep.Po�etSous, _
                Ep.Po�etSous < 0, _
                    "n�hodn�, nejv�ce " _
                    & System.Math.Abs(Ep.Po�etSous), _
                IsNothing(Ep.Po�etSous), _
                    "�pln� n�hodn�", _
                Not IsNothing(Ep.Po�etSous) _
                And Ep.Po�etSous = 0, _
                    "v�ichni mo�n�"), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 11
            ' Druh mutace hodnoty
            .Add(Plat�LinSm(Co:=VB6.GetItemString( _
                cmbMutace, Ep.Mutace), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 12
            ' Po�et vrstev, pop��pad� neomezen�
            .Add(Plat�LinSm(Co:=IIf(Ep.Po�etVrstev = 0, _
                "neomezen�", Ep.Po�etVrstev), _
                Ep:=Ep, Lin:=True, Sm:=True)) ' 13
            ' Povolit p�ekr�v�n�?
            .Add(Plat�LinSm(Co:=AnoNe(Ep.PovolitP�ekr�v�n�), _
                Ep:=Ep, Lin:=False, Sm:=True)) ' 14
            ' Dla�dicov�?
            .Add(Plat�LinSm(Co:=AnoNe(Ep.Dla�dice), _
                Ep:=Ep, Lin:=True, Sm:=True)) ' 15
            ' Vy�istit mapu p�ed startem?
            .Add(Plat�LinSm(Co:=AnoNe(Ep.Vy�istit), _
                Ep:=Ep, Lin:=True, Sm:=True)) ' 16
            ' Za��tek Linie na ose X
            .Add(Plat�LinSm(Co:=Ep.LinX, _
                Ep:=Ep, Lin:=True, Sm:=False, Oby�:=False)) ' 17
            ' Za��tek Linie na ose Y
            .Add(Plat�LinSm(Co:=Ep.LinY, _
                Ep:=Ep, Lin:=True, Sm:=False, Oby�:=False)) ' 18
        End With
        If Nov� Then It.Focused = True
    End Sub

    Sub P�e�ti()
        Dim Ep As Epidemia
        If Not lvEp.FocusedItem Is Nothing Then
            Ep = KolEp(lvEp.FocusedItem.Index + 1)
            With Ep
                p�epLinia.Hodnota = .Linia
                p�epSm�rov�.Hodnota = .Sm�rov�
                If .Sm�rov� Then Sm�ry = .Sm�ry
                p�epNav�zatNaMinulou.Hodnota = .Nav�zatNaMinulou
                p�epP�edatV�e.Hodnota = Not .VracetJenPoslVr
                p�epPou��tOhniska.Hodnota = Not (IsNothing(.HodOh) And Not .N�hHodOh) And Not .Linia
                p�epN�hPo�Oh.Hodnota = IsNothing(.Po�Oh)
                �ouPo�etOh.Hodnota = .Po�Oh
                �ouHodOh.Hodnota = .HodOh
                p�epN�hHodOh.Hodnota = .N�hHodOh
                Soused� = .Soused�
                cmbPo�ad�Soused�.SelectedIndex = .Po�ad�Sous
                If (Not .Sm�rov�) And (Not .Linia) Then Rozptyl = .SmRozptyl
                p�epN�hPo�Sous.Hodnota = IsNothing(.Po�etSous) Or .Po�etSous < 0
                p�ep�plN�hPo�Sous.Hodnota = IsNothing(.Po�etSous)
                �ouPo�etVybran�chSous.Hodnota = System.Math.Abs(.Po�etSous)
                cmbMutace.SelectedIndex = .Mutace
                �ouPo�etVrstev.Hodnota = .Po�etVrstev
                p�epP�ekr�v�n�.Hodnota = .PovolitP�ekr�v�n�
                'p�epVyhnoutSeNakr.Hodnota = .VyhnoutSeNakreslen�m
                p�epDla�dice.Hodnota = .Dla�dice
                p�epVy�istit.Hodnota = .Vy�istit
                �ouLinX.Hodnota = .LinX
                �ouLinY.Hodnota = .LinY
                p�epP�idatR�m.Hodnota = .P�idatR�m
            End With
        End If
    End Sub

    Private Sub tlSmazat_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlSmazat.Click
        Dim Index As Object
        If Vybr�no Then
            Index = lvEp.FocusedItem.Index
            KolEp.Remove(Index + 1)
            lvEp.Items.RemoveAt(Index)
        Else
            Index = 0
        End If
        If lvEp.Items.Count > Index Then
            lvEp.FocusedItem = lvEp.Items.Item(Index)
        ElseIf lvEp.Items.Count > 0 Then
            lvEp.FocusedItem = lvEp.Items.Item(Index - 1)
        End If
        P�e�ti()
    End Sub

    Private Sub tlSpustit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlSpustit.Click
        Dim Ep As Epidemia
        Dim MinEp As Epidemia = Nothing
        P�eru�it = False
        '�ik.Pic.BackColor = System.Drawing.Color.White
        If modSP.�istit Then �ik.Pic.Vy�isti()
        For Each Ep In KolEp
            Ep.Naka�en� = Nothing
            Ep.Ohniska = Nothing
        Next Ep
        For Each Ep In KolEp
            ' m��e se zru�it, pokud to bude t�eba:
            ' prvn� epidemie �ist� mapu nakreslen�ch
            If MinEp Is Nothing Then
                Ep.Vy�istit = True
            Else ' m�me minulou epidemii
                ' p�ed�n� minul�ch naka�en�ch do nyn�j��ch ohnisek
                If Ep.Nav�zatNaMinulou Then
                    Ep.Ohniska = MinEp.Naka�en�
                End If
            End If
            Ep.Spus�()
            MinEp = Ep
        Next Ep
        �ik.Pic.Cursor = System.Windows.Forms.Cursors.Default
        M�stoKreslen� = CXY(-1, -1)
        'Beep()
        MsgBox("Hotovo")
    End Sub

    Private Sub tlSt�edX_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlSt�edX.Click
        �ouLinX.Hodnota = frmFrakt�ln�k.obrFr.ClientRectangle.Width / 2
    End Sub

    Private Sub tlSt�edY_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlSt�edY.Click
        �ouLinY.Hodnota = frmFrakt�ln�k.obrFr.ClientRectangle.Height / 2
    End Sub

    Private Sub tlUlo�it_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlUlo�it.Click
        Dim I As Integer
        ReDim SouslEp(KolEp.Count)
        For I = 0 To KolEp.Count - 1
            SouslEp(I) = CVlEp(KolEp(I + 1))
        Next
        CDOpen.InitialDirectory = My.Application.Info.DirectoryPath & "\Epidemie"
        CDSave.InitialDirectory = CDOpen.InitialDirectory
        CDOpen.Filter = "Soubory epidemi� (*.epidemia)|*.epidemia"
        CDSave.Filter = "Soubory epidemi� (*.epidemia)|*.epidemia"
        If CDSave.ShowDialog() = Windows.Forms.DialogResult.OK Then
            CDOpen.FileName = CDSave.FileName
            On Error GoTo 0
            Dim Jm As String
            Jm = CDOpen.FileName
            FileOpen(1, Jm, OpenMode.Output)
            FileClose()
            FileOpen(1, Jm, OpenMode.Binary)
            FilePut(1, UBound(SouslEp))
            FilePut(1, SouslEp)
            FileClose(1)
        End If
    End Sub

    Private Sub tlOtev��t_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlOtev��t.Click
        CDOpen.InitialDirectory = My.Application.Info.DirectoryPath & "\Epidemie"
        CDSave.InitialDirectory = My.Application.Info.DirectoryPath & "\Epidemie"
        CDOpen.Filter = "Soubory epidemi� (*.epidemia)|*.epidemia"
        CDSave.Filter = CDOpen.Filter

        If CDOpen.ShowDialog() = Windows.Forms.DialogResult.OK Then
            CDSave.FileName = CDOpen.FileName
            On Error GoTo 0
            Dim Jm As String
            Jm = CDOpen.FileName
            FileOpen(1, Jm, OpenMode.Binary)
            Dim Po� As Integer
            FileGet(1, Po�)
            ReDim SouslEp(Po�)
            FileGet(1, SouslEp)
            FileClose(1)
            Dim I As Integer
            Dim Ep As Epidemia
            KolEp = New KolEp
            lvEp.Items.Clear()
            For I = 0 To Po� - 1
                Napi���dek(KolEp.Add(CEp(SouslEp(I))))
            Next
            P�e�ti()
        End If
    End Sub

    Private Sub tlZastavit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlZastavit.Click
        P�eru�it = True
    End Sub

    Private Sub tlZav��t_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlZav��t.Click
        Me.Hide()
    End Sub

    Private Sub tlZm�nit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlZm�nit.Click
        If Not lvEp.FocusedItem Is Nothing Then
            Zapi�(lvEp.FocusedItem.Index)
        End If
    End Sub

    Sub Uka�ProLinii(ByRef Uka� As Boolean)
        Dim Ctl As System.Windows.Forms.Control
        Dim ColCtl As New Collection
        'Dim I As Integer
        With ColCtl
            .Add(Me.cmbMutace)
            .Add(Me.cmbPo�ad�Soused�)
            .Add(Me.cmbSmRozptyl(0))
            .Add(Me.cmbSmRozptyl(1))
            .Add(Me.cmbSoused�)
            .Add(Me.p�epDr�etSm�r)
            .Add(Me.p�epN�hPo�Oh)
            .Add(Me.p�epN�hPo�Sous)
            .Add(Me.p�epNav�zatNaMinulou)
            .Add(Me.p�epPou��tOhniska)
            .Add(Me.p�epP�edatV�e)
            .Add(Me.p�epP�idatR�m)
            '.Add Me.p�epP�ekr�v�n� ' to se te� pou��v�
            .Add(Me.Sm�rovka)
            .Add(Me.p�ep�plN�hPo�Sous)
            '.Add Me.p�epVyhnoutSeNakr ' to se pou��v�
            .Add(Me.�ouPo�etOh)
            .Add(Me.�ouPo�etVybran�chSous)
            .Add(Me.lblPo�ad�Soused�)
            .Add(Me.lblSmRozptyl)
            .Add(Me.lblSoused�)
        End With
        For Each Ctl In ColCtl
            Ctl.Visible = Not Uka�
        Next Ctl
        If Uka� Then
            p�epSm�rov�.Hodnota = False
        Else
            'p�epSm_Click(_p�epSm_0, New System.EventArgs)
            'MsgBox("Co se m� d�lat te�?")
            ' Asi nic :-D
        End If
    End Sub

    Sub Uka�ProSm�rovou(ByRef Uka� As Boolean)
        Dim Ctl As System.Windows.Forms.Control
        Dim ColCtl As New Collection
        With ColCtl
            .Add(Me.cmbSmRozptyl(0))
            .Add(Me.cmbSmRozptyl(1))
            .Add(Me.cmbSoused�)
            .Add(Me.p�epDr�etSm�r)
            .Add(Me.�ouLinX)
            .Add(Me.�ouLinY)
            .Add(Me.lblSmRozptyl)
            .Add(Me.lblSoused�)
            .Add(Me.tlSt�edX)
            .Add(Me.tlSt�edY)
            .Add(Me.tlN�hX)
            .Add(Me.tlN�hY)
        End With
        For Each Ctl In ColCtl
            Ctl.Visible = Not Uka�
        Next Ctl
        If Uka� Then p�epLinia.Hodnota = False
    End Sub


    Private Sub Sm�rovka_Click(ByRef Sender As System.Object, ByVal e As System.EventArgs, ByVal Index As System.Int16) Handles Sm�rovka.Click
        Zm�naSm�rovky()
    End Sub

    Private Sub Sm�rovka_Zm�na(ByRef Sender As System.Object, ByVal e As System.EventArgs, ByVal Index As System.Int16) Handles Sm�rovka.Zm�na
        Zm�naSm�rovky()
    End Sub

    Sub Zm�naSm�rovky()
        p�epSm�rov�.Hodnota = Sm�rovka.V�echnySm <> 0
    End Sub

    Private Sub Uka�V�b�r_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Uka�V�b�r.CheckedChanged
        frmV�b�r.Hide()
        If Uka�V�b�r.Checked Then
            frmV�b�r.Show(frmFrakt�ln�k)
        End If
    End Sub
End Class