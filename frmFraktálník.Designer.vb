<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmFrakt�ln�k
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents _sbrStav_Panel1 As System.Windows.Forms.ToolStripStatusLabel
	Public WithEvents sbrStav As System.Windows.Forms.StatusStrip
	Public WithEvents obrTisk As System.Windows.Forms.PictureBox
	Public CDOpen As System.Windows.Forms.OpenFileDialog
	Public CDSave As System.Windows.Forms.SaveFileDialog
	Public WithEvents IL As System.Windows.Forms.ImageList
	Public WithEvents pbKolikJe�t� As System.Windows.Forms.ProgressBar
    Public WithEvents V�b�r As System.Windows.Forms.ToolStripButton
    Public WithEvents Epidemia_ As System.Windows.Forms.ToolStripButton
    Public WithEvents _tbP��k_Button3 As System.Windows.Forms.ToolStripSeparator
    Public WithEvents Otev��t As System.Windows.Forms.ToolStripButton
    Public WithEvents Ulo�it As System.Windows.Forms.ToolStripButton
    Public WithEvents Tisk As System.Windows.Forms.ToolStripButton
    Public WithEvents Rozm�ry As System.Windows.Forms.ToolStripButton
    Public WithEvents tbP��k As System.Windows.Forms.ToolStrip
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmFrakt�ln�k))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.sbrStav = New System.Windows.Forms.StatusStrip
        Me._sbrStav_Panel1 = New System.Windows.Forms.ToolStripStatusLabel
        Me.obrTisk = New System.Windows.Forms.PictureBox
        Me.CDOpen = New System.Windows.Forms.OpenFileDialog
        Me.CDSave = New System.Windows.Forms.SaveFileDialog
        Me.IL = New System.Windows.Forms.ImageList(Me.components)
        Me.pbKolikJe�t� = New System.Windows.Forms.ProgressBar
        Me.tbP��k = New System.Windows.Forms.ToolStrip
        Me.V�b�r = New System.Windows.Forms.ToolStripButton
        Me.Epidemia_ = New System.Windows.Forms.ToolStripButton
        Me._tbP��k_Button3 = New System.Windows.Forms.ToolStripSeparator
        Me.Otev��t = New System.Windows.Forms.ToolStripButton
        Me.Ulo�it = New System.Windows.Forms.ToolStripButton
        Me.Tisk = New System.Windows.Forms.ToolStripButton
        Me.Rozm�ry = New System.Windows.Forms.ToolStripButton
        Me.PD = New System.Drawing.Printing.PrintDocument
        Me.obrFr = New Frakt�ln�k.Kresl�k
        Me.sbrStav.SuspendLayout()
        CType(Me.obrTisk, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbP��k.SuspendLayout()
        CType(Me.obrFr, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sbrStav
        '
        Me.sbrStav.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._sbrStav_Panel1})
        Me.sbrStav.Location = New System.Drawing.Point(0, 298)
        Me.sbrStav.Name = "sbrStav"
        Me.sbrStav.Size = New System.Drawing.Size(312, 17)
        Me.sbrStav.TabIndex = 4
        Me.sbrStav.Visible = False
        '
        '_sbrStav_Panel1
        '
        Me._sbrStav_Panel1.AutoSize = False
        Me._sbrStav_Panel1.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._sbrStav_Panel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._sbrStav_Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me._sbrStav_Panel1.Name = "_sbrStav_Panel1"
        Me._sbrStav_Panel1.Size = New System.Drawing.Size(297, 17)
        Me._sbrStav_Panel1.Spring = True
        Me._sbrStav_Panel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'obrTisk
        '
        Me.obrTisk.BackColor = System.Drawing.SystemColors.Control
        Me.obrTisk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.obrTisk.Cursor = System.Windows.Forms.Cursors.Default
        Me.obrTisk.ForeColor = System.Drawing.SystemColors.ControlText
        Me.obrTisk.Location = New System.Drawing.Point(40, 152)
        Me.obrTisk.Name = "obrTisk"
        Me.obrTisk.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.obrTisk.Size = New System.Drawing.Size(121, 73)
        Me.obrTisk.TabIndex = 3
        Me.obrTisk.TabStop = False
        Me.obrTisk.Visible = False
        '
        'IL
        '
        Me.IL.ImageStream = CType(resources.GetObject("IL.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.IL.TransparentColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.IL.Images.SetKeyName(0, "")
        Me.IL.Images.SetKeyName(1, "")
        Me.IL.Images.SetKeyName(2, "")
        Me.IL.Images.SetKeyName(3, "")
        Me.IL.Images.SetKeyName(4, "")
        Me.IL.Images.SetKeyName(5, "")
        Me.IL.Images.SetKeyName(6, "")
        Me.IL.Images.SetKeyName(7, "")
        '
        'pbKolikJe�t�
        '
        Me.pbKolikJe�t�.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pbKolikJe�t�.Location = New System.Drawing.Point(0, 297)
        Me.pbKolikJe�t�.Name = "pbKolikJe�t�"
        Me.pbKolikJe�t�.Size = New System.Drawing.Size(312, 18)
        Me.pbKolikJe�t�.TabIndex = 2
        Me.pbKolikJe�t�.Visible = False
        '
        'tbP��k
        '
        Me.tbP��k.ImageList = Me.IL
        Me.tbP��k.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.V�b�r, Me.Epidemia_, Me._tbP��k_Button3, Me.Otev��t, Me.Ulo�it, Me.Tisk, Me.Rozm�ry})
        Me.tbP��k.Location = New System.Drawing.Point(0, 0)
        Me.tbP��k.Name = "tbP��k"
        Me.tbP��k.Size = New System.Drawing.Size(312, 42)
        Me.tbP��k.TabIndex = 1
        '
        'V�b�r
        '
        Me.V�b�r.AutoSize = False
        Me.V�b�r.ImageIndex = 1
        Me.V�b�r.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.V�b�r.Name = "V�b�r"
        Me.V�b�r.Size = New System.Drawing.Size(40, 39)
        Me.V�b�r.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.V�b�r.ToolTipText = "V�b�r funkce"
        '
        'Epidemia_
        '
        Me.Epidemia_.AutoSize = False
        Me.Epidemia_.ImageIndex = 7
        Me.Epidemia_.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Epidemia_.Name = "Epidemia_"
        Me.Epidemia_.Size = New System.Drawing.Size(40, 39)
        Me.Epidemia_.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Epidemia_.ToolTipText = "Kreslen� epidemi�"
        '
        '_tbP��k_Button3
        '
        Me._tbP��k_Button3.AutoSize = False
        Me._tbP��k_Button3.Name = "_tbP��k_Button3"
        Me._tbP��k_Button3.Size = New System.Drawing.Size(40, 39)
        '
        'Otev��t
        '
        Me.Otev��t.AutoSize = False
        Me.Otev��t.ImageIndex = 2
        Me.Otev��t.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Otev��t.Name = "Otev��t"
        Me.Otev��t.Size = New System.Drawing.Size(40, 39)
        Me.Otev��t.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Otev��t.ToolTipText = "Otev��t obr�zek"
        '
        'Ulo�it
        '
        Me.Ulo�it.AutoSize = False
        Me.Ulo�it.ImageIndex = 0
        Me.Ulo�it.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Ulo�it.Name = "Ulo�it"
        Me.Ulo�it.Size = New System.Drawing.Size(40, 39)
        Me.Ulo�it.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Ulo�it.ToolTipText = "Ulo�it obr�zek"
        '
        'Tisk
        '
        Me.Tisk.AutoSize = False
        Me.Tisk.ImageIndex = 3
        Me.Tisk.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Tisk.Name = "Tisk"
        Me.Tisk.Size = New System.Drawing.Size(40, 39)
        Me.Tisk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Tisk.ToolTipText = "Vytisknout"
        '
        'Rozm�ry
        '
        Me.Rozm�ry.AutoSize = False
        Me.Rozm�ry.ImageIndex = 4
        Me.Rozm�ry.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.Rozm�ry.Name = "Rozm�ry"
        Me.Rozm�ry.Size = New System.Drawing.Size(40, 39)
        Me.Rozm�ry.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me.Rozm�ry.ToolTipText = "Srovnat rozm�ry pro tisk"
        '
        'PD
        '
        Me.PD.DocumentName = "Frakt�ln�k"
        '
        'obrFr
        '
        Me.obrFr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.obrFr.Location = New System.Drawing.Point(40, 69)
        Me.obrFr.Name = "obrFr"
        Me.obrFr.Size = New System.Drawing.Size(121, 50)
        Me.obrFr.TabIndex = 5
        Me.obrFr.TabStop = False
        '
        'frmFrakt�ln�k
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(312, 315)
        Me.Controls.Add(Me.obrFr)
        Me.Controls.Add(Me.sbrStav)
        Me.Controls.Add(Me.obrTisk)
        Me.Controls.Add(Me.pbKolikJe�t�)
        Me.Controls.Add(Me.tbP��k)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(11, 30)
        Me.Name = "frmFrakt�ln�k"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Frakt�ln�k"
        Me.sbrStav.ResumeLayout(False)
        Me.sbrStav.PerformLayout()
        CType(Me.obrTisk, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbP��k.ResumeLayout(False)
        Me.tbP��k.PerformLayout()
        CType(Me.obrFr, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PD As System.Drawing.Printing.PrintDocument
    Friend WithEvents obrFr As Frakt�ln�k.Kresl�k
#End Region 
End Class