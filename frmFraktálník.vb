Option Strict Off
Option Explicit On
Imports Frakt�ln�k.modSP
Friend Class frmFrakt�ln�k
    Inherits System.Windows.Forms.Form



    Private Sub frmFrakt�ln�k_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        CDOpen.InitialDirectory = My.Application.Info.DirectoryPath & "\Obr�zky"
        CDSave.InitialDirectory = My.Application.Info.DirectoryPath & "\Obr�zky"
        frmV�b�r.Show(Me)
        ObnovNastaven�()
    End Sub

    Private Sub frmFrakt�ln�k_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
        Static V�host As Boolean
        Dim V�� As Short
        With obrFr
            On Error Resume Next
            .SetBounds(0, tbP��k.Height, ClientRectangle.Width, ClientRectangle.Height - tbP��k.Height)
            'V�� = tbP��k.Height + ClientRectangle.Width * (10 / IIf(frmTisk.Star�Form�t, 16, 15))
            V�� = tbP��k.Height + ClientRectangle.Width * (10 / 15)
            .Height = V�� - tbP��k.Height
            If Me.WindowState = System.Windows.Forms.FormWindowState.Normal Then
                Me.Height = Me.Height - Me.ClientRectangle.Height + V��
            End If
            If V�host Then
                V�host = False
                Exit Sub
            End If
            �ik.Pic = obrFr
            '�ik.Size
            V�host = True
        End With
    End Sub

    Private Sub frmFrakt�ln�k_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        End
    End Sub

    Private Sub obrFr_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs)
        With obrFr
        End With
    End Sub

    Private Sub tbP��k_ButtonClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles V�b�r.Click, Epidemia_.Click, _tbP��k_Button3.Click, Otev��t.Click, Ulo�it.Click, Tisk.Click, Rozm�ry.Click
        Dim Button As System.Windows.Forms.ToolStripItem = CType(eventSender, System.Windows.Forms.ToolStripItem)
        Dim B As Object
        Dim T As frmTisk
        Dim V�� As Short
        Dim GrTisk As Graphics
        Select Case Button.Name
            Case "V�b�r"
                With frmV�b�r
                    If .Visible Then .Hide()
                    .Show(Me)
                End With
            Case "Epidemia_"
                With frmEpidemia
                    If .Visible Then .Hide()
                    .Show(Me)
                End With
            Case "Otev��t"
                On Error GoTo vypadni
                System.Windows.Forms.Application.DoEvents()
                CDOpen.DefaultExt = ".bmp"
                CDSave.DefaultExt = CDOpen.DefaultExt
                CDOpen.Filter = "obr�zky (*.bmp;*.pcx;*.jpg;*.gif;*.cur;*ico)|*.bmp;*.pcx;*.jpg;*.gif;*.cur;*ico"
                CDSave.Filter = CDOpen.Filter
                CDOpen.ShowReadOnly = False
                CDOpen.CheckFileExists = True
                CDOpen.CheckPathExists = True
                CDSave.CheckPathExists = True
                If CDOpen.ShowDialog() <> Windows.Forms.DialogResult.OK Then GoTo vypadni
                CDSave.FileName = CDOpen.FileName
                obrFr.Image = System.Drawing.Image.FromFile(CDOpen.FileName)
            Case "Ulo�it"
                On Error GoTo vypadni
                System.Windows.Forms.Application.DoEvents()
                CDOpen.DefaultExt = ".png"
                CDSave.DefaultExt = CDOpen.DefaultExt
                CDOpen.Filter = "PNG (*.png)|*.png"
                CDSave.Filter = CDOpen.Filter
                CDOpen.ShowReadOnly = False
                If CDSave.ShowDialog() <> Windows.Forms.DialogResult.OK Then GoTo vypadni
                CDOpen.FileName = CDSave.FileName
                obrTisk.SetBounds(0, 0, obrFr.Width, obrFr.Height)
                obrTisk.Image = New Bitmap(obrFr.ClientSize.Width, obrFr.ClientSize.Height)
                'obrTisk.PaintPicture(obrFr.Image, 0, 0,  ,  ,  ,  , obrFr.ClientRectangle.Width, obrFr.ClientRectangle.Height)
                GrTisk = Graphics.FromImage(obrTisk.Image) 'obrTisk.CreateGraphics
                GrTisk.DrawImage(obrFr.Z�sobn�k, 0, 0)
                obrTisk.Image.Save(CDSave.FileName)
            Case "Tisk"
                '        Dim Odp As VbMsgBoxResult
                '        Odp = MsgBox("Tisknout na pravou polovinu pap�ru?", vbYesNoCancel + vbQuestion + vbDefaultButton2, "Tisk obr�zku")
                T = frmTisk
                T.Text = "Tisk"
                'VB6.ShowForm(T, 1, Me)
                T.ShowDialog(Me)
                If Not T.V�host Then
                    obrTisk.SetBounds(0, 0, obrFr.Width, obrFr.Height)
                    obrTisk.Image = Nothing
                    'obrTisk.PaintPicture(obrFr.Image, 0, 0,  ,  ,  ,  , obrFr.ClientRectangle.Width, obrFr.ClientRectangle.Height)
                    GrTisk = obrTisk.CreateGraphics
                    GrTisk.DrawImage(obrFr.Image, obrFr.ClientRectangle)

                    'Printer.ScaleMode = vbCentimeters
                    '             If T.Star�Form�t Then
                    '                 Printer.Orientation = vbPRORLandscape
                    '                 If T.Prav�Polovina Then
                    '                     Printer.Line((0, 10.2) - (16, 20.2), 0, B)
                    '                     Printer.PaintPicture(obrTisk.Image, 0.5, 0.5 + 10.2, 15, 9)
                    '                 Else
                    '                     Printer.Line((0, 0) - (16, 10), 0, B)
                    '                     Printer.PaintPicture(obrTisk.Image, 0.5, 0.5, 15, 9)
                    '                 End If
                    '             Else
                    PD.DefaultPageSettings.Landscape = False
                    'Printer.PrintQuality = 1200

                    Dim Res As New Printing.PrinterResolution
                    Res.X = 1200
                    Res.Y = 1200
                    PD.DefaultPageSettings.PrinterResolution = Res

                    PD.Print() ' P�i ud�losti PrintPage se obr�zek teprve po�le na tisk�rnu
                End If
            Case "Rozm�ry"
                'Odp = MsgBox("Zm�nit rozm�ry na nov� form�t?", vbYesNoCancel, "Rozm�ry")
                frmTisk.Text = "Rozm�ry"
                'VB6.ShowForm(frmTisk, 1, Me)
                frmTisk.ShowDialog(Me)
                'If frmTisk.Star�Form�t Then
                '   V�� = tbP��k.Height + ClientRectangle.Width * (10 / 16)
                'Else
                V�� = tbP��k.Height + ClientRectangle.Width * (10 / 15)
                'End If
                obrFr.Height = V�� - tbP��k.Height
                If Me.WindowState = System.Windows.Forms.FormWindowState.Normal Then
                    Me.Height = Me.Height - Me.ClientRectangle.Height + V��
                End If
        End Select
vypadni:
    End Sub



    Sub Po��tej()
        Dim �X, �Y As Integer
        Dim Y, X, Z As Double
        Dim Z2, Z1, Z3 As Double
        Dim N, M, O As Double
        Dim G, R, B As Integer
        Dim ���, V�� As Object
        Dim St�edX, St�edY As Object
        Dim N�hProlnut� As New GenerN
        Dim N�hRozptyl As New GenerN
        N�hProlnut�.Smallest = -1
        N�hProlnut�.Largest = 0
        Dim I As Integer
        Dim Lin As Dru�ina
        Dim Ep As Dru�ina
        Dim Koleno As TTrpasl�k
        Dim StKolena, NoKolena As Dru�ina
        Dim J As Short
        Dim Za�D�lV�tve As Integer
        Dim Polom�r As Object
        Dim Zn As Object
        Dim ZX, CX, CY, ZY As Object
        Dim Cc As New CComplexNumber
        Dim Zc As New CComplexNumber
        'Dim Gr As Graphics = Graphics.FromHwnd(obrFr.Handle)
        'Dim Gr As Graphics = obrFr.CreateGraphics
        'If obrFr.Image Is Nothing Then
        '    obrFr.Image = New Bitmap(obrFr.ClientSize.Width, obrFr.ClientSize.Height)
        'End If
        'Dim Gr As Graphics = Graphics.FromImage(obrFr.Image)
        With obrFr
            .Cursor = System.Windows.Forms.Cursors.WaitCursor
            ��� = .ClientRectangle.Width
            V�� = .ClientRectangle.Height
            St�edX = ��� / 2
            St�edY = V�� / 2
            If frmV�b�r.chbVy�istit.CheckState = System.Windows.Forms.CheckState.Checked Then
                .Image = Nothing
                .BackColor = System.Drawing.Color.White
            End If
            ' tady se vypne p�eru�ovac� p��znak - na konci cyklu se vyp�nat nemus�
            P�eru�it = False
            Select Case frmV�b�r.sezV�b�r.Text
                Case "Epidemia"
                    P�ipravMapu()
                    modSP.Epidemia(Dru�(�ik(St�edX, St�edY)))
                Case "Epidemia mnoho�etn�"
                    modSP.Epidemia(, TypP�enosu.tpN�hodn�Zm�na)
                Case "Epidemia plynul� 1"
                    modSP.Epidemia(, TypP�enosu.tpP��r�stek, TypSoused�.tsN�strann�, Po�ad�Soused�.psN�hodn�)
                Case "Epidemia plynul� 2"
                    modSP.Epidemia(, TypP�enosu.tpV�t��P��r�stek, TypSoused�.tsN�strann�, Po�ad�Soused�.psN�hodn�)
                Case "Epidemia nepravideln�"
                    modSP.Epidemia(, TypP�enosu.tpP��r�stek, TypSoused�.tsV�ichni, Po�ad�Soused�.psN�hodn�, 0)
                Case "Epidemia nepravideln� paprs�it�"
                    modSP.Epidemia(, TypP�enosu.tpN�hodn�Zm�na, TypSoused�.tsV�ichni, Po�ad�Soused�.psN�hodn�, 0, , , 10)
                Case "Linie"
                    modSP.Linia(, , , 1000)
                Case "Ach�t"
                    modSP.Epidemia(modSP.Linia(St�edX, St�edY, , 1000), TypP�enosu.tpP��r�stek, TypSoused�.tsN�hodn�N�strann��iN�ro�n�, Po�ad�Soused�.psN�hodn�, 0, , False, , , , , False)
                Case "Ach�t paprs�it�"
                    modSP.Epidemia(Linia(St�edX, St�edY, , 1000), TypP�enosu.tpN�hodn�Zm�na, TypSoused�.tsN�hodn�N�strann��iN�ro�n�, Po�ad�Soused�.psN�hodn�, 0, , False, , , , , False)
                Case "Slunce v chrp�"
                    P�ipravMapu()
                    For I = 1 To 64
                        If P�eru�it Then Exit For
                        '�ik(St�edX, St�edY).SmEp(tsV�ichni) = N�hoda(1, 2, 3, 4, 5, 6, 7, 8)
                        �ik(St�edX, St�edY).SmEp() = I
                        modSP.Epidemia(Dru�(�ik(St�edX, St�edY)), TypP�enosu.tpV�t��P��r�stek, TypSoused�.tsV�ichni, Po�ad�Soused�.psN�hodn�, 1, 1, True, , , , , I = 1)
                    Next
                Case "M�lochlupatec"
                    Lin = Linia(St�edX, St�edY, , 1000)
                    For I = 1 To 10
                        If P�eru�it Then Exit For
                        '�ik(St�edX, St�edY).SmEp(tsV�ichni) = N�hoda(1, 2, 3, 4, 5, 6, 7, 8)
                        modSP.Epidemia(Lin, TypP�enosu.tpV�t��P��r�stek, TypSoused�.tsV�ichni, Po�ad�Soused�.psN�hodn�, 1, -1, False, , , , , False)
                    Next
                Case "Dlouhochlupatec"
                    Lin = Linia(St�edX, St�edY, , 1000)
                    For I = 1 To 10
                        If P�eru�it Then Exit For
                        modSP.Epidemia(Lin, TypP�enosu.tpV�t��P��r�stek, TypSoused�.tsV�ichni, Po�ad�Soused�.psN�hodn�, 1, 1, False, , , , , False)
                    Next
                Case "Dlouchochlupatec ��lov�"
                    Lin = modSP.Linia(St�edX, St�edY, , 1000)
                    For I = 1 To 20
                        If P�eru�it Then Exit For
                        modSP.Epidemia(Lin, TypP�enosu.tpV�t��P��r�stek, TypSoused�.tsV�ichni, Po�ad�Soused�.psN�hodn�, Dvojn�hoda(2, 1, 0.3), 1, False, , , , , False)
                    Next
                Case "Dlouhochlupatec jednosm�rn�"
                    P�ipravMapu()
                    �ik(St�edX, St�edY).SmEp() = 5
                    �ik(St�edX, St�edY).Hodnota = 64
                    Lin = modSP.Linia(St�edX, St�edY, , 1000)
                    modSP.EpidemiaSm�rov�(Lin, TypP�enosu.tpRostouc�P��r�stek, Po�ad�Soused�.psN�hodn�, 1, False, 0, 0, 0, 0, False, 4, 5, 6)
                Case "Dlouhochlupatec jednosm�rn� obalen�"
                    P�ipravMapu()
                    �ik(St�edX, St�edY).SmEp() = 5
                    �ik(St�edX, St�edY).Hodnota = 64
                    Lin = Linia(St�edX, St�edY, , 1000)
                    Ep = EpidemiaSm�rov�(Lin, TypP�enosu.tpRostouc�P��r�stek, Po�ad�Soused�.psN�hodn�, 1, False, 0, 0, 0, 0, False, 4, 5, 6)
                    modSP.Epidemia(Ep, TypP�enosu.tpBezeZm�ny, TypSoused�.tsN�ro�n�, Po�ad�Soused�.psN�hodn�, , , False, , , , 100, False)
                Case "Vlo�ka"
                    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
                    ��� = .ClientRectangle.Width
                    V�� = .ClientRectangle.Height
                    St�edX = ��� / 2
                    St�edY = V�� / 2
                    P�ipravMapu()
                    'Za�D�lV�tve = St�edY / 3.2
                    Za�D�lV�tve = 64
                    Koleno = �ik(St�edX, St�edY)
                    'Koleno.Vrstva = 1
                    NoKolena = New Dru�ina
                    For I = 1 To 7 Step 2
                        Koleno.P�vSmEp() = I
                        'Set Lin = Epidemia(Dru�(Koleno), tpP��r�stek, tsV�ichni, psN�hodn�, 1, 0, True, , , , Za�D�lV�tve / (2 ^ J), False)
                        Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve, False, I)
                        NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve - 1)(1))
                        If P�eru�it Then Exit For
                    Next
                    Do While Za�D�lV�tve / (2 ^ (J + 1)) >= 1
                        If P�eru�it Then Exit Do
                        StKolena = NoKolena
                        NoKolena = New Dru�ina
                        For Each Koleno In StKolena
                            If P�eru�it Then Exit For
                            Koleno.Vrstva = 1
                            ' v�tev rovn�
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, Koleno.P�vSmEp())
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J))(1))
                            ' v�tev doleva
                            Koleno.P�vSmEp = �Sm(Koleno.P�vSmEp() - 1)
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp()))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J))(1))
                            ' v�tev doprava
                            Koleno.P�vSmEp = �Sm(Koleno.P�vSmEp() + 2)
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp()))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J))(1))
                        Next Koleno
                        J = J + 1
                    Loop
                Case "Vlo�ka oto�en�"
                    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
                    ��� = .ClientRectangle.Width
                    V�� = .ClientRectangle.Height
                    St�edX = ��� / 2
                    St�edY = V�� / 2
                    P�ipravMapu()
                    'Za�D�lV�tve = St�edY / 3.2
                    Za�D�lV�tve = 64
                    Koleno = �ik(St�edX, St�edY)
                    'Koleno.Vrstva = 1
                    NoKolena = New Dru�ina
                    For I = 2 To 8 Step 2
                        Koleno.P�vSmEp() = I
                        'Set Lin = Epidemia(Dru�(Koleno), tpP��r�stek, tsV�ichni, psN�hodn�, 1, 0, True, , , , Za�D�lV�tve / (2 ^ J), False)
                        Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve, False, I)
                        If P�eru�it Then Exit For
                        NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve)(1))
                        If P�eru�it Then Exit For
                    Next
                    Do While Za�D�lV�tve / (2 ^ (J + 1)) >= 1
                        If P�eru�it Then Exit Do
                        StKolena = NoKolena
                        NoKolena = New Dru�ina
                        For Each Koleno In StKolena
                            If P�eru�it Then Exit For
                            Koleno.Vrstva = 1
                            ' v�tev rovn�
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, Koleno.P�vSmEp)
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J))(1))
                            ' v�tev doleva
                            Koleno.P�vSmEp() = Koleno.P�vSmEp() - 1
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, Koleno.P�vSmEp)
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J))(1))
                            ' v�tev doprava
                            Koleno.P�vSmEp() = Koleno.P�vSmEp() + 2
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psPravideln�, 0, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, Koleno.P�vSmEp)
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J))(1))
                        Next Koleno
                        J = J + 1
                    Loop
                Case "Strome�ek"
                    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
                    ��� = .ClientRectangle.Width
                    V�� = .ClientRectangle.Height
                    St�edX = ��� / 2
                    St�edY = V�� / 2
                    P�ipravMapu()
                    Za�D�lV�tve = St�edY / 3.2
                    'Za�D�lV�tve = 64
                    Koleno = �ik(St�edX, St�edY)
                    'Koleno.Vrstva = 1
                    NoKolena = New Dru�ina
                    For I = 2 To 8 Step 2
                        Koleno.P�vSmEp() = I
                        'Set Lin = Epidemia(Dru�(Koleno), tpP��r�stek, tsV�ichni, psN�hodn�, 1, 0, True, , , , Za�D�lV�tve \ (2 ^ J), False)
                        Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve, False, �Sm(I - 1), I, �Sm(I + 1))
                        If P�eru�it Then Exit For
                        NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve - 1)(1))
                        If P�eru�it Then Exit For
                    Next
                    Do While Za�D�lV�tve \ (2 ^ (J + 1)) >= 1
                        If P�eru�it Then Exit Do
                        StKolena = NoKolena
                        NoKolena = New Dru�ina
                        For Each Koleno In StKolena
                            If P�eru�it Then Exit For
                            'Koleno.Vrstva = 1 ' vrstva je posl�ze p�eps�na - ohniska dost�vaj� vrstvu 0
                            ' v�tev rovn�
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve \ (2 ^ J), False, �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve \ (2 ^ J) - 1)(1))
                            ' v�tev doleva
                            Koleno.P�vSmEp() = Koleno.P�vSmEp() - 1
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve \ (2 ^ J), False, �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve \ (2 ^ J) - 1)(1))
                            ' v�tev doprava
                            Koleno.P�vSmEp() = Koleno.P�vSmEp() + 2
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve \ (2 ^ J), False, �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve \ (2 ^ J) - 1)(1))
                        Next Koleno
                        J = J + 1
                    Loop
                Case "Strome�ek jednosm�rn�"
                    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
                    ��� = .ClientRectangle.Width
                    V�� = .ClientRectangle.Height
                    St�edX = ��� / 2
                    St�edY = V�� / 2
                    P�ipravMapu()
                    'Za�D�lV�tve = St�edY / 3.2
                    Za�D�lV�tve = 64
                    Koleno = �ik(St�edX, St�edY)
                    'Koleno.Vrstva = 1
                    NoKolena = New Dru�ina
                    For I = 2 To 8 Step 2
                        Koleno.P�vSmEp = I
                        'Set Lin = Epidemia(Dru�(Koleno), tpP��r�stek, tsV�ichni, psN�hodn�, 1, 0, True, , , , Za�D�lV�tve / (2 ^ J), False)
                        Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, False, Za�D�lV�tve, False, �Sm(I - 1), I, �Sm(I + 1))
                        If P�eru�it Then Exit For
                        NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve - 1)(1))
                        If P�eru�it Then Exit For
                    Next
                    Do While Za�D�lV�tve / (2 ^ (J + 1)) >= 1
                        If P�eru�it Then Exit Do
                        StKolena = NoKolena
                        NoKolena = New Dru�ina
                        For Each Koleno In StKolena
                            If P�eru�it Then Exit For
                            Koleno.Vrstva = 1
                            ' v�tev rovn�
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, False, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J) - 1)(1))
                            ' v�tev doleva
                            'Koleno.P�vSmEp(tsV�ichni) = Koleno.P�vSmEp(tsV�ichni) - 1
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp - 2), �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J) - 1)(1))
                            ' v�tev doprava
                            'Koleno.P�vSmEp(tsV�ichni) = Koleno.P�vSmEp(tsV�ichni) + 2
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1), �Sm(Koleno.P�vSmEp + 2))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J) - 1)(1))
                        Next Koleno
                        J = J + 1
                    Loop
                Case "Strome�ek s chybou"
                    Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
                    ��� = .ClientRectangle.Width
                    V�� = .ClientRectangle.Height
                    St�edX = ��� / 2
                    St�edY = V�� / 2
                    P�ipravMapu()
                    'Za�D�lV�tve = St�edY / 3.2
                    Za�D�lV�tve = 64
                    Koleno = �ik(St�edX, St�edY)
                    'Koleno.Vrstva = 1
                    NoKolena = New Dru�ina
                    For I = 2 To 8 Step 2
                        Koleno.P�vSmEp() = I
                        'Set Lin = Epidemia(Dru�(Koleno), tpP��r�stek, tsV�ichni, psN�hodn�, 1, 0, True, , , , Za�D�lV�tve / (2 ^ J), False)
                        Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve, False, �Sm(I - 1), I, �Sm(I + 1))
                        If P�eru�it Then Exit For
                        NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve - 1)(1))
                        If P�eru�it Then Exit For
                    Next
                    Do While Za�D�lV�tve / (2 ^ (J + 1)) >= 1
                        If P�eru�it Then Exit Do
                        StKolena = NoKolena
                        NoKolena = New Dru�ina
                        For Each Koleno In StKolena
                            If P�eru�it Then Exit For
                            Koleno.Vrstva = 1
                            ' v�tev rovn�
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J) - 1)(1))
                            ' v�tev doleva
                            Koleno.P�vSmEp() = Koleno.SmEp() - 1
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp - 2), �Sm(Koleno.P�vSmEp - 1), �Sm(Koleno.P�vSmEp))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J) - 1)(1))
                            ' v�tev doprava
                            Koleno.P�vSmEp() = Koleno.SmEp() + 2
                            Lin = EpidemiaSm�rov�(Dru�(Koleno), TypP�enosu.tpP��r�stek, Po�ad�Soused�.psN�hodn�, 1, True, 0, 0, 0, Za�D�lV�tve / (2 ^ J), False, �Sm(Koleno.P�vSmEp), �Sm(Koleno.P�vSmEp + 1), �Sm(Koleno.P�vSmEp + 2))
                            ' p�idat konec do p��t�ch kolen
                            If P�eru�it Then Exit For
                            NoKolena.P�idej(Lin.Vrstva(Za�D�lV�tve / (2 ^ J) - 1)(1))
                        Next Koleno
                        J = J + 1
                    Loop
                Case Else
                    For �Y = 0 To .ClientRectangle.Height - 1
                        If P�eru�it Then
                            P�eru�it = False
                            Exit For
                        End If
                        For �X = 0 To .ClientRectangle.Width - 1
                            If P�eru�it Then
                                P�eru�it = False
                                Exit For
                            End If
                            X = �X - frmV�b�r.chbVyst�edit.CheckState * St�edX
                            Y = �Y - frmV�b�r.chbVyst�edit.CheckState * St�edY
                            'On Error Resume Next
                            Select Case frmV�b�r.sezV�b�r.Text
                                Case "Z = X * Y * 5000"
                                    Z = X * Y * 5000
                                Case "Z = Y * (Sin(X) + 2)"
                                    Z = Y * (System.Math.Sin(X) + 2) '*500
                                Case "Z = (X ^ 2 + Y ^ 2)"
                                    Z = (X ^ 2 + Y ^ 2) '/ 1000
                                Case "Z = (X ^ 2 * Y ^ 2) / 1500"
                                    Z = (X ^ 2 * Y ^ 2) / 1500
                                Case "Z = X ^ (1 / 3) * Y ^ (1 / 3) * 5000"
                                    Z = X ^ (1 / 3) * Y ^ (1 / 3) * 5000
                                Case "Z = (X ^ (1 / 3) + Y ^ (1 / 3)) * 5000"
                                    Z = (X ^ (1 / 3) + Y ^ (1 / 3)) * 5000
                                Case "Zrcadlen�"
                                    If Y = 0 Then Y = 0.01
                                    Z = X / Y * 6000 + Y * 500
                                Case "Z = X Or Y"
                                    Z = X Or Y
                                Case "Z = X ^ (Y / 175)"
                                    Z = X ^ (Y / 175)
                                Case "Z = (X ^ 4 - Y ^ 4) / 5000"
                                    Z = (X ^ 4 - Y ^ 4) / 5000
                                Case "Z = X ^ 3 - Y ^ 3"
                                    Z = X ^ 3 - Y ^ 3
                                Case "Z = X ^ 2 - Y ^ 2"
                                    Z = X ^ 2 - Y ^ 2
                                Case "Z = X ^ 3 Or Y ^ 2"
                                    Z = X ^ 3 Or Y ^ 2
                                Case "Z = X ^ 2 And Y ^ 4"
                                    Z = X ^ 2 And Y ^ 4
                                Case "Z = X ^ 2"
                                    Z = X ^ 2
                                Case "Z = (X Mod Y) * 1000"
                                    Z = (X Mod Y) * 1000
                                Case "Flanel"
                                    Z = RGB((System.Math.Sin(X) + 1) * 255, (System.Math.Sin(Y) + 1) * 255, (System.Math.Cos(X) + 1) * 255)
                                Case "Voda"
                                    Polom�r = (X ^ 2 + Y ^ 2) ^ (1 / 2)
                                    R = ((System.Math.Sin(Polom�r ^ 2 / 1000) + 1) / 3 - System.Math.Sin(X / 500)) * 255
                                    G = ((System.Math.Sin(Polom�r ^ 2 / 1000) + 1) / 3 - System.Math.Sin(X / 500)) * 255
                                    B = 255
                                    If R < 0 Then R = 0
                                    If G < 0 Then G = 0
                                    Z = RGB(R, G, B)
                                Case "Malt�zsk� k��"
                                    Z = RGB((System.Math.Sin(((X * 5) ^ 2 + (Y * 5) ^ 2) ^ (1 / 2)) + 1) * 255, (System.Math.Sin(((X * 5) ^ 2 + (Y * 5) ^ 2) ^ (1 / 2)) + 1) * 255, 255)
                                Case "Zelen� malt�zsk� k��"
                                    Z = RGB((System.Math.Sin(((X * 5) ^ 2 + (Y * 5) ^ 2) ^ (1 / 2)) + 1) * 255, 255, (System.Math.Sin(((X * 5) ^ 2 + (Y * 5) ^ 2) ^ (1 / 2)) + 1) * 255)
                                Case "Barevn� malt�zsk� k��"
                                    Z = RGB((System.Math.Cos(((X * 4) ^ 2 + (Y * 4) ^ 2) ^ (1 / 2)) + 1) * 255, (System.Math.Sin(((X * 4) ^ 2 + (Y * 4) ^ 2) ^ (1 / 2)) + 1) * 255, (System.Math.Cos(((X * 4) ^ 2 + (Y * 4) ^ 2) ^ (1 / 2)) + 1) * 255)
                                Case "Kyti�ka"
                                    Z = RGB((System.Math.Cos((System.Math.Abs(X * 100) ^ 2 + System.Math.Abs(Y * 100) ^ 2) ^ (1 / 3)) + 1) * 255, (System.Math.Sin((System.Math.Abs(X * 100) ^ 2 + System.Math.Abs(Y * 100) ^ 2) ^ (1 / 3)) + 1) * 255, (System.Math.Cos((System.Math.Abs(X * 100) ^ 2 + System.Math.Abs(Y * 100) ^ 2) ^ (1 / 3)) + 1) * 255)
                                Case "Bouli�ky"
                                    Z = RGB((System.Math.Cos((System.Math.Abs(X * 2) ^ 3 + System.Math.Abs(Y * 2) ^ 3) ^ (1 / 2)) + 1) * 255, (System.Math.Sin((System.Math.Abs(X * 2) ^ 3 + System.Math.Abs(Y * 2) ^ 3) ^ (1 / 2)) + 1) * 255, (System.Math.Cos((System.Math.Abs(X * 2) ^ 3 + System.Math.Abs(Y * 2) ^ 3) ^ (1 / 2)) + 1) * 255)
                                Case "Pavoukovy o�i"
                                    Z = RGB((System.Math.Cos(System.Math.Abs(X * 1000000) ^ (1 / 3) + System.Math.Abs(Y * 1000000) ^ (1 / 3)) + 1) * 255, (System.Math.Sin(System.Math.Abs(X * 1000000) ^ (1 / 3) + System.Math.Abs(Y * 1000000) ^ (1 / 3)) + 1) * 255, (System.Math.Cos(System.Math.Abs(X * 1000000) ^ (1 / 3) + System.Math.Abs(Y * 1000000) ^ (1 / 3)) + 1) * 255)
                                Case "Trubky"
                                    On Error Resume Next
                                    Z = RGB(System.Math.Sin(((X / 2) ^ 2 + (Y / 2) ^ 2) ^ (1 / 2)) * 255, System.Math.Tan(((X / 2) ^ 2 + (Y / 2) ^ 2) ^ (1 / 2)) * 255, 255)
                                    On Error GoTo 0
                                Case "Lamely"
                                    Z = System.Math.Tan(X / 20) + System.Math.Abs(Y) ^ (1 / 3)
                                    Z = Fasis(Z, 20, System.Drawing.Color.White, System.Drawing.Color.Blue, System.Drawing.Color.Magenta, System.Drawing.Color.Black, System.Drawing.Color.Lime)
                                Case "Lamely vlnov�"
                                    Z = System.Math.Tan(X / 20) + System.Math.Abs(Y) ^ (1 / 3) * System.Math.Sign(Y)
                                    'If Y < 0 Then Z = -Z
                                    Z = Fasis(Z, 20, System.Drawing.Color.White, System.Drawing.Color.Blue, System.Drawing.Color.Magenta, System.Drawing.Color.Black, System.Drawing.Color.Lime)
                                Case "Lamely prou�kovan�"
                                    Z = System.Math.Tan(X / 20) + System.Math.Abs(Y) ^ (1 / 3)
                                    Z = Fasis(Z, 1, System.Drawing.Color.White, System.Drawing.Color.Blue, System.Drawing.Color.Magenta, System.Drawing.Color.Black, System.Drawing.Color.Lime)
                                Case "Duha"
                                    Z = ((X * 32) ^ 2 + (Y * 32) ^ 2) ^ (1 / 2)
                                    Z = Fasis(Z, 20, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Lime)
                                Case "Modulo magnum"
                                    Z = ModuloMagnum(X, Y)
                                    Z = Fasis(Z, 20, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Cyan, System.Drawing.Color.Lime)
                                Case "Sklen�n� kapky"
                                    Z = (X / 2) ^ 3 + (Y / 2) ^ 3
                                    Z = Fasis(Z, 5000, System.Drawing.Color.White, System.Drawing.Color.Blue, System.Drawing.Color.Magenta, System.Drawing.Color.Black, System.Drawing.Color.Lime)
                                Case "Oko 1"
                                    Z = (X / 2) ^ 3 + (Y / 2) ^ 3
                                    Z = Fasis(Z, 35000, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Black, System.Drawing.Color.Lime, System.Drawing.Color.Yellow)
                                Case "Oko 2"
                                    Z = (X / 2.75) ^ 5 - (Y / 2.75) ^ 5
                                    Z = Fasis(Z, 100000000, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Yellow, System.Drawing.Color.Lime)
                                Case "P�loko"
                                    Z = (X / 2) ^ 3 + (Y / 2) ^ 3
                                    Z = Fasis(Z, 30000, System.Drawing.Color.White, System.Drawing.Color.Blue, _
                                        System.Drawing.Color.Magenta, System.Drawing.Color.Black, _
                                        System.Drawing.Color.Lime, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White, System.Drawing.Color.White, _
                                        System.Drawing.Color.White)
                                Case "Kulat� �tverec"
                                    Z = (X / 2.75) ^ 4 + (Y / 2.75) ^ 4
                                    Z = Fasis(Z, 1000000, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Cyan, System.Drawing.Color.Lime, System.Drawing.Color.Yellow, System.Drawing.Color.Red)
                                Case "Z�vitky 1"
                                    Z = (X * Y) ^ 2.0#
                                    Z = Fasis(Z, 100000, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Black, System.Drawing.Color.Lime, System.Drawing.Color.Yellow)
                                Case "Z�vitky 2"
                                    Z = (X / 6) ^ 3 * (Y / 6) ^ 3
                                    Z = Fasis(Z, 1000000, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Lime, System.Drawing.Color.Yellow)
                                Case "Hv�zdice"
                                    Z = (X * Y)
                                    Z = Fasis(Z, 50, System.Drawing.Color.White, System.Drawing.Color.Magenta, System.Drawing.Color.Blue, System.Drawing.Color.Black, System.Drawing.Color.Lime, System.Drawing.Color.Yellow)
                                Case "Horizonty"
                                    If Y = 0 Then
                                        Z = X
                                    Else
                                        Z = (X / Y) * 20000
                                    End If
                                    Z = Fasis(Z, 32, System.Drawing.Color.White, System.Drawing.Color.Magenta, _
                                             System.Drawing.Color.Blue, System.Drawing.Color.Lime, _
                                             System.Drawing.Color.Yellow)
                                Case "Horizonty zv�t�en�"
                                    If Y = 0 Then
                                        Z = X
                                    Else
                                        Z = (X / Y) * 20000
                                    End If
                                    Z = Fasis(Z, 10, (System.Drawing.Color.White), (System.Drawing.Color.Magenta), (System.Drawing.Color.Blue), (System.Drawing.Color.Lime), (System.Drawing.Color.Yellow))
                                Case "Ma�le prou�kovan�"
                                    If Y = 0 Then
                                        Z = 0
                                    Else
                                        Z = ModuloMagnum(X * 100, Y ^ 2)
                                    End If
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Ma�le 1"
                                    If Y = 0 Then
                                        Z = X
                                    Else
                                        Z = ModuloMagnum(X * 100, Y ^ 2)
                                    End If
                                    Z = Fasis(Z, 100, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Ma�le 2"
                                    If Y = 0 Then
                                        Z = X
                                    Else
                                        Z = ModuloMagnum(X * 100, Y ^ 2)
                                    End If
                                    Z = Fasis(Z, 400, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "P�v 1"
                                    If X = 0 Then
                                        Z = Y
                                    Else
                                        Z = Y ^ 5 / X ^ 2
                                    End If
                                    Z = Fasis(Z, 50000, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "P�v 2"
                                    If Y = 0 Then
                                        Z = X
                                    Else
                                        Z = (X * 2 / 3) ^ 5 / (Y) ^ 2
                                    End If
                                    Z = Fasis(Z, 50000, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Prou�ky"
                                    Z = Fasis(X, 100, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.Black), (System.Drawing.Color.Lime))
                                Case "Zrcadlen� plynul�"
                                    Y = Y + 0.001
                                    Z = ((X / (Y)) * 120 + Y) * 5
                                    Z = Fasis(Z, 600, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Zrcadlen� plynul� posunut�"
                                    X = X + 490
                                    Y = Y + 300.001
                                    Z = ((X / (Y)) * 120 + Y) * 5
                                    Z = Fasis(Z, 600, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "�ikm� prou�ky"
                                    Z = (X + Y) ^ 2 / 10 ^ (1 / 2)
                                    Z = Fasis(Z, 100, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Sloupy pevn�"
                                    Z = Y * (X - System.Math.Sign(X) * 20) ^ 2 / 60
                                    Z = Fasis(Z, 100, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Dvojparabola"
                                    Zn = IIf(System.Math.Sign(Y) = 0, 1, System.Math.Sign(Y))
                                    Z = ((X + 100) * 10 + (Y - Zn * 50) ^ 2 / 4) / 40
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Dvojparabola kropenat�"
                                    Zn = IIf(System.Math.Sign(Y) = 0, 1, System.Math.Sign(Y))
                                    Z = ((X + 100) * 10 + (Y - Zn * 50) ^ 2 / 4) / 40
                                    Z = Rozptyl(Z, 5)
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Dvojparabola velekropenat�"
                                    Zn = IIf(System.Math.Sign(Y) = 0, 1, System.Math.Sign(Y))
                                    Z = ((X + 100) * 10 + (Y - Zn * 50) ^ 2 / 4) / 40
                                    Z = Rozptyl(Z, 50)
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Logaritmus"
                                    Z = System.Math.Log(System.Math.Abs(Y) + 0.0000000001) * 200 + (X + 165) / 2
                                    Z = Fasis(Z, 10, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Dvojparabola a logaritmus"
                                    Zn = IIf(System.Math.Sign(Y) = 0, 1, System.Math.Sign(Y))
                                    M = ((X + 100) * 10 + (Y - Zn * 50) ^ 2 / 4) / 40
                                    M = Fasis(M, 20, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                    N = System.Math.Log(System.Math.Abs(Y) + 0.0000000001) * 200 + (X + 165) / 2
                                    N = Fasis(N, 10, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                    Z = IIf(N�hProlnut�.Value, M, N)
                                Case "Zrcadlen� kropenat� 1"
                                    Z = ((X / (Y + 0.001)) * 120 + Y) * 5
                                    Z = Fasis(Z, 600, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                    'Z = Dvojn�hoda(Z, vbWhite, 0.5)
                                    Z = N�hoda(Z, ColorTranslator.ToOle(System.Drawing.Color.White), ColorTranslator.ToOle(System.Drawing.Color.Lime))
                                Case "Zrcadlen� kropenat� 2"
                                    Z = ((X / (Y + 0.001)) * 120 + Y) * 5
                                    Z = Fasis(Z, 600, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Black), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                    Z = Dvojn�hoda(Z, ColorTranslator.ToOle(System.Drawing.Color.White), 0.25)
                                    'Z = N�hoda(Z, vbWhite, vbGreen)
                                Case "Hyperparabola"
                                    If X = 0 Then
                                        M = 0
                                    Else
                                        M = (X * Y) / 100
                                    End If
                                    N = ((Y) * 10 + (X) ^ 2 / 4) / 40
                                    Z = (M + N) / 2
                                    'Z = M
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Parahyperbola 1"
                                    If X = 0 Then
                                        M = 0
                                    Else
                                        M = 100 / X
                                    End If
                                    N = ((Y) * 10 + (X) ^ 2 / 4) / 40
                                    Z = (M + N) / 2 + Y
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Parahyperbola 2"
                                    M = (Y) ^ 2 / 100
                                    If Y = 0 Then
                                        N = 0
                                    Else
                                        N = 500 / Y
                                    End If
                                    Z = (M + N + X + 100) / 2
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Parahyperbola prokl�dan�"
                                    Y = Y + 200
                                    If X = 0 Then
                                        M = 0
                                    Else
                                        M = 1000 / X
                                    End If
                                    N = ((Y) * 10 + (X) ^ 2 / 4) / 40
                                    Z = (M + N) / 2 + Y
                                    Z2 = (X / 2) ^ 3 + (Y / 2) ^ 3
                                    Z2 = Fasis(Z2, 5000, (System.Drawing.Color.White), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.Black), (System.Drawing.Color.Lime))
                                    Dim Z2GDI As System.Drawing.Color = System.Drawing.ColorTranslator.FromWin32(Z2)
                                    Z = Fasis(Z, 20, System.Drawing.Color.White, System.Drawing.Color.Red, _
                                              System.Drawing.Color.Yellow, System.Drawing.Color.Lime, _
                                              System.Drawing.Color.Cyan, System.Drawing.Color.Blue, _
                                              System.Drawing.Color.Magenta, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, Z2GDI, _
                                              Z2GDI, Z2GDI, Z2GDI, Z2GDI, Z2GDI, Z2GDI, Z2GDI, Z2GDI, _
                                              Z2GDI, Z2GDI, Z2GDI, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White, _
                                              System.Drawing.Color.White, System.Drawing.Color.White)
                                Case "Xsinparabola"
                                    Z = (X ^ 2 + System.Math.Sin(X / 20) * 5000) / 200 + Y - 50
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Ysinparabola"
                                    Z = (X ^ 2 + System.Math.Sin(Y / 20) * 5000) / 200 + Y - 75
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Vlna"
                                    Y = Y - 30
                                    Z = ((Y ^ 2) * 200 + (Y ^ 3)) / 10000 + X
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "N�hrdeln�k"
                                    M = X ^ 2 / 20 '+ Y * 20
                                    '                N = ((Y ^ 2) * 200 + (Y ^ 3)) / 1000 + X
                                    '                N = ((Y ^ 2) * 200 + (Y ^ 3)) / 10000 + X
                                    N = ((Y ^ 3)) / 10000 '+ X
                                    Z = M + N
                                    Z = Fasis(Z, 150, (System.Drawing.Color.White), (System.Drawing.Color.Red), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Cyan), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))
                                Case "Hrne�ek"
                                    M = X ^ 2 / 50
                                    M = Fasis(M, 200, (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue))
                                    N = Y ^ 2 / 50
                                    N = Fasis(N, 200, (System.Drawing.Color.White), (System.Drawing.Color.Cyan), (System.Drawing.Color.Magenta))
                                    Z = (X ^ 6 + Y ^ 7) / 200000
                                    Z = (X ^ 6 + Y ^ 7) / 200000
                                    Z = Fasis(Z, 200000000, System.Drawing.ColorTranslator.FromWin32(M), System.Drawing.ColorTranslator.FromWin32(N))
                                Case "Ob�lka"
                                    X = (X + St�edX - 200)
                                    Y = (Y + St�edY - 200)
                                    Z = (X ^ 12 + Y ^ 13) / 2.0E+24 + 0.5
                                    Z = Fasis(Z, 1, (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue))
                                Case "Ob�lka centrovan�"
                                    Z = (X ^ 13 + Y ^ 12) / 2.0E+24 + 0.5
                                    Z = Fasis(Z, 1, (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue))
                                Case "XsinYsin"
                                    X = X / 50
                                    Y = Y / 50
                                    Z = (System.Math.Sin(X) + System.Math.Sin(Y)) * 10
                                    Z = Fasis(Z, 10, (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue))
                                Case "Amoeba 1"
                                    X = X / 50 / 3
                                    Y = Y / 50 / 3
                                    Z = ((System.Math.Sin(X + Y) + System.Math.Sin(X - Y)) * 10 + (System.Math.Cos((X + Y) * 5) + System.Math.Cos((X - Y) * 5)))
                                    Z = Fasis(Z, 10, (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue))
                                Case "Amoeba 2"
                                    X = X / 50 / 3
                                    Y = Y / 50 / 3
                                    Z = ((System.Math.Sin(X + Y) + System.Math.Sin(X - Y)) * 10 + (System.Math.Cos((X + Y) * 5) + System.Math.Cos((X - Y) * 5))) * 5
                                    Z = Fasis(Z, 50, (System.Drawing.Color.White), (System.Drawing.Color.Magenta), (System.Drawing.Color.Red), (System.Drawing.Color.Blue), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Ubrus 1"
                                    X = X / 20
                                    Y = (Y + St�edY - 40) / 20
                                    Z = ((System.Math.Sin(X + Y) + System.Math.Sin(X - Y) + System.Math.Abs(X) - Y) * 10 + (System.Math.Cos((X + Y) * 5) + System.Math.Cos((X - Y) * 5))) * 5
                                    If Z > 0 Then Z = 0
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Magenta), (System.Drawing.Color.Red), (System.Drawing.Color.Blue), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Ubrus 2"
                                    X = X / 20
                                    Y = (Y + St�edY - 40) / 20
                                    Z = ((System.Math.Sin(X + Y) + System.Math.Sin(X - Y) + System.Math.Abs(X) - Y) * 10 + (System.Math.Cos((X + Y) * 5) + System.Math.Cos((X - Y) * 5)) * 2) * 10
                                    If Z > 0 Then Z = 0
                                    Z = Fasis(Z, 20, (System.Drawing.Color.White), (System.Drawing.Color.Magenta), (System.Drawing.Color.Red), (System.Drawing.Color.Blue), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Ubrus zak�iven�"
                                    X = X / 20
                                    Y = (Y + St�edY - 100) / 20
                                    Z = ((System.Math.Sin(X + Y) + System.Math.Sin(X - Y) + (X ^ 3) / 500 - Y) * 10 + (System.Math.Cos((X + Y) * 5) + System.Math.Cos((X - Y) * 5))) * 2
                                    If Z > 0 Then Z = 0
                                    Z = FasisC(Z, 50, (System.Drawing.Color.White), (System.Drawing.Color.Magenta), (System.Drawing.Color.Red), (System.Drawing.Color.Blue), (System.Drawing.Color.Yellow), (System.Drawing.Color.Lime))
                                Case "Vzep�t�"
                                    '                Zn = IIf(Sgn(Y) = 0, 1, Sgn(Y))
                                    '                M = ((X + 100) * 10 + (Y) ^ 2 / 4) / 40
                                    '                N = ((X + 500) * 10 - (Y) ^ 2 / 4) / 40
                                    '                O = M + N / 2
                                    X = X / 20
                                    Y = Y / 1.5
                                    O = 1 / ((X + 0.001)) ^ 2 + Y
                                    Z = Fasis(O, 20, (System.Drawing.Color.White), (System.Drawing.Color.Blue), Dvojn�hoda((System.Drawing.Color.Cyan), (System.Drawing.Color.Black), 0.8), (System.Drawing.Color.Magenta), Dvojn�hoda((System.Drawing.Color.Yellow), (System.Drawing.Color.Cyan), 0.75), (System.Drawing.Color.Lime), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White), (System.Drawing.Color.White))

                                Case "Prvn� polofrakt�l"
                                    CX = (X) / 140
                                    ZX = CX
                                    CY = (Y) / 140
                                    ZY = CY
                                    For I = 0 To 255
                                        CX = CX ^ 2 * CY + ZX
                                        CY = CY ^ 2 * CX + ZY
                                        If AbsCompl(CX, CY) > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "Druh� polofrakt�l"
                                    CX = (X) / 140
                                    ZX = CX
                                    CY = (Y) / 140
                                    ZY = CY
                                    For I = 0 To 255
                                        CX = CX ^ 2 + ZX
                                        CY = CY ^ 2 + ZY
                                        If AbsCompl(CX, CY) > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "T�et� polofrakt�l"
                                    CX = (X) / 140
                                    ZX = CX
                                    CY = (Y) / 140
                                    ZY = CY
                                    For I = 0 To 255
                                        CX = CX ^ 2 + CY + ZX
                                        CY = CY ^ 2 + CX + ZY
                                        If AbsCompl(CX, CY) > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "�tvrt� polofrakt�l"
                                    CX = (X) / 140
                                    ZX = CX
                                    CY = (Y) / 140
                                    ZY = CY
                                    For I = 0 To 255
                                        CX = CX ^ 2 + CX * CY + ZX
                                        CY = CY ^ 2 + CX * CY + ZY
                                        If AbsCompl(CX, CY) > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "P�t� polofrakt�l"
                                    CX = (X) / 140
                                    ZX = CX
                                    CY = (Y) / 140
                                    ZY = CY
                                    For I = 0 To 255
                                        CX = CX ^ 3 + CX * CY + ZX
                                        CY = CY ^ 3 + CX * CY + ZY
                                        If AbsCompl(CX, CY) > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "Mandelbrot"
                                    Cc.Init((X - 100) / 250, Y / 250)
                                    Zc.Init((Cc.Real), (Cc.Imaginary))
                                    For I = 0 To 260
                                        Cc.Multiply(Cc)
                                        Cc.Add(Zc)
                                        If Cc.Absol > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "Mandelbrot od��tan�"
                                    Cc.Init((X - 100) / 250, Y / 250)
                                    Zc.Init((Cc.Real), (Cc.Imaginary))
                                    For I = 0 To 260
                                        Cc.Multiply(Cc)
                                        Cc.Subtract(Zc)
                                        If Cc.Absol > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "Julia"
                                    Cc.Init(X / 250, Y / 250)
                                    Zc.Init(-1, -0.434)
                                    For I = 0 To 260
                                        Cc.Multiply(Cc)
                                        Cc.Add(Zc)
                                        If Cc.Absol > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "Julia od��tan�"
                                    Cc.Init(X / 180, Y / 180)
                                    Zc.Init(-1, -0.434)
                                    For I = 0 To 260
                                        Cc.Multiply(Cc)
                                        Cc.Subtract(Zc)
                                        If Cc.Absol > 2 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case "Julia kubick�"
                                    Cc.Init(X / 200, Y / 200)
                                    Zc.Init(-1, -0.434)
                                    For I = 0 To 260
                                        Cc.Multiply(Cc.Multiply(Cc))
                                        Cc.Add(Zc)
                                        If Cc.Absol > 3 Then Exit For
                                    Next
                                    Z = Fasis(I, 5, (System.Drawing.Color.White), (System.Drawing.Color.Lime), (System.Drawing.Color.Blue), (System.Drawing.Color.Magenta))
                                Case Else
                                    Z = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White)
                            End Select
                            'obrFr.PSet (�X, �Y), Z
                            On Error Resume Next
                            'Dim Pero As New System.Drawing.Pen(System.Drawing.ColorTranslator.FromOle(Z))
                            'Gr.DrawLine(Pero, �X, �Y, �X + 1, �Y + 1)
                            .Pixel(�X, �Y) = System.Drawing.ColorTranslator.FromOle(Z)
                            On Error GoTo 0
                        Next
                        .Refresh()
                        System.Windows.Forms.Application.DoEvents()
                    Next
            End Select
            Dim PeroR�m As Pen = Pens.Black
            If frmV�b�r.chbOkraj.Checked Then
                Dim Gr As Graphics = Graphics.FromImage(.Z�sobn�k)
                Gr.DrawRectangle(PeroR�m, 0, 0, .ClientRectangle.Width - 1, .ClientRectangle.Height - 1)
                .Refresh()
            End If
            .Cursor = System.Windows.Forms.Cursors.Default
        End With
        'Beep()
        MsgBox("Hotovo")
    End Sub

    Function AbsCompl(ByRef X As Object, ByRef Y As Object) As Object
        AbsCompl = (X ^ 2 + Y ^ 2) ^ (1 / 2)
    End Function

    Private Sub PD_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PD.PrintPage
        Dim RX As Integer = e.Graphics.DpiX
        Dim RY As Integer = e.Graphics.DpiY
        'Printer.Line((0, 0) - (18, 13), 0, B)
        e.Graphics.DrawRectangle(Pens.Black, 0, 0, cmNaPx(18, RX), cmNaPx(13, RY))
        'Printer.Line((1.4, 1.4) - (18 - 1.4, 13 - 1.4), (System.Drawing.Color.Blue), B)
        e.Graphics.DrawRectangle(Pens.Blue, cmNaPx(1.4, RX), cmNaPx(1.4, RY), cmNaPx(18 - 1.4, RX), cmNaPx(13 - 1.4, RY))
        'Printer.PaintPicture(obrTisk.Image, 1.5, 1.5, 15, 10)
        e.Graphics.DrawImage(obrTisk.Image, cmNaPx(1.5, RX), cmNaPx(1.5, RY), cmNaPx(15, RX), cmNaPx(10, RY))
        'End If
        'Printer.EndDoc()
    End Sub

    Private Function cmNaPx(ByVal cm As Single, ByVal Rozli�en� As Integer) As Single
        ' Nap�ed ud�l�me z centimetr� palce (p�es body - points)
        Dim palc� As Double = cm * 2.54
        Return palc� * Rozli�en�
    End Function

    Private Function cmNaPxDbl(ByVal cm As Double, ByVal Rozli�en� As Integer) As Double
        ' Nap�ed ud�l�me z centimetr� palce (p�es body - points)
        Dim palc� As Double = cm * 2.54
        Return palc� * Rozli�en�
    End Function

End Class