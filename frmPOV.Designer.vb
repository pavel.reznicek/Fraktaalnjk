<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmPOV
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents tlStop As System.Windows.Forms.Button
	Public WithEvents p�epBarvyObr�zku As P�ep�na�
	Public WithEvents PB As System.Windows.Forms.ProgressBar
	Public WithEvents �ouPr�hl As �oup�
	Public WithEvents _optGraf_3 As System.Windows.Forms.RadioButton
	Public WithEvents _optGraf_2 As System.Windows.Forms.RadioButton
	Public WithEvents _optGraf_1 As System.Windows.Forms.RadioButton
	Public WithEvents _optGraf_0 As System.Windows.Forms.RadioButton
	Public WithEvents Frame1 As System.Windows.Forms.GroupBox
	Public WithEvents _optZ_1 As System.Windows.Forms.RadioButton
	Public WithEvents _optZ_0 As System.Windows.Forms.RadioButton
	Public WithEvents �ouZ As �oup�
	Public WithEvents fraZ As System.Windows.Forms.GroupBox
	Public WithEvents tlZav��t As System.Windows.Forms.Button
	Public WithEvents tlPOV As System.Windows.Forms.Button
	Public WithEvents p�epVynechatNulHod As P�ep�na�
	Public WithEvents optGraf As Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray
	Public WithEvents optZ As Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmPOV))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.tlStop = New System.Windows.Forms.Button
		Me.p�epBarvyObr�zku = New P�ep�na�
		Me.PB = New System.Windows.Forms.ProgressBar
		Me.�ouPr�hl = New �oup�
		Me.Frame1 = New System.Windows.Forms.GroupBox
		Me._optGraf_3 = New System.Windows.Forms.RadioButton
		Me._optGraf_2 = New System.Windows.Forms.RadioButton
		Me._optGraf_1 = New System.Windows.Forms.RadioButton
		Me._optGraf_0 = New System.Windows.Forms.RadioButton
		Me.fraZ = New System.Windows.Forms.GroupBox
		Me._optZ_1 = New System.Windows.Forms.RadioButton
		Me._optZ_0 = New System.Windows.Forms.RadioButton
		Me.�ouZ = New �oup�
		Me.tlZav��t = New System.Windows.Forms.Button
		Me.tlPOV = New System.Windows.Forms.Button
		Me.p�epVynechatNulHod = New P�ep�na�
		Me.optGraf = New Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray(components)
		Me.optZ = New Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray(components)
		Me.Frame1.SuspendLayout()
		Me.fraZ.SuspendLayout()
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.optGraf, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.optZ, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
		Me.Text = "POV export"
		Me.ClientSize = New System.Drawing.Size(233, 253)
		Me.Location = New System.Drawing.Point(3, 22)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.ShowInTaskbar = False
		Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmPOV"
		Me.tlStop.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.tlStop.Text = "Stop"
		Me.tlStop.Size = New System.Drawing.Size(65, 25)
		Me.tlStop.Location = New System.Drawing.Point(160, 168)
		Me.tlStop.TabIndex = 14
		Me.tlStop.BackColor = System.Drawing.SystemColors.Control
		Me.tlStop.CausesValidation = True
		Me.tlStop.Enabled = True
		Me.tlStop.ForeColor = System.Drawing.SystemColors.ControlText
		Me.tlStop.Cursor = System.Windows.Forms.Cursors.Default
		Me.tlStop.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.tlStop.TabStop = True
		Me.tlStop.Name = "tlStop"
		Me.p�epBarvyObr�zku.Size = New System.Drawing.Size(145, 17)
		Me.p�epBarvyObr�zku.Location = New System.Drawing.Point(8, 104)
		Me.p�epBarvyObr�zku.TabIndex = 12
        Me.p�epBarvyObr�zku.Hodnota = 1
		Me.p�epBarvyObr�zku.Name = "p�epBarvyObr�zku"
		Me.PB.Size = New System.Drawing.Size(217, 17)
		Me.PB.Location = New System.Drawing.Point(8, 232)
		Me.PB.TabIndex = 11
		Me.PB.Name = "PB"
		Me.�ouPr�hl.Size = New System.Drawing.Size(145, 19)
		Me.�ouPr�hl.Location = New System.Drawing.Point(8, 144)
		Me.�ouPr�hl.TabIndex = 10
        Me.�ouPr�hl.Min = 0
		Me.�ouPr�hl.���kaTxt = 36
		Me.�ouPr�hl.N�pis = "Pr�hlednost (%):"
		Me.�ouPr�hl.N�pov�da = ""
		Me.�ouPr�hl.Name = "�ouPr�hl"
		Me.Frame1.Text = "Graf"
		Me.Frame1.Size = New System.Drawing.Size(145, 89)
		Me.Frame1.Location = New System.Drawing.Point(8, 8)
		Me.Frame1.TabIndex = 6
		Me.Frame1.BackColor = System.Drawing.SystemColors.Control
		Me.Frame1.Enabled = True
		Me.Frame1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Frame1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Frame1.Visible = True
		Me.Frame1.Name = "Frame1"
		Me._optGraf_3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_3.Text = "Kuli�ky"
		Me._optGraf_3.Size = New System.Drawing.Size(113, 17)
		Me._optGraf_3.Location = New System.Drawing.Point(8, 64)
		Me._optGraf_3.TabIndex = 13
		Me._optGraf_3.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_3.BackColor = System.Drawing.SystemColors.Control
		Me._optGraf_3.CausesValidation = True
		Me._optGraf_3.Enabled = True
		Me._optGraf_3.ForeColor = System.Drawing.SystemColors.ControlText
		Me._optGraf_3.Cursor = System.Windows.Forms.Cursors.Default
		Me._optGraf_3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._optGraf_3.Appearance = System.Windows.Forms.Appearance.Normal
		Me._optGraf_3.TabStop = True
		Me._optGraf_3.Checked = False
		Me._optGraf_3.Visible = True
		Me._optGraf_3.Name = "_optGraf_3"
		Me._optGraf_2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_2.Text = "Sloupce"
		Me._optGraf_2.Size = New System.Drawing.Size(121, 17)
		Me._optGraf_2.Location = New System.Drawing.Point(8, 48)
		Me._optGraf_2.TabIndex = 9
		Me._optGraf_2.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_2.BackColor = System.Drawing.SystemColors.Control
		Me._optGraf_2.CausesValidation = True
		Me._optGraf_2.Enabled = True
		Me._optGraf_2.ForeColor = System.Drawing.SystemColors.ControlText
		Me._optGraf_2.Cursor = System.Windows.Forms.Cursors.Default
		Me._optGraf_2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._optGraf_2.Appearance = System.Windows.Forms.Appearance.Normal
		Me._optGraf_2.TabStop = True
		Me._optGraf_2.Checked = False
		Me._optGraf_2.Visible = True
		Me._optGraf_2.Name = "_optGraf_2"
		Me._optGraf_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_1.Text = "Krychli�ky"
		Me._optGraf_1.Size = New System.Drawing.Size(121, 17)
		Me._optGraf_1.Location = New System.Drawing.Point(8, 32)
		Me._optGraf_1.TabIndex = 8
		Me._optGraf_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_1.BackColor = System.Drawing.SystemColors.Control
		Me._optGraf_1.CausesValidation = True
		Me._optGraf_1.Enabled = True
		Me._optGraf_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._optGraf_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._optGraf_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._optGraf_1.Appearance = System.Windows.Forms.Appearance.Normal
		Me._optGraf_1.TabStop = True
		Me._optGraf_1.Checked = False
		Me._optGraf_1.Visible = True
		Me._optGraf_1.Name = "_optGraf_1"
		Me._optGraf_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_0.Text = "Spojen� troj�heln�ky"
		Me._optGraf_0.Size = New System.Drawing.Size(121, 17)
		Me._optGraf_0.Location = New System.Drawing.Point(8, 16)
		Me._optGraf_0.TabIndex = 7
		Me._optGraf_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optGraf_0.BackColor = System.Drawing.SystemColors.Control
		Me._optGraf_0.CausesValidation = True
		Me._optGraf_0.Enabled = True
		Me._optGraf_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._optGraf_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._optGraf_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._optGraf_0.Appearance = System.Windows.Forms.Appearance.Normal
		Me._optGraf_0.TabStop = True
		Me._optGraf_0.Checked = False
		Me._optGraf_0.Visible = True
		Me._optGraf_0.Name = "_optGraf_0"
		Me.fraZ.Text = "Osa Z"
		Me.fraZ.Size = New System.Drawing.Size(145, 57)
		Me.fraZ.Location = New System.Drawing.Point(8, 168)
		Me.fraZ.TabIndex = 2
		Me.fraZ.BackColor = System.Drawing.SystemColors.Control
		Me.fraZ.Enabled = True
		Me.fraZ.ForeColor = System.Drawing.SystemColors.ControlText
		Me.fraZ.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.fraZ.Visible = True
		Me.fraZ.Name = "fraZ"
		Me._optZ_1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optZ_1.Text = "Prota�en�"
		Me._optZ_1.Size = New System.Drawing.Size(65, 17)
		Me._optZ_1.Location = New System.Drawing.Point(8, 32)
		Me._optZ_1.TabIndex = 5
		Me._optZ_1.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optZ_1.BackColor = System.Drawing.SystemColors.Control
		Me._optZ_1.CausesValidation = True
		Me._optZ_1.Enabled = True
		Me._optZ_1.ForeColor = System.Drawing.SystemColors.ControlText
		Me._optZ_1.Cursor = System.Windows.Forms.Cursors.Default
		Me._optZ_1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._optZ_1.Appearance = System.Windows.Forms.Appearance.Normal
		Me._optZ_1.TabStop = True
		Me._optZ_1.Checked = False
		Me._optZ_1.Visible = True
		Me._optZ_1.Name = "_optZ_1"
		Me._optZ_0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optZ_0.Text = "Zplo�t�n�"
		Me._optZ_0.Size = New System.Drawing.Size(65, 17)
		Me._optZ_0.Location = New System.Drawing.Point(8, 16)
		Me._optZ_0.TabIndex = 4
		Me._optZ_0.Checked = True
		Me._optZ_0.CheckAlign = System.Drawing.ContentAlignment.MiddleLeft
		Me._optZ_0.BackColor = System.Drawing.SystemColors.Control
		Me._optZ_0.CausesValidation = True
		Me._optZ_0.Enabled = True
		Me._optZ_0.ForeColor = System.Drawing.SystemColors.ControlText
		Me._optZ_0.Cursor = System.Windows.Forms.Cursors.Default
		Me._optZ_0.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me._optZ_0.Appearance = System.Windows.Forms.Appearance.Normal
		Me._optZ_0.TabStop = True
		Me._optZ_0.Visible = True
		Me._optZ_0.Name = "_optZ_0"
		Me.�ouZ.Size = New System.Drawing.Size(57, 19)
		Me.�ouZ.Location = New System.Drawing.Point(72, 24)
		Me.�ouZ.TabIndex = 3
		Me.�ouZ.���kaTxt = 36
		Me.�ouZ.N�pis = ""
		Me.�ouZ.N�pov�da = ""
		Me.�ouZ.Name = "�ouZ"
		Me.tlZav��t.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.CancelButton = Me.tlZav��t
		Me.tlZav��t.Text = "Zav��t"
		Me.tlZav��t.Size = New System.Drawing.Size(65, 25)
		Me.tlZav��t.Location = New System.Drawing.Point(160, 200)
		Me.tlZav��t.TabIndex = 1
		Me.tlZav��t.BackColor = System.Drawing.SystemColors.Control
		Me.tlZav��t.CausesValidation = True
		Me.tlZav��t.Enabled = True
		Me.tlZav��t.ForeColor = System.Drawing.SystemColors.ControlText
		Me.tlZav��t.Cursor = System.Windows.Forms.Cursors.Default
		Me.tlZav��t.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.tlZav��t.TabStop = True
		Me.tlZav��t.Name = "tlZav��t"
		Me.tlPOV.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.tlPOV.Text = "POV!"
		Me.tlPOV.Size = New System.Drawing.Size(65, 25)
		Me.tlPOV.Location = New System.Drawing.Point(160, 136)
		Me.tlPOV.TabIndex = 0
		Me.tlPOV.BackColor = System.Drawing.SystemColors.Control
		Me.tlPOV.CausesValidation = True
		Me.tlPOV.Enabled = True
		Me.tlPOV.ForeColor = System.Drawing.SystemColors.ControlText
		Me.tlPOV.Cursor = System.Windows.Forms.Cursors.Default
		Me.tlPOV.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.tlPOV.TabStop = True
		Me.tlPOV.Name = "tlPOV"
		Me.p�epVynechatNulHod.Size = New System.Drawing.Size(145, 17)
		Me.p�epVynechatNulHod.Location = New System.Drawing.Point(8, 120)
		Me.p�epVynechatNulHod.TabIndex = 15
        Me.p�epVynechatNulHod.Hodnota = 1
		Me.p�epVynechatNulHod.Name = "p�epVynechatNulHod"
		Me.Controls.Add(tlStop)
		Me.Controls.Add(p�epBarvyObr�zku)
		Me.Controls.Add(PB)
		Me.Controls.Add(�ouPr�hl)
		Me.Controls.Add(Frame1)
		Me.Controls.Add(fraZ)
		Me.Controls.Add(tlZav��t)
		Me.Controls.Add(tlPOV)
		Me.Controls.Add(p�epVynechatNulHod)
		Me.Frame1.Controls.Add(_optGraf_3)
		Me.Frame1.Controls.Add(_optGraf_2)
		Me.Frame1.Controls.Add(_optGraf_1)
		Me.Frame1.Controls.Add(_optGraf_0)
		Me.fraZ.Controls.Add(_optZ_1)
		Me.fraZ.Controls.Add(_optZ_0)
		Me.fraZ.Controls.Add(�ouZ)
		Me.optGraf.SetIndex(_optGraf_3, CType(3, Short))
		Me.optGraf.SetIndex(_optGraf_2, CType(2, Short))
		Me.optGraf.SetIndex(_optGraf_1, CType(1, Short))
		Me.optGraf.SetIndex(_optGraf_0, CType(0, Short))
		Me.optZ.SetIndex(_optZ_1, CType(1, Short))
		Me.optZ.SetIndex(_optZ_0, CType(0, Short))
		CType(Me.optZ, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.optGraf, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Frame1.ResumeLayout(False)
		Me.fraZ.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class