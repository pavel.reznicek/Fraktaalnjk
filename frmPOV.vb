Option Strict Off
Option Explicit On
Friend Class frmPOV
	Inherits System.Windows.Forms.Form
	
	Dim Zastav As Boolean
	
	Private Sub frmPOV_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		optGraf(0).Checked = True
		�ouZ.Max = MaxInt
	End Sub
	
	Private Sub frmPOV_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
			Cancel = 1
			Me.Hide()
		End If
		eventArgs.Cancel = Cancel
	End Sub
	
	Private Sub tlPOV_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlPOV.Click
		Zastav = False
		PB.Value = 0
		On Error GoTo chyba
		PB.Maximum = UBound(Mapa, 2)
		On Error GoTo 0
		FileOpen(1, My.Application.Info.DirectoryPath & "\POV\graf.inc", OpenMode.Output)
		Dim Y, X, I As Integer
		Dim Nejv As Integer
        Dim NejnZ As Double
        Dim Polov��ka As Double
		'UPGRADE_WARNING: Couldn't resolve default property of object Polov��ka. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		Polov��ka = UBound(Mapa, 2) / 2
		'UPGRADE_WARNING: Couldn't resolve default property of object Polov��ka. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		PrintLine(1, "#declare Polovyska = " & �et(Polov��ka) & ";")
		If optGraf(0).Checked Then
			PrintLine(1, "#declare Graf = mesh{")
		Else
			PrintLine(1, "#declare Graf = union{")
			'Print #1, ""
		End If
		If optGraf(2).Checked Then
			For Y = 0 To UBound(Mapa, 2)
				For X = 0 To UBound(Mapa, 1)
					'UPGRADE_WARNING: Couldn't resolve default property of object NejnZ. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Mapa(X, Y).Hodnota < NejnZ Then NejnZ = Mapa(X, Y).Hodnota
				Next 
				System.Windows.Forms.Application.DoEvents()
				PB.Value = Y
			Next 
		End If
		Dim NulV��ka As Boolean
		Dim Sou�Z2, Sou�M1, Sou��1, Bar1, Bar2, Sou�Z1, Sou��2, Sou�M2 As Integer
		Dim Bod1, Bod2 As XYZ
		'UPGRADE_WARNING: Lower bound of array Tr2 was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Tr2(3) As XYZ
		'UPGRADE_WARNING: Lower bound of array Tr1 was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Tr1(3) As XYZ
		'UPGRADE_WARNING: Lower bound of array Body was changed from 1 to 0. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="0F1C9BE1-AF9D-476E-83B1-17D43BECFF20"'
		Dim Body(4) As XYZ
		For Y = 0 To UBound(Mapa, 2)
			For X = 0 To UBound(Mapa, 1)
				If optGraf(0).Checked Then
					Body(1).X = X
					Body(1).Y = Y
					For I = 2 To 4
						Body(I).X = Body(1).X + Sm�r(I).X
						Body(I).Y = Body(1).Y + Sm�r(I).Y
					Next 
					NulV��ka = True
					For I = 1 To 4
						'UPGRADE_WARNING: Couldn't resolve default property of object Body().Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        Body(I).Z = Mapa(X, Y).Hodnota * Prota�en�
						'UPGRADE_WARNING: Couldn't resolve default property of object Body().Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
						NulV��ka = NulV��ka And Body(I).Z = 0
					Next 
                    If Not (NulV��ka And p�epVynechatNulHod.Hodnota) Then
                        Nejv = 1
                        For I = 2 To 4
                            'UPGRADE_WARNING: Couldn't resolve default property of object Body(I - 1).Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            'UPGRADE_WARNING: Couldn't resolve default property of object Body(I).Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If Body(I).Z > Body(I - 1).Z Then Nejv = I
                        Next
                        Select Case Nejv
                            Case 1
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(1) = Body(1)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(2) = Body(3)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(3) = Body(2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(1) = Body(1)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(2) = Body(3)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(3) = Body(4)
                            Case 2
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(1) = Body(2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(2) = Body(4)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(3) = Body(3)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(1) = Body(2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(2) = Body(4)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(3) = Body(1)
                            Case 3
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(1) = Body(3)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(2) = Body(1)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(3) = Body(2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(1) = Body(3)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(2) = Body(1)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(3) = Body(4)
                            Case 4
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(1) = Body(4)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(2) = Body(2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr1(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr1(3) = Body(1)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(1). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(1) = Body(4)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(2). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(2) = Body(2)
                                'UPGRADE_WARNING: Couldn't resolve default property of object Tr2(3). Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                                Tr2(3) = Body(3)
                        End Select
                        Sou��1 = 0
                        Sou�Z1 = 0
                        Sou�M1 = 0
                        Sou��2 = 0
                        Sou�Z2 = 0
                        Sou�M2 = 0

                        For I = 1 To 3
                            Sou��1 = Sou��1 + �erven�(BarBodu(Tr1(I)))
                            Sou�Z1 = Sou�Z1 + Zelen�(BarBodu(Tr1(I)))
                            Sou�M1 = Sou�M1 + Modr�(BarBodu(Tr1(I)))
                            Sou��2 = Sou��2 + �erven�(BarBodu(Tr2(I)))
                            Sou�Z2 = Sou�Z2 + Zelen�(BarBodu(Tr2(I)))
                            Sou�M2 = Sou�M2 + Modr�(BarBodu(Tr2(I)))
                        Next
                        Bar1 = RGB(Sou��1 / 3, Sou�Z1 / 3, Sou�M1 / 3)
                        Bar2 = RGB(Sou��2 / 3, Sou�Z2 / 3, Sou�M2 / 3)
                        PrintLine(1, "#declare Povrch=texture{pigment{" & POVBarva(Bar2) & POVFiltr & "}}")
                        PrintLine(1, "triangle{" & POVBod(Tr1(1)) & "," & POVBod(Tr1(2)) & "," & POVBod(Tr1(3)) & "texture{Povrch}}")
                        PrintLine(1, "#declare Povrch=texture{pigment{" & POVBarva(Bar2) & POVFiltr & "}}")
                        PrintLine(1, "triangle{" & POVBod(Tr2(1)) & "," & POVBod(Tr2(2)) & "," & POVBod(Tr2(3)) & "texture{Povrch}}")
                    End If
				Else
					Bod1.X = X
					Bod1.Y = Y
					'UPGRADE_WARNING: Couldn't resolve default property of object Bod1.Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    Bod1.Z = Mapa(X, Y).Hodnota * Prota�en�
					'UPGRADE_WARNING: Couldn't resolve default property of object Bod1.Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    If Not (Bod1.Z = 0 And p�epVynechatNulHod.Hodnota) Then
                        If optGraf(1).Checked Then ' krychli�ky
                            Bod2.X = X + 1
                            Bod2.Y = Y + 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Bod2.Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Bod2.Z = (Mapa(X, Y).Hodnota + 1) * Prota�en�
                            'UPGRADE_WARNING: Couldn't resolve default property of object p�epBarvyObr�zku. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If CBool(p�epBarvyObr�zku.Hodnota) Then PrintLine(1, "#declare Povrch=texture{pigment{" & POVBarva(BarBodu(Bod1)) & POVFiltr & "}}")
                            'UPGRADE_WARNING: Couldn't resolve default property of object p�epBarvyObr�zku. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            PrintLine(1, "box{" & POVBod(Bod1) & "," & POVBod(Bod2) & IIf(CBool(p�epBarvyObr�zku.Hodnota), "texture{Povrch}", "") & "}")
                        ElseIf optGraf(2).Checked Then
                            Bod2.X = X + 1
                            Bod2.Y = Y + 1
                            'UPGRADE_WARNING: Couldn't resolve default property of object Bod2.Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            Bod2.Z = (NejnZ) * Prota�en�
                            'UPGRADE_WARNING: Couldn't resolve default property of object p�epBarvyObr�zku. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If CBool(p�epBarvyObr�zku.Hodnota) Then PrintLine(1, "#declare Povrch=texture{pigment{" & POVBarva(BarBodu(Bod1)) & POVFiltr & "}}")
                            'UPGRADE_WARNING: Couldn't resolve default property of object p�epBarvyObr�zku. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            PrintLine(1, "box{" & POVBod(Bod1) & "," & POVBod(Bod2) & IIf(CBool(p�epBarvyObr�zku.Hodnota), "texture{Povrch}", "") & "}")
                        ElseIf optGraf(3).Checked Then
                            'UPGRADE_WARNING: Couldn't resolve default property of object p�epBarvyObr�zku. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                            If CBool(p�epBarvyObr�zku.Hodnota) Then PrintLine(1, "#declare Povrch=texture{pigment{" & POVBarva(BarBodu(Bod1)) & POVFiltr & "}}")
                            'UPGRADE_WARNING: Couldn't resolve default property of object p�epBarvyObr�zku. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                        End If
                    End If
				End If
			Next 
			System.Windows.Forms.Application.DoEvents()
			PB.Value = Y
			If Zastav Then Exit For
		Next 
		PrintLine(1, "}")
		FileClose()
		PB.Value = 0
        'Beep()
        MsgBox("Hotovo")
		Exit Sub
chyba: 
		MsgBox("Chyba, mapa bod� neexistuje!", MsgBoxStyle.Critical)
	End Sub
	
	ReadOnly Property �et(ByVal Num As Object) As String
		Get
			'UPGRADE_WARNING: Couldn't resolve default property of object Num. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			�et = Trim(Str(Num))
		End Get
	End Property
	
	ReadOnly Property POVBarva(ByVal Bar As Integer) As String
		Get
			POVBarva = "color rgb <" & �et(�erven�(Bar) / 255) & "," & �et(Zelen�(Bar) / 255) & "," & �et(Modr�(Bar) / 255) & ">"
		End Get
	End Property
	
	Private ReadOnly Property POVBod(ByVal Bod As XYZ) As String
		Get
			'UPGRADE_WARNING: Couldn't resolve default property of object Bod.Z. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			POVBod = "<" & �et(Bod.X - UBound(Mapa, 1) / 2) & "," & �et(Bod.Y - UBound(Mapa, 2) / 2) & "," & �et(-Bod.Z) & ">"
		End Get
	End Property
	
	Private ReadOnly Property BarBodu(ByVal Bod As XYZ) As Integer
		Get
			'UPGRADE_ISSUE: PictureBox method obrFr.Point was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
            'BarBodu = frmFrakt�ln�k.obrFr.Point(Bod.X, Bod.Y)
		End Get
	End Property
	
	ReadOnly Property POVFiltr() As String
		Get
			Dim Pom�r As Object
			'UPGRADE_WARNING: Couldn't resolve default property of object �ouPr�hl. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object Pom�r. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Pom�r = CDbl(�ouPr�hl.Hodnota) / 100
			'UPGRADE_WARNING: Couldn't resolve default property of object Pom�r. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			If Not Pom�r = 0 Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Pom�r. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				POVFiltr = "filter " & �et(Pom�r)
            Else
                Return ""
            End If
		End Get
	End Property
	
	ReadOnly Property Prota�en�() As Object
		Get
			'UPGRADE_WARNING: Couldn't resolve default property of object �ouZ. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			'UPGRADE_WARNING: Couldn't resolve default property of object Prota�en�. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            Prota�en� = IIf(optZ(0).Checked, 1 / CDbl(�ouZ.Hodnota), �ouZ.Hodnota)
		End Get
	End Property
	
	Private Sub tlStop_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlStop.Click
		Zastav = True
	End Sub
	
	Private Sub tlZav��t_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlZav��t.Click
		Me.Hide()
	End Sub
End Class