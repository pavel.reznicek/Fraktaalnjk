<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmTisk
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents tlPry� As System.Windows.Forms.Button
	Public WithEvents tlOK As System.Windows.Forms.Button
	Public WithEvents _optForm�t_2 As System.Windows.Forms.RadioButton
	Public WithEvents p�epR�me�ek As P�ep�na�
	Public CDColor As System.Windows.Forms.ColorDialog
	Public WithEvents tlBarva As System.Windows.Forms.Button
	Public WithEvents _optPolovina_1 As System.Windows.Forms.RadioButton
	Public WithEvents _optPolovina_0 As System.Windows.Forms.RadioButton
	Public WithEvents fraPolovina As System.Windows.Forms.GroupBox
	Public WithEvents _optForm�t_0 As System.Windows.Forms.RadioButton
	Public WithEvents _optForm�t_1 As System.Windows.Forms.RadioButton
	Public WithEvents fraRozm�ry As System.Windows.Forms.GroupBox
	Public WithEvents optForm�t As Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray
	Public WithEvents optPolovina As Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmTisk))
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.tlBarva = New System.Windows.Forms.Button()
        Me.tlPry� = New System.Windows.Forms.Button()
        Me.tlOK = New System.Windows.Forms.Button()
        Me.fraRozm�ry = New System.Windows.Forms.GroupBox()
        Me._optForm�t_2 = New System.Windows.Forms.RadioButton()
        Me.p�epR�me�ek = New Frakt�ln�k.P�ep�na�()
        Me.fraPolovina = New System.Windows.Forms.GroupBox()
        Me._optPolovina_1 = New System.Windows.Forms.RadioButton()
        Me._optPolovina_0 = New System.Windows.Forms.RadioButton()
        Me._optForm�t_0 = New System.Windows.Forms.RadioButton()
        Me._optForm�t_1 = New System.Windows.Forms.RadioButton()
        Me.CDColor = New System.Windows.Forms.ColorDialog()
        Me.optForm�t = New Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray(Me.components)
        Me.optPolovina = New Microsoft.VisualBasic.Compatibility.VB6.RadioButtonArray(Me.components)
        Me._optForm�t_3 = New System.Windows.Forms.RadioButton()
        Me.gbRozm�ry = New System.Windows.Forms.GroupBox()
        Me.�ou���ka = New Frakt�ln�k.�oup�()
        Me.�ouV��ka = New Frakt�ln�k.�oup�()
        Me.�ouOkraj = New Frakt�ln�k.�oup�()
        Me.fraRozm�ry.SuspendLayout()
        Me.fraPolovina.SuspendLayout()
        CType(Me.optForm�t, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.optPolovina, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbRozm�ry.SuspendLayout()
        Me.SuspendLayout()
        '
        'tlBarva
        '
        Me.tlBarva.BackColor = System.Drawing.Color.Blue
        Me.tlBarva.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlBarva.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlBarva.Location = New System.Drawing.Point(112, 89)
        Me.tlBarva.Name = "tlBarva"
        Me.tlBarva.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlBarva.Size = New System.Drawing.Size(89, 17)
        Me.tlBarva.TabIndex = 8
        Me.ToolTip1.SetToolTip(Me.tlBarva, "Barva r�me�ku")
        Me.tlBarva.UseVisualStyleBackColor = False
        '
        'tlPry�
        '
        Me.tlPry�.BackColor = System.Drawing.SystemColors.Control
        Me.tlPry�.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlPry�.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.tlPry�.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlPry�.Location = New System.Drawing.Point(144, 272)
        Me.tlPry�.Name = "tlPry�"
        Me.tlPry�.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlPry�.Size = New System.Drawing.Size(81, 25)
        Me.tlPry�.TabIndex = 4
        Me.tlPry�.Text = "&Zav��t"
        Me.tlPry�.UseVisualStyleBackColor = False
        '
        'tlOK
        '
        Me.tlOK.BackColor = System.Drawing.SystemColors.Control
        Me.tlOK.Cursor = System.Windows.Forms.Cursors.Default
        Me.tlOK.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tlOK.Location = New System.Drawing.Point(8, 272)
        Me.tlOK.Name = "tlOK"
        Me.tlOK.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.tlOK.Size = New System.Drawing.Size(81, 25)
        Me.tlOK.TabIndex = 3
        Me.tlOK.Text = "&OK"
        Me.tlOK.UseVisualStyleBackColor = False
        '
        'fraRozm�ry
        '
        Me.fraRozm�ry.BackColor = System.Drawing.SystemColors.Control
        Me.fraRozm�ry.Controls.Add(Me.gbRozm�ry)
        Me.fraRozm�ry.Controls.Add(Me._optForm�t_3)
        Me.fraRozm�ry.Controls.Add(Me._optForm�t_2)
        Me.fraRozm�ry.Controls.Add(Me.p�epR�me�ek)
        Me.fraRozm�ry.Controls.Add(Me.tlBarva)
        Me.fraRozm�ry.Controls.Add(Me.fraPolovina)
        Me.fraRozm�ry.Controls.Add(Me._optForm�t_0)
        Me.fraRozm�ry.Controls.Add(Me._optForm�t_1)
        Me.fraRozm�ry.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraRozm�ry.Location = New System.Drawing.Point(8, 0)
        Me.fraRozm�ry.Name = "fraRozm�ry"
        Me.fraRozm�ry.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraRozm�ry.Size = New System.Drawing.Size(217, 266)
        Me.fraRozm�ry.TabIndex = 0
        Me.fraRozm�ry.TabStop = False
        Me.fraRozm�ry.Text = "Form�t"
        '
        '_optForm�t_2
        '
        Me._optForm�t_2.BackColor = System.Drawing.SystemColors.Control
        Me._optForm�t_2.Cursor = System.Windows.Forms.Cursors.Default
        Me._optForm�t_2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optForm�t.SetIndex(Me._optForm�t_2, CType(2, Short))
        Me._optForm�t_2.Location = New System.Drawing.Point(8, 112)
        Me._optForm�t_2.Name = "_optForm�t_2"
        Me._optForm�t_2.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optForm�t_2.Size = New System.Drawing.Size(161, 17)
        Me._optForm�t_2.TabIndex = 10
        Me._optForm�t_2.TabStop = True
        Me._optForm�t_2.Text = "15 x &20 (velk�, bez r�mu)"
        Me._optForm�t_2.UseVisualStyleBackColor = False
        '
        'p�epR�me�ek
        '
        Me.p�epR�me�ek.Caption = "&R�me�ek"
        Me.p�epR�me�ek.Hodnota = False
        Me.p�epR�me�ek.Location = New System.Drawing.Point(112, 72)
        Me.p�epR�me�ek.Name = "p�epR�me�ek"
        Me.p�epR�me�ek.Size = New System.Drawing.Size(97, 17)
        Me.p�epR�me�ek.TabIndex = 9
        Me.p�epR�me�ek.ToolTipText = ""
        '
        'fraPolovina
        '
        Me.fraPolovina.BackColor = System.Drawing.SystemColors.Control
        Me.fraPolovina.Controls.Add(Me._optPolovina_1)
        Me.fraPolovina.Controls.Add(Me._optPolovina_0)
        Me.fraPolovina.ForeColor = System.Drawing.SystemColors.ControlText
        Me.fraPolovina.Location = New System.Drawing.Point(104, 8)
        Me.fraPolovina.Name = "fraPolovina"
        Me.fraPolovina.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.fraPolovina.Size = New System.Drawing.Size(105, 57)
        Me.fraPolovina.TabIndex = 5
        Me.fraPolovina.TabStop = False
        Me.fraPolovina.Text = "Polovina"
        '
        '_optPolovina_1
        '
        Me._optPolovina_1.BackColor = System.Drawing.SystemColors.Control
        Me._optPolovina_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._optPolovina_1.Enabled = False
        Me._optPolovina_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optPolovina.SetIndex(Me._optPolovina_1, CType(1, Short))
        Me._optPolovina_1.Location = New System.Drawing.Point(8, 32)
        Me._optPolovina_1.Name = "_optPolovina_1"
        Me._optPolovina_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optPolovina_1.Size = New System.Drawing.Size(73, 17)
        Me._optPolovina_1.TabIndex = 7
        Me._optPolovina_1.TabStop = True
        Me._optPolovina_1.Text = "V&pravo"
        Me._optPolovina_1.UseVisualStyleBackColor = False
        '
        '_optPolovina_0
        '
        Me._optPolovina_0.BackColor = System.Drawing.SystemColors.Control
        Me._optPolovina_0.Checked = True
        Me._optPolovina_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._optPolovina_0.Enabled = False
        Me._optPolovina_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optPolovina.SetIndex(Me._optPolovina_0, CType(0, Short))
        Me._optPolovina_0.Location = New System.Drawing.Point(8, 16)
        Me._optPolovina_0.Name = "_optPolovina_0"
        Me._optPolovina_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optPolovina_0.Size = New System.Drawing.Size(73, 17)
        Me._optPolovina_0.TabIndex = 6
        Me._optPolovina_0.TabStop = True
        Me._optPolovina_0.Text = "V&levo"
        Me._optPolovina_0.UseVisualStyleBackColor = False
        '
        '_optForm�t_0
        '
        Me._optForm�t_0.BackColor = System.Drawing.SystemColors.Control
        Me._optForm�t_0.Cursor = System.Windows.Forms.Cursors.Default
        Me._optForm�t_0.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optForm�t.SetIndex(Me._optForm�t_0, CType(0, Short))
        Me._optForm�t_0.Location = New System.Drawing.Point(8, 32)
        Me._optForm�t_0.Name = "_optForm�t_0"
        Me._optForm�t_0.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optForm�t_0.Size = New System.Drawing.Size(93, 17)
        Me._optForm�t_0.TabIndex = 2
        Me._optForm�t_0.TabStop = True
        Me._optForm�t_0.Text = "10 x 1&6 (star�)"
        Me._optForm�t_0.UseVisualStyleBackColor = False
        '
        '_optForm�t_1
        '
        Me._optForm�t_1.BackColor = System.Drawing.SystemColors.Control
        Me._optForm�t_1.Checked = True
        Me._optForm�t_1.Cursor = System.Windows.Forms.Cursors.Default
        Me._optForm�t_1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.optForm�t.SetIndex(Me._optForm�t_1, CType(1, Short))
        Me._optForm�t_1.Location = New System.Drawing.Point(8, 72)
        Me._optForm�t_1.Name = "_optForm�t_1"
        Me._optForm�t_1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optForm�t_1.Size = New System.Drawing.Size(93, 17)
        Me._optForm�t_1.TabIndex = 1
        Me._optForm�t_1.TabStop = True
        Me._optForm�t_1.Text = "10 x 1&5 (nov�)"
        Me._optForm�t_1.UseVisualStyleBackColor = False
        '
        'optForm�t
        '
        '
        '_optForm�t_3
        '
        Me._optForm�t_3.BackColor = System.Drawing.SystemColors.Control
        Me._optForm�t_3.Cursor = System.Windows.Forms.Cursors.Default
        Me._optForm�t_3.ForeColor = System.Drawing.SystemColors.ControlText
        Me._optForm�t_3.Location = New System.Drawing.Point(8, 135)
        Me._optForm�t_3.Name = "_optForm�t_3"
        Me._optForm�t_3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._optForm�t_3.Size = New System.Drawing.Size(161, 17)
        Me._optForm�t_3.TabIndex = 11
        Me._optForm�t_3.TabStop = True
        Me._optForm�t_3.Text = "&Vlastn�"
        Me._optForm�t_3.UseVisualStyleBackColor = False
        '
        'gbRozm�ry
        '
        Me.gbRozm�ry.Controls.Add(Me.�ouOkraj)
        Me.gbRozm�ry.Controls.Add(Me.�ouV��ka)
        Me.gbRozm�ry.Controls.Add(Me.�ou���ka)
        Me.gbRozm�ry.Location = New System.Drawing.Point(8, 158)
        Me.gbRozm�ry.Name = "gbRozm�ry"
        Me.gbRozm�ry.Size = New System.Drawing.Size(201, 100)
        Me.gbRozm�ry.TabIndex = 12
        Me.gbRozm�ry.TabStop = False
        Me.gbRozm�ry.Text = "Rozm�ry (v mm)"
        '
        '�ou���ka
        '
        Me.�ou���ka.Hodnota = 0.0R
        Me.�ou���ka.Location = New System.Drawing.Point(6, 19)
        Me.�ou���ka.Max = CType(100, Short)
        Me.�ou���ka.Min = CType(0, Short)
        Me.�ou���ka.Name = "�ou���ka"
        Me.�ou���ka.N�pis = "���&ka pap�ru"
        Me.�ou���ka.N�pov�da = ""
        Me.�ou���ka.���kaTxt = 80
        Me.�ou���ka.Size = New System.Drawing.Size(187, 20)
        Me.�ou���ka.TabIndex = 0
        '
        '�ouV��ka
        '
        Me.�ouV��ka.Hodnota = 0.0R
        Me.�ouV��ka.Location = New System.Drawing.Point(6, 45)
        Me.�ouV��ka.Max = CType(100, Short)
        Me.�ouV��ka.Min = CType(0, Short)
        Me.�ouV��ka.Name = "�ouV��ka"
        Me.�ouV��ka.N�pis = "V��k&a pap�ru"
        Me.�ouV��ka.N�pov�da = ""
        Me.�ouV��ka.���kaTxt = 80
        Me.�ouV��ka.Size = New System.Drawing.Size(187, 20)
        Me.�ouV��ka.TabIndex = 1
        '
        '�ouOkraj
        '
        Me.�ouOkraj.Hodnota = 0.0R
        Me.�ouOkraj.Location = New System.Drawing.Point(6, 71)
        Me.�ouOkraj.Max = CType(100, Short)
        Me.�ouOkraj.Min = CType(0, Short)
        Me.�ouOkraj.Name = "�ouOkraj"
        Me.�ouOkraj.N�pis = "&Okraj"
        Me.�ouOkraj.N�pov�da = ""
        Me.�ouOkraj.���kaTxt = 80
        Me.�ouOkraj.Size = New System.Drawing.Size(187, 20)
        Me.�ouOkraj.TabIndex = 2
        '
        'frmTisk
        '
        Me.AcceptButton = Me.tlOK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.CancelButton = Me.tlPry�
        Me.ClientSize = New System.Drawing.Size(233, 304)
        Me.Controls.Add(Me.tlPry�)
        Me.Controls.Add(Me.tlOK)
        Me.Controls.Add(Me.fraRozm�ry)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(3, 22)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmTisk"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Tisk"
        Me.fraRozm�ry.ResumeLayout(False)
        Me.fraPolovina.ResumeLayout(False)
        CType(Me.optForm�t, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.optPolovina, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbRozm�ry.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Public WithEvents _optForm�t_3 As System.Windows.Forms.RadioButton
    Friend WithEvents gbRozm�ry As System.Windows.Forms.GroupBox
    Friend WithEvents �ouV��ka As Frakt�ln�k.�oup�
    Friend WithEvents �ou���ka As Frakt�ln�k.�oup�
    Friend WithEvents �ouOkraj As Frakt�ln�k.�oup�
#End Region 
End Class