Option Strict Off
Option Explicit On
Friend Class frmTisk
	Inherits System.Windows.Forms.Form
	
    Public V�host As Boolean
	
    Public Enum DruhForm�tu
        fmt10x16
        fmt10x15
        fmt15x20
        fmtVlastn�
    End Enum

    Public ReadOnly Property Prav�Polovina() As Boolean
        Get
            Prav�Polovina = optPolovina(1).Checked
        End Get
    End Property

    Public ReadOnly Property Form�t() As DruhForm�tu
        Get
            Dim I As Integer
            For I = 0 To optForm�t.Count - 1
                If optForm�t(I).Checked Then
                    Form�t = I
                    Exit For
                End If
            Next
        End Get
    End Property
	
	'UPGRADE_WARNING: Form event frmTisk.Activate has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
	Private Sub frmTisk_Activated(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Activated
		'UPGRADE_WARNING: Couldn't resolve default property of object V�host. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		V�host = False
	End Sub
	
	Private Sub frmTisk_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
		Dim Cancel As Boolean = eventArgs.Cancel
		Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
		If UnloadMode = 0 Then Cancel = True
		eventArgs.Cancel = Cancel
	End Sub
	
	'UPGRADE_WARNING: Event optForm�t.CheckedChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub optForm�t_CheckedChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles optForm�t.CheckedChanged
		If eventSender.Checked Then
			Dim Index As Short = optForm�t.GetIndex(eventSender)
			optPolovina(0).Enabled = (Index = 0)
			optPolovina(1).Enabled = (Index = 0)
			tlBarva.Enabled = (Index = 1)
			p�epR�me�ek.Enabled = (Index = 1)
		End If
	End Sub
	
	Private Sub p�epR�me�ek_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles p�epR�me�ek.Click
		'UPGRADE_WARNING: Couldn't resolve default property of object p�epR�me�ek. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
        Vn�j��R�me�ek = CBool(p�epR�me�ek.Hodnota)
	End Sub
	
	Private Sub tlBarva_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlBarva.Click
		'UPGRADE_ISSUE: Constant cdlCCRGBInit was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="55B59875-9A95-4B71-9D6A-7C294BF7139D"'
		'UPGRADE_ISSUE: MSComDlg.CommonDialog property CD.Flags was not upgraded. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="CC4C7EC0-C903-48FC-ACCC-81861D12DA4A"'
        'CD.Flags = MSComDlg.ColorConstants.cdlCCRGBInit
		'UPGRADE_WARNING: MSComDlg.CommonDialog property CD.Flags was upgraded to CDColor.FullOpen which has a new behavior. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E"'
		CDColor.FullOpen = True
		CDColor.Color = System.Drawing.ColorTranslator.FromOle(BarvaR�me�ku)
		CDColor.ShowDialog()
		BarvaR�me�ku = System.Drawing.ColorTranslator.ToOle(CDColor.Color)
	End Sub
	
	Private Sub tlOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlOK.Click
		Me.Hide()
	End Sub
	
	Private Sub tlPry�_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlPry�.Click
		'UPGRADE_WARNING: Couldn't resolve default property of object V�host. Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		V�host = True
		Me.Hide()
	End Sub
End Class