Option Strict Off
Option Explicit On
Friend Class frmV�b�r
    Inherits System.Windows.Forms.Form
    Public Loaded As Boolean = False

    Private Sub frmV�b�r_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
        'Me.Icon = frmFrakt�ln�k.IL.Images.Item(2)
        '�ouKrok.Max = MaxInt
        '�ouKrok.Value = opBarvy.Po�et
        '�ouPo�et.Value = opBarvy.Po�et
        chbOkraj.CheckState = System.Windows.Forms.CheckState.Checked
        CDOpen.InitialDirectory = My.Application.Info.DirectoryPath & "\�k�ly"
        CDSave.InitialDirectory = My.Application.Info.DirectoryPath & "\�k�ly"
        sezV�b�r.Items.Add("Z = X * Y * 5000")
        sezV�b�r.Items.Add("Z = Y * (Sin(X) + 2)")
        sezV�b�r.Items.Add("Z = (X ^ 2 + Y ^ 2)")
        sezV�b�r.Items.Add("Z = (X ^ 2 * Y ^ 2) / 1500")
        sezV�b�r.Items.Add("Z = X ^ (1 / 3) * Y ^ (1 / 3) * 5000")
        sezV�b�r.Items.Add("Z = (X ^ (1 / 3) + Y ^ (1 / 3)) * 5000")
        sezV�b�r.Items.Add("Zrcadlen�")
        sezV�b�r.Items.Add("Z = X Or Y")
        sezV�b�r.Items.Add("Z = X ^ 3 Or Y ^ 2")
        sezV�b�r.Items.Add("Z = X ^ (Y / 175)")
        sezV�b�r.Items.Add("Z = (X ^ 4 - Y ^ 4) / 5000")
        sezV�b�r.Items.Add("Z = X ^ 3 - Y ^ 3")
        sezV�b�r.Items.Add("Z = X ^ 2 - Y ^ 2")
        sezV�b�r.Items.Add("Z = X ^ 2 And Y ^ 4")
        sezV�b�r.Items.Add("Z = X ^ 2")
        sezV�b�r.Items.Add("Z = (X Mod Y) * 1000")
        sezV�b�r.Items.Add("Flanel")
        sezV�b�r.Items.Add("Voda")
        sezV�b�r.Items.Add("Malt�zsk� k��")
        sezV�b�r.Items.Add("Zelen� malt�zsk� k��")
        sezV�b�r.Items.Add("Barevn� malt�zsk� k��")
        sezV�b�r.Items.Add("Bouli�ky")
        sezV�b�r.Items.Add("Pavoukovy o�i")
        sezV�b�r.Items.Add("Kyti�ka")
        sezV�b�r.Items.Add("Trubky")
        sezV�b�r.Items.Add("Lamely")
        sezV�b�r.Items.Add("Lamely vlnov�")
        sezV�b�r.Items.Add("Lamely prou�kovan�")
        sezV�b�r.Items.Add("Duha")
        sezV�b�r.Items.Add("Prou�ky")
        sezV�b�r.Items.Add("Modulo magnum")
        sezV�b�r.Items.Add("Sklen�n� kapky")
        sezV�b�r.Items.Add("Oko 1")
        sezV�b�r.Items.Add("Oko 2")
        sezV�b�r.Items.Add("P�loko")
        sezV�b�r.Items.Add("Kulat� �tverec")
        sezV�b�r.Items.Add("Z�vitky 1")
        sezV�b�r.Items.Add("Z�vitky 2")
        sezV�b�r.Items.Add("Hv�zdice")
        sezV�b�r.Items.Add("Horizonty")
        sezV�b�r.Items.Add("Horizonty zv�t�en�")
        sezV�b�r.Items.Add("Ma�le prou�kovan�")
        sezV�b�r.Items.Add("Ma�le 1")
        sezV�b�r.Items.Add("Ma�le 2")
        sezV�b�r.Items.Add("P�v 1")
        sezV�b�r.Items.Add("P�v 2")
        sezV�b�r.Items.Add("Zrcadlen� plynul�")
        sezV�b�r.Items.Add("Zrcadlen� plynul� posunut�")
        sezV�b�r.Items.Add("Sloupy pevn�")
        sezV�b�r.Items.Add("Dvojparabola")
        sezV�b�r.Items.Add("Dvojparabola kropenat�")
        sezV�b�r.Items.Add("Dvojparabola velekropenat�")
        sezV�b�r.Items.Add("Logaritmus")
        sezV�b�r.Items.Add("Dvojparabola a logaritmus")
        sezV�b�r.Items.Add("Zrcadlen� kropenat� 1")
        sezV�b�r.Items.Add("Zrcadlen� kropenat� 2")
        sezV�b�r.Items.Add("Hyperparabola")
        sezV�b�r.Items.Add("Parahyperbola 1")
        sezV�b�r.Items.Add("Parahyperbola 2")
        sezV�b�r.Items.Add("Parahyperbola prokl�dan�")
        sezV�b�r.Items.Add("Xsinparabola")
        sezV�b�r.Items.Add("Ysinparabola")
        sezV�b�r.Items.Add("Vlna")
        sezV�b�r.Items.Add("N�hrdeln�k")
        sezV�b�r.Items.Add("Hrne�ek")
        sezV�b�r.Items.Add("Ob�lka")
        sezV�b�r.Items.Add("Ob�lka centrovan�")
        sezV�b�r.Items.Add("XsinYsin")
        sezV�b�r.Items.Add("Amoeba 1")
        sezV�b�r.Items.Add("Amoeba 2")
        sezV�b�r.Items.Add("Ubrus 1")
        sezV�b�r.Items.Add("Ubrus 2")
        sezV�b�r.Items.Add("Ubrus zak�iven�")
        sezV�b�r.Items.Add("Epidemia")
        sezV�b�r.Items.Add("Epidemia mnoho�etn�")
        sezV�b�r.Items.Add("Epidemia plynul� 1")
        sezV�b�r.Items.Add("Epidemia plynul� 2")
        sezV�b�r.Items.Add("Epidemia nepravideln�")
        sezV�b�r.Items.Add("Epidemia nepravideln� paprs�it�")
        sezV�b�r.Items.Add("Linie")
        sezV�b�r.Items.Add("Ach�t")
        sezV�b�r.Items.Add("Ach�t paprs�it�")
        sezV�b�r.Items.Add("Slunce v chrp�")
        sezV�b�r.Items.Add("M�lochlupatec")
        sezV�b�r.Items.Add("Dlouhochlupatec")
        sezV�b�r.Items.Add("Dlouchochlupatec ��lov�")
        sezV�b�r.Items.Add("Dlouhochlupatec jednosm�rn�")
        sezV�b�r.Items.Add("Dlouhochlupatec jednosm�rn� obalen�")
        sezV�b�r.Items.Add("Vzep�t�")
        sezV�b�r.Items.Add("Prvn� polofrakt�l")
        sezV�b�r.Items.Add("Druh� polofrakt�l")
        sezV�b�r.Items.Add("T�et� polofrakt�l")
        sezV�b�r.Items.Add("�tvrt� polofrakt�l")
        sezV�b�r.Items.Add("P�t� polofrakt�l")
        sezV�b�r.Items.Add("Mandelbrot")
        sezV�b�r.Items.Add("Mandelbrot od��tan�")
        sezV�b�r.Items.Add("Julia")
        sezV�b�r.Items.Add("Julia od��tan�")
        sezV�b�r.Items.Add("Julia kubick�")
        sezV�b�r.Items.Add("Vlo�ka")
        sezV�b�r.Items.Add("Vlo�ka oto�en�")
        sezV�b�r.Items.Add("Strome�ek")
        sezV�b�r.Items.Add("Strome�ek jednosm�rn�")
        sezV�b�r.Items.Add("Strome�ek s chybou")
        sezV�b�r.Items.Add("Pokus")

        Loaded = True
    End Sub

    Private Sub frmV�b�r_FormClosing(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim Cancel As Boolean = eventArgs.Cancel
        Dim UnloadMode As System.Windows.Forms.CloseReason = eventArgs.CloseReason
        If UnloadMode = System.Windows.Forms.CloseReason.UserClosing Then
            Hide()
            Cancel = True
        End If
        eventArgs.Cancel = Cancel
    End Sub

    Private Sub Text1_Change()

    End Sub

    Private Sub fra�k�la_DblClick()
        Dim I As Short
        For I = 0 To opBarvy.Po�et - 1
            Debug.Print("&H" & Hex(opBarvy.Barva(I)))
        Next
    End Sub

    Private Sub sezV�b�r_DoubleClick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles sezV�b�r.DoubleClick
        tlOK_Click(tlOK, New System.EventArgs())
    End Sub

    Private Sub tlN�hodn�k�la_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlN�hodn�k�la.Click
        Dim Barv�k As New GenerN
        Dim I As Integer
        Barv�k.Largest = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White)
        For I = 0 To opBarvy.Po�et - 1
            opBarvy.Barva(I) = Barv�k.Value
        Next
    End Sub

    Private Sub tlN�hodn�k�laSB�lou_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlN�hodn�k�laSB�lou.Click
        Dim Barv�k As New GenerN
        Dim I As Integer
        Barv�k.Largest = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White)
        opBarvy.Barva(0) = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White)
        For I = 1 To opBarvy.Po�et - 1
            opBarvy.Barva(I) = Barv�k.Value
        Next
    End Sub

    Private Sub tlOK_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlOK.Click
        P�eru�it = False
        frmFrakt�ln�k.Po��tej()
    End Sub

    Private Sub tlOtev��t�k�lu_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlOtev��t�k�lu.Click
        CDOpen.Title = "Otev��t �k�lu"
        CDSave.Title = "Otev��t �k�lu"
        CDOpen.ShowReadOnly = False
        On Error GoTo vypadni
        CDOpen.ShowDialog()
        CDSave.FileName = CDOpen.FileName
        On Error GoTo 0
        Otev�i�k�lu((CDOpen.FileName))
vypadni:
    End Sub

    Sub Otev�i�k�lu(ByRef Soubor As String)
        Dim I, Po�et, pamKrok As Short
        Dim Barva As Integer
        FileOpen(1, Soubor, OpenMode.Binary)
        FileGet(1, Po�et)
        �ouPo�et.Hodnota = Po�et
        FileGet(1, pamKrok)
        Krok = IIf(pamKrok = 0, 1, pamKrok)
        For I = 0 To Po�et - 1
            FileGet(1, Barva)
            opBarvy.Barva(I) = Barva
        Next
        FileClose()
    End Sub

    Private Sub tlUlo�it�k�lu_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlUlo�it�k�lu.Click
        CDOpen.Title = "Ulo�it �k�lu"
        CDSave.Title = CDOpen.Title
        CDOpen.ShowReadOnly = False
        Dim V�sl As DialogResult
        V�sl = CDSave.ShowDialog()
        If V�sl = Windows.Forms.DialogResult.OK Then
            CDOpen.FileName = CDSave.FileName
            Ulo��k�lu(CDOpen.FileName)
        End If
    End Sub

    Sub Ulo��k�lu(ByRef Soubor As String)
        Dim Po�et, I As Short
        Dim Barva As Integer ', Krok As Integer
        Po�et = opBarvy.Po�et
        FileOpen(1, Soubor, OpenMode.Output)
        FileClose()
        FileOpen(1, Soubor, OpenMode.Binary)
        FilePut(1, Po�et)
        FilePut(1, Krok)
        For I = 0 To Po�et - 1
            Barva = opBarvy.Barva(I)
            FilePut(1, Barva)
        Next
        FileClose()
    End Sub

    Private Sub tlP�eru�it_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlP�eru�it.Click
        P�eru�it = True
    End Sub

    Private Sub tlZav��t_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tlZav��t.Click
        'Hide
        Me.Close()
    End Sub

    Private Sub txtKrok_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs)
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        Dim Hod As Object
        If KeyAscii = 13 Then
            KeyAscii = 0
            Hod = �ouKrok.Hodnota
            Hod = IIf(Hod > �ouKrok.Max, �ouKrok.Max, Hod)
            Hod = IIf(Hod < �ouKrok.Min, �ouKrok.Min, Hod)
            �ouKrok.Hodnota = Hod
        End If
        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    Private Sub txtPo�et_KeyPress(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.KeyPressEventArgs)
        Dim KeyAscii As Short = Asc(eventArgs.KeyChar)
        Dim Hod As Object
        If KeyAscii = 13 Then
            KeyAscii = 0
            Hod = �ouPo�et.Hodnota
            Hod = IIf(Hod > �ouPo�et.Max, �ouPo�et.Max, Hod)
            Hod = IIf(Hod < �ouPo�et.Min, �ouPo�et.Min, Hod)
            �ouPo�et.Hodnota = Hod
            'txtPo�et.Text = CStr(�ouPo�et.Hodnota)
        End If
        eventArgs.KeyChar = Chr(KeyAscii)
        If KeyAscii = 0 Then
            eventArgs.Handled = True
        End If
    End Sub

    Private Sub �ouPo�et_Change(ByVal Zdroj As �oup�, ByVal e As EventArgs) Handles �ouPo�et.Zm�na
        opBarvy.Po�et = Zdroj.Hodnota
        If Loaded Then Ulo�Nastaven�("Po�etBarev", Zdroj.Hodnota)
    End Sub


    Public Property Krok() As Short
        Get
            Krok = �ouKrok.Hodnota
        End Get
        Set(ByVal Value As Short)
            �ouKrok.Hodnota = Value
        End Set
    End Property

    Private Sub �ouKrok_Zm�na(ByVal Zdroj As �oup�, ByVal e As System.EventArgs) Handles �ouKrok.Zm�na
        If Loaded Then Ulo�Nastaven�("Krok", Zdroj.Hodnota)
    End Sub

    Private Sub chbKrok_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbKrok.CheckedChanged
        If Loaded Then Ulo�Nastaven�("Pou��tKrok", chbKrok.Checked)
    End Sub

    Private Sub chbPou��t�k�lu_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chbPou��t�k�lu.CheckedChanged
        If Loaded Then Ulo�Nastaven�("Pou��t�k�lu", chbPou��t�k�lu.Checked)
    End Sub

    Private Sub chbVyst�edit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chbVyst�edit.CheckedChanged
        If Loaded Then Ulo�Nastaven�("Vyst�edit", chbVyst�edit.Checked)
    End Sub
End Class