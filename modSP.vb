Option Strict Off
Option Explicit On
Module modSP
	'Sd�len� polo�ky
	
	Public Const MaxInt As Short = 32767
	Public Const P� As Double = 3.04159265358979
	Public P�eru�it As Boolean

    Public Mapa(,) As VlTrp
	
	Public �ik As New �ikTrpasl�k�
	
    Public PoslID As Integer

    Structure VlTrp
        '    ID As Long
        Dim X As Short
        Dim Y As Short
        Dim Hodnota As Double
        Dim Sm As Byte
        Dim SmEp As Byte
        Dim P�vSmEp As Byte
        Dim IndVeVr As Integer
        Dim Vrstva As Integer
        Dim ID As Double
        Dim Nakreslen As Boolean
    End Structure
	
	Structure VlEp
        Dim Mutace As Short
        Dim Soused� As TypSoused�
        Dim Po�ad�Sous As Po�ad�Soused�
		Dim Po�etSous As Object
        Dim SmRozptyl As TSmRozptyl
        Dim PovolitP�ekr�v�n� As Boolean
        Dim Po�Oh As Object
        Dim HodOh As Object
        Dim N�hHodOh As Boolean
        Dim Po�etVrstev As Integer
        Dim Vy�istit As Boolean
        Dim Dla�dice As Boolean
        'Dim VyhnoutSeNakreslen�m As Boolean
        Dim Sm�rov� As Boolean
        Dim Sm�ry As Object
        Dim Linia As Boolean
        Dim LinX As Object
        Dim LinY As Object
        Dim P�idatR�m As Boolean
        Dim VracetJenPoslVr As Boolean
        Dim Nav�zatNaMinulou As Boolean
    End Structure

    Structure XY
        Dim X As Short
        Dim Y As Short
    End Structure

    Structure XYZ
        Dim X As Short
        Dim Y As Short
        Dim Z As Object
    End Structure

    Enum TypSpektra
        spSpojit�
        spZrcadlov�
    End Enum

    'Public Enum TN�strSm�r
    '    smNikam = 0
    '    smDoprava = 1
    '    smDol� = 2
    '    smDoleva = 3
    '    smNahoru = 4
    'End Enum

    Public Platn�N�strSm�ry As TOsmism�r() = { _
        TOsmism�r.V, TOsmism�r.J, TOsmism�r.Z, TOsmism�r.S _
    }

    'Public Enum TN�rSm�r
    '    smNikam = 0
    '    smSV = 1
    '    smJV = 2
    '    smJZ = 3
    '    smSZ = 4
    'End Enum

    Public Platn�N�rSm�ry As TOsmism�r() = { _
        TOsmism�r.SV, TOsmism�r.JV, TOsmism�r.JZ, TOsmism�r.SZ _
    }

    Public Enum TOsmism�r
        SV = 1
        V = 2
        JV = 3
        J = 4
        JZ = 5
        Z = 6
        SZ = 7
        S = 8
    End Enum

    Public Enum TSmRozptyl
        sr0 = 1
        sr90 = 3
        sr180 = 5
        sr270 = 7
        sr360 = 8
        srP�v0 = -1
        srP�v90 = -3
        srP�v180 = -5
        srP�v270 = -7
        srP�v360 = -8
    End Enum

    Enum TypP�enosu
        tpN�hodn�Zm�na
        tpBezeZm�ny
        tpP��r�stek
        tpV�t��P��r�stek
        tpRostouc�P��r�stek
    End Enum

    Public Enum Slo�kyBarvy
        sb�erven�
        sbZelen�
        sbModr�
    End Enum

    Enum Po�ad�Soused�
        psPravideln�
        psN�hodn�
    End Enum

    Enum TypSoused�
        tsN�strann�
        tsN�ro�n�
        tsN�hodn�N�strann��iN�ro�n�
        tsSp�eN�strann�
        tsV�ichni
    End Enum

    Public Enum V�b�rSoused�
        vsV�ichni = 0
        vsN�hodn� = Nothing
        vs1 = 1
        vs2 = 2
        vs3 = 3
        vs4 = 4
        vs5 = 5
        vs6 = 6
        vs7 = 7
        vs8 = 8
    End Enum

    Public Function Nov�ID() As Object
        Nov�ID = PoslID + 1
        PoslID = Nov�ID
    End Function

    Public Function CXY(ByVal X As Short, ByVal Y As Short) As XY
        CXY.X = X
        CXY.Y = Y
    End Function

    Public Sm�r() As XY = New XY() { _
        CXY(0, 0), _
        CXY(1, -1), _
        CXY(1, 0), _
        CXY(1, 1), _
        CXY(0, 1), _
        CXY(-1, 1), _
        CXY(-1, 0), _
        CXY(-1, -1), _
        CXY(0, -1) _
    }

    Public ReadOnly Property XSlN�str(ByVal Sm�r As TOsmism�r) As Integer
        Get
            Select Case Sm�r
                Case TOsmism�r.Z
                    XSlN�str = -1
                Case TOsmism�r.V
                    XSlN�str = 1
                Case Else
                    XSlN�str = 0
            End Select
        End Get
    End Property

    Public ReadOnly Property YSlN�str(ByVal Sm�r As TOsmism�r) As Integer
        Get
            Select Case Sm�r
                Case TOsmism�r.S
                    YSlN�str = -1
                Case TOsmism�r.J
                    YSlN�str = 1
                Case Else
                    YSlN�str = 0
            End Select
        End Get
    End Property

    ' slo�ka n�ro�n�ho sm�ru
    Public ReadOnly Property XSlN�r(ByVal Sm�r As TOsmism�r) As Integer
        Get
            Select Case Sm�r
                Case TOsmism�r.SV, TOsmism�r.JV
                    XSlN�r = 1
                Case TOsmism�r.JZ, TOsmism�r.SZ
                    XSlN�r = -1
                Case Else
                    XSlN�r = 0
            End Select
        End Get
    End Property

    Public ReadOnly Property YSlN�r(ByVal Sm�r As TOsmism�r) As Integer
        Get
            Select Case Sm�r
                Case TOsmism�r.SV, TOsmism�r.SZ
                    YSlN�r = -1
                Case TOsmism�r.JV, TOsmism�r.JZ
                    YSlN�r = 1
                Case Else
                    YSlN�r = 0
            End Select
        End Get
    End Property

    ' slo�ka osmism�ru
    Public ReadOnly Property XSlOsm(ByVal Sm�r As TOsmism�r) As Short
        Get
            Return modSP.Sm�r(Sm�r).X
        End Get
    End Property

    Public ReadOnly Property YSlOsm(ByVal Sm�r As TOsmism�r) As Short
        Get
            Return modSP.Sm�r(Sm�r).Y
        End Get
    End Property

    Public ReadOnly Property �Sm(ByVal Sm As TOsmism�r) As TOsmism�r
        Get ' charakteristick� ��slo sm�ru
            Const Po�Sm As Short = 8
            �Sm = (Sm Mod Po�Sm + Po�Sm) Mod Po�Sm
            If �Sm = 0 Then �Sm = Po�Sm
        End Get
    End Property

    Public ReadOnly Property Po�etSousPodleTypu(ByVal Jac�Sous As TypSoused�) As Short
        Get
            Po�etSousPodleTypu = IIf( _
                Jac�Sous = TypSoused�.tsV�ichni, 8, 4)
        End Get
    End Property


    Public Property Stav() As String
        Get
            Stav = frmFrakt�ln�k.sbrStav.Text
        End Get
        Set(ByVal Value As String)
            frmFrakt�ln�k.sbrStav.Text = Value
            frmFrakt�ln�k.sbrStav.Visible = Not (Value = "")
            frmFrakt�ln�k.pbKolikJe�t�.Top = _
                frmFrakt�ln�k.sbrStav.Top - _
                frmFrakt�ln�k.pbKolikJe�t�.Height
            frmFrakt�ln�k.pbKolikJe�t�.Visible = Not (Value = "")
        End Set
    End Property

    Public ReadOnly Property DejNastaven�(ByVal Kl�� As String, Optional ByVal V�choz� As Object = Nothing) As Object
        Get
            DejNastaven� = GetSetting(My.Application.Info.AssemblyName, "Nastaven�", Kl��, V�choz�)
        End Get
    End Property


    Public Property Uka�V�b�r() As Boolean
        Get
            Uka�V�b�r = (frmV�b�r.WindowState = System.Windows.Forms.FormWindowState.Normal And frmV�b�r.Visible = True)
        End Get
        Set(ByVal Value As Boolean)
            If frmV�b�r.Visible Then frmV�b�r.Hide()
            frmV�b�r.Show(frmFrakt�ln�k)
            If Value Then
                frmV�b�r.WindowState = System.Windows.Forms.FormWindowState.Normal
            Else
                frmV�b�r.WindowState = System.Windows.Forms.FormWindowState.Minimized
                frmFrakt�ln�k.Show()
            End If
        End Set
    End Property


    Public Property Uka�Epidemii() As Boolean
        Get
            Uka�Epidemii = (frmEpidemia.WindowState = System.Windows.Forms.FormWindowState.Normal And frmEpidemia.Visible = True)
        End Get
        Set(ByVal Value As Boolean)
            If frmEpidemia.Visible Then frmEpidemia.Hide()
            frmEpidemia.Show(frmFrakt�ln�k)
            If Value Then
                frmEpidemia.WindowState = _
                    System.Windows.Forms.FormWindowState.Normal
            Else
                frmEpidemia.WindowState = _
                    System.Windows.Forms.FormWindowState.Minimized
                frmFrakt�ln�k.Show()
            End If
        End Set
    End Property

    Public Property Vyst�edit() As Boolean
        Get
            Vyst�edit = frmV�b�r.chbVyst�edit.Checked
        End Get
        Set(ByVal Value As Boolean)
            frmV�b�r.chbVyst�edit.Checked = Value
            Ulo�Nastaven�("Vyst�edit", CStr(Vyst�edit))
        End Set
    End Property


    Public Property �istit() As Boolean
        Get
            �istit = frmV�b�r.chbVy�istit.Checked
        End Get
        Set(ByVal Value As Boolean)
            frmV�b�r.chbVy�istit.Checked = Value
            Ulo�Nastaven�("�istit", CStr(�istit))
        End Set
    End Property


    Public Property Pou��tKrok() As Boolean
        Get
            Pou��tKrok = frmV�b�r.chbKrok.Checked
        End Get
        Set(ByVal Value As Boolean)
            frmV�b�r.chbKrok.Checked = Value
            Ulo�Nastaven�("Pou��tKrok", CStr(Pou��tKrok))
        End Set
    End Property


    Public Property P�idatOkraj() As Boolean
        Get
            P�idatOkraj = frmV�b�r.chbOkraj.Checked
        End Get
        Set(ByVal Value As Boolean)
            frmV�b�r.chbOkraj.Checked = Value
            Ulo�Nastaven�("P�idatOkraj", CStr(P�idatOkraj))
        End Set
    End Property


    Public Property Pou��t�k�lu() As Boolean
        Get
            Pou��t�k�lu = frmV�b�r.chbPou��t�k�lu.Checked
        End Get
        Set(ByVal Value As Boolean)
            frmV�b�r.chbPou��t�k�lu.Checked = Value
            Ulo�Nastaven�("Pou��t�k�lu", CStr(Pou��t�k�lu))
        End Set
    End Property


    Public Property Spektrum() As TypSpektra
        Get
            If frmV�b�r.opSpektrum(0).Checked Then
                Spektrum = TypSpektra.spSpojit�
            Else
                Spektrum = TypSpektra.spZrcadlov�
            End If
        End Get
        Set(ByVal Value As TypSpektra)
            frmV�b�r.opSpektrum(Value).Checked = True
            Ulo�Nastaven�("Spektrum", CStr(Spektrum))
        End Set
    End Property

    Public Property U�ivatelsk�Barvy(ByVal Index As Short) As Integer
        Get
            Return frmV�b�r.opBarvy.Barva(Index:=Index)
        End Get
        Set(ByVal value As Integer)
            frmV�b�r.opBarvy.Barva(Index:=Index) = value
        End Set
    End Property

    Public Property Po�etBarev As Integer
        Get
            Return frmV�b�r.�ouPo�et.Hodnota
        End Get
        Set(ByVal value As Integer)
            frmV�b�r.�ouPo�et.Hodnota = value
            Ulo�Nastaven�("Po�etBarev", value)
        End Set
    End Property


    Public Property Krok() As Short
        Get
            Krok = frmV�b�r.�ouKrok.Hodnota
        End Get
        Set(ByVal Value As Short)
            frmV�b�r.�ouKrok.Hodnota = Value
            Ulo�Nastaven�("Krok", CStr(Krok))
        End Set
    End Property


    Public Property BarvaR�me�ku() As Integer
        Get
            Return DejNastaven�("BarvaR�me�ku", _
                System.Drawing.ColorTranslator.ToOle( _
                frmTisk.tlBarva.BackColor))
        End Get
        Set(ByVal Value As Integer)
            frmTisk.tlBarva.BackColor = _
                System.Drawing.ColorTranslator.FromOle(Value)
            Ulo�Nastaven�("BarvaR�me�ku", CStr(Value))
        End Set
    End Property


    Public Property Vn�j��R�me�ek() As Boolean
        Get
            Vn�j��R�me�ek = DejNastaven�("Vn�j��R�me�ek", _
                                         frmTisk.p�epR�me�ek.Hodnota)
        End Get
        Set(ByVal Value As Boolean)
            frmTisk.p�epR�me�ek.Hodnota = Value
            Ulo�Nastaven�("Vn�j��R�me�ek", CStr(Value))
        End Set
    End Property

    Public ReadOnly Property AnoNe(ByVal Co As Boolean) As String
        Get
            AnoNe = IIf(Co, "ano", "ne")
        End Get
    End Property

    Public ReadOnly Property BoolZAnoNe(ByVal Co As String) As Boolean
        Get
            BoolZAnoNe = IIf(Co = "ne", False, True)
        End Get
    End Property

    Public WriteOnly Property M�stoKreslen�() As XY
        Set(ByVal Value As XY)
            'Dim shp As Shape
            'Static Po��tadlo As Integer
            'Po��tadlo = Po��tadlo + 1
            'shp = frmFrakt�ln�k.shpM�stoKreslen�
            'If Value.X = -1 And Value.Y = -1 Then
            '    shp.Visible = False
            'ElseIf Not shp.Visible Then
            '    shp.Visible = True
            'End If
            'If Po��tadlo Mod 16 = 0 Then
            '    shp.SetBounds(VB6.TwipsToPixelsX(Value.X - VB6.PixelsToTwipsX(shp.Width) \ 2), VB6.TwipsToPixelsY(Value.Y - VB6.PixelsToTwipsY(shp.Height) \ 2), 0, 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y)
            'End If
        End Set
    End Property

    Public Function IndSm(ByRef X As Object, ByRef Y As Object) As Short
        Dim I As Integer
        For I = 1 To 8
            If X = Sm�r(I).X AndAlso Y = Sm�r(I).Y Then
                IndSm = I
                Exit For
            End If
        Next
    End Function

    Public Function JeSm�rVMez�ch(ByVal NSm�r As Short, ByVal P�vSm�r As Short, ByVal Rozptyl As TSmRozptyl) As Boolean
        Dim Po�Sm As Short
        Dim HP�es, DP�es As Boolean 'p�esahy p�es cyklus
        Dim Podm As Boolean
        Dim Do�, Od�, Posuz� As Short
        Dim RozptNaKa�douStr As Short
        Rozptyl = Math.Abs(Rozptyl)
        Select Case Rozptyl
            Case TSmRozptyl.sr0
                RozptNaKa�douStr = 0
            Case TSmRozptyl.sr90
                RozptNaKa�douStr = 1
            Case TSmRozptyl.sr180
                RozptNaKa�douStr = 2
            Case TSmRozptyl.sr270
                RozptNaKa�douStr = 3
            Case TSmRozptyl.sr360
                RozptNaKa�douStr = 4
        End Select
        Po�Sm = 8
        HP�es = (�Sm(P�vSm�r) + RozptNaKa�douStr > Po�Sm)
        DP�es = (�Sm(P�vSm�r) - RozptNaKa�douStr < 1)
        Od� = �Sm(P�vSm�r - RozptNaKa�douStr)
        Do� = �Sm(P�vSm�r + RozptNaKa�douStr)
        Posuz� = �Sm(NSm�r)
        If HP�es Or DP�es Then
            Podm = (Posuz� >= Od�) Or (Posuz� <= Do�)
        Else
            Podm = (Posuz� >= Od�) And (Posuz� <= Do�)
        End If
        JeSm�rVMez�ch = Podm
    End Function


    Public Function �erven�(ByVal Barva As Integer) As Byte
        '�erven� = Barva And &HFFS
        Return ColorTranslator.FromOle(Barva).R
    End Function

    Public Function Zelen�(ByVal Barva As Integer) As Byte
        'Zelen� = (Barva And &HFF00) \ &H100S
        Return ColorTranslator.FromOle(Barva).G
    End Function

    Public Function Modr�(ByVal Barva As Integer) As Byte
        'Modr� = (Barva And &HFF0000) \ &H10000
        Return ColorTranslator.FromOle(Barva).B
    End Function

    Public Function Slo�kaBar(ByVal Slo�ka As Slo�kyBarvy, ByVal Barva As Integer) As Byte
        Const Maska As Short = &H100S
        Dim Maskova� As Integer
        Maskova� = Maska ^ Slo�ka
        Slo�kaBar = CShort(Barva And &HFF * Maskova�) / Maskova�
    End Function


    Public Function ModuloMagnum(ByRef D�lenec As Object, ByRef D�litel As Object) As Object
        Dim Pod�l, Z�klad As Object
        If D�litel = 0 Then
            ModuloMagnum = 0
            Exit Function
        End If
        Pod�l = D�lenec / D�litel
        Pod�l = Fix(Pod�l)
        Z�klad = Fix(Pod�l * D�litel)
        ModuloMagnum = D�lenec - Z�klad
    End Function

    Public Function InCol(ByRef Col As Collection, ByRef Member As Object) As Boolean
        Dim Obj As Object
        InCol = False
        If Col Is Nothing Then Exit Function
        For Each Obj In Col
            If Obj Is Member Then
                InCol = True
                Exit For
            End If
        Next Obj
        '    Debug.Print Col.Count
    End Function

    Public Function VeDru�(ByRef Dru� As Dru�ina, ByRef Trp As TTrpasl�k) As Boolean
        VeDru� = Dru�.Obsahuje(Trp)
    End Function

    Public Function Col(ByVal ParamArray Members() As Object) As Collection
        Dim Obj As Object
        Dim RealMembers As Object()
        If IsArray(Members(0)) AndAlso Members.Length = 1 Then
            ReDim RealMembers(UBound(Members(0)))
            For M As Integer = 0 To UBound(Members(0))
                RealMembers(M) = Members(0)(M)
            Next
        Else
            RealMembers = Members
        End If
        Col = New Collection
        For Each Obj In RealMembers
            If Not Obj Is Nothing Then
                Col.Add(Obj)
            End If
        Next Obj
    End Function

    Public Function Dru�(ByVal ParamArray �lenov�() As Object) As Dru�ina
        Dim Obj As Object
        Dru� = New Dru�ina
        For Each Obj In �lenov�
            If Not Obj Is Nothing Then
                Dru�.P�idej(Obj)
            End If
        Next Obj
    End Function

    Public Function Rozh�zejDru�inu(ByRef Dr As Dru�ina) As Dru�ina
        Dim I As Integer
        Dim N�h As New GenerN
        Dim Obsazeno As Boolean
        N�h.Smallest = 1
        N�h.Largest = Dr.Po�et

        Rozh�zejDru�inu = New Dru�ina

        Dim N�vrh As TTrpasl�k
        For I = 1 To Dr.Po�et
            Do
                N�vrh = Dr(N�h)
                Obsazeno = VeDru�(Rozh�zejDru�inu, N�vrh)
            Loop While Obsazeno
            Rozh�zejDru�inu.P�idej(N�vrh)
        Next
    End Function

    Public Function Vyh�zejZDru�iny(ByRef Dr As Dru�ina, _
      ByRef KolikVyhodit As Object, _
      Optional ByRef VyhoditNakreslen� As Object = Nothing) _
      As Dru�ina
        Dim I As Integer
        Dim KolikNechat As Object
        Dim N�h As New GenerN
        Dim Obsazeno As Boolean
        Dim N�vrh As TTrpasl�k

        If KolikVyhodit = 0 Then
            Vyh�zejZDru�iny = Dr
            Exit Function
        End If

        Vyh�zejZDru�iny = New Dru�ina

        KolikNechat = Dr.Po�et - KolikVyhodit

        If KolikNechat <= 0 Then KolikNechat = 0 '1
        If KolikNechat > Dr.Po�et Then KolikNechat = Dr.Po�et

        N�h.Smallest = 1
        N�h.Largest = Dr.Po�et
        For I = 1 To KolikNechat
            Do
                N�vrh = Dr(N�h)
                Obsazeno = Vyh�zejZDru�iny.Obsahuje(N�vrh)
            Loop While Obsazeno
            If Not (N�vrh Is Nothing) Then
                If Not (N�vrh.Nakreslen And VyhoditNakreslen�) Then
                    Vyh�zejZDru�iny.P�idej(N�vrh)
                End If
            Else
                ' pr�zdn� taky vyhod�me
            End If
        Next
    End Function

    Public Function Vyh�zejPr�zdn�(ByRef Dru� As Dru�ina) As Dru�ina
        Vyh�zejPr�zdn� = New Dru�ina
        Dim I As Integer
        For I = 1 To Dru�.Po�et
            If Not (Dru�(I) Is Nothing) Then
                Vyh�zejPr�zdn�.P�idej(Dru�(I))
            End If
        Next
    End Function

    Public Function VyberSm�ryKSoused�mPodleTypu( _
      ByVal Vstupn�Sm�ry As TOsmism�r(), _
      ByVal Typ As TypSoused�) As TOsmism�r()
        Dim V�sledek As TOsmism�r() = {}
        ' �prava p�echodn�ch typ� soused�
        Select Case Typ
            Case TypSoused�.tsN�hodn�N�strann��iN�ro�n�
                Typ = Dvojn�hoda(TypSoused�.tsN�strann�, _
                                 TypSoused�.tsN�ro�n�, 0.3)
            Case TypSoused�.tsSp�eN�strann�
                Typ = Dvojn�hoda(TypSoused�.tsN�strann�, _
                                 TypSoused�.tsN�ro�n�, 0.7)
        End Select
        ' V�b�r vyhovuj�c�ch sm�r�
        For Each Sm As TOsmism�r In Vstupn�Sm�ry
            Select Case Typ
                Case TypSoused�.tsN�ro�n�
                    If Array.IndexOf(Platn�N�rSm�ry, Sm) > -1 Then
                        ReDim Preserve V�sledek(UBound(V�sledek) + 1)
                        V�sledek(UBound(V�sledek)) = Sm
                    End If
                Case TypSoused�.tsN�strann�
                    If Array.IndexOf(Platn�N�strSm�ry, Sm) > -1 Then
                        ReDim Preserve V�sledek(UBound(V�sledek) + 1)
                        V�sledek(UBound(V�sledek)) = Sm
                    End If
                Case Else ' ponech�me v�echny
                    ReDim Preserve V�sledek(UBound(V�sledek) + 1)
                    V�sledek(UBound(V�sledek)) = Sm
            End Select
        Next
        Return V�sledek
    End Function

    Public Function Se�a�Sm�ryKSoused�mPodlePo�ad�( _
      ByVal Vstupn�Sm�ry As TOsmism�r(), _
      ByVal Po�ad� As Po�ad�Soused�) As TOsmism�r()
        Dim Kol As Collection = modSP.Col(Vstupn�Sm�ry)
        Dim V�sledek As TOsmism�r() = {}
        Dim Index As Integer
        Do Until Kol.Count = 0
            Select Case Po�ad�
                Case Po�ad�Soused�.psPravideln�
                    Dim Nejni���Sm�r As TOsmism�r = 9
                    Dim Sm As TOsmism�r
                    For I As Integer = 1 To Kol.Count
                        Sm = Kol(I)
                        If Sm < Nejni���Sm�r Then
                            Nejni���Sm�r = Sm
                            Index = I
                        End If
                    Next
                Case Po�ad�Soused�.psN�hodn�
                    Dim N�hodn�k As New GenerN
                    N�hodn�k.Smallest = 1
                    N�hodn�k.Largest = Kol.Count
                    Index = N�hodn�k.Value
            End Select
            ReDim Preserve V�sledek(UBound(V�sledek) + 1)
            V�sledek(UBound(V�sledek)) = Kol(Index)
            Kol.Remove(Index)
        Loop
        Return V�sledek
    End Function

    Public Function VyberSm�ryKSoused�mPodleRozptylu( _
      ByVal Vstupn�Sm�ry As TOsmism�r(), _
      ByVal P�vodn�Sm�r As TOsmism�r, _
      ByVal Rozptyl As TSmRozptyl) As TOsmism�r()
        Dim V�sledek As TOsmism�r() = {}
        For Each Sm As TOsmism�r In Vstupn�Sm�ry
            If JeSm�rVMez�ch(Sm, P�vodn�Sm�r, Rozptyl) Then
                ReDim Preserve V�sledek(UBound(V�sledek) + 1)
                V�sledek(UBound(V�sledek)) = Sm
            End If
        Next
        Return V�sledek
    End Function

    ' Vstupn� sm�ry by m�ly b�t u� prom�chan�
    Public Function VyberSm�ryKSoused�mPodlePo�tu( _
      ByVal Vstupn�Sm�ry As TOsmism�r(), _
      ByVal Po�et As Short) As TOsmism�r()
        Dim V�slPo�et As Short
        Dim N�hodn�k As New GenerN
        Dim V�sledek As TOsmism�r() = {}

        If Po�et > 8 Then Po�et = 8
        If Po�et < -8 Then Po�et = -8

        If Po�et = 0 Then
            N�hodn�k.Smallest = 1
            N�hodn�k.Largest = 8
            V�slPo�et = N�hodn�k.Value
        ElseIf Po�et < 0 Then
            N�hodn�k.Smallest = 1
            N�hodn�k.Largest = Math.Abs(Po�et)
            V�slPo�et = N�hodn�k.Value
        Else
            V�slPo�et = Po�et
        End If
        ' Sraz�me v�sledn� po�et na po�et vstupn�ch sm�r�
        V�slPo�et = Men��(V�slPo�et, Vstupn�Sm�ry.Length)
        ReDim V�sledek(V�slPo�et - 1)
        For I As Short = LBound(V�sledek) To UBound(V�sledek)
            V�sledek(I) = Vstupn�Sm�ry(I)
        Next
        Return V�sledek
    End Function

    Public V�echnyOsmism�ry As TOsmism�r() = { _
        TOsmism�r.SV, _
        TOsmism�r.V, _
        TOsmism�r.JV, _
        TOsmism�r.J, _
        TOsmism�r.JZ, _
        TOsmism�r.Z, _
        TOsmism�r.SZ, _
        TOsmism�r.S _
    }


    Function RadZeStup(ByVal Stup As Object) As Object
        RadZeStup = Stup * P� / 180
    End Function

    Function N�hoda(ByVal ParamArray Hodnoty() As Object) As Object
        Dim St�elec As New GenerN
        St�elec.Smallest = 0
        St�elec.Largest = UBound(Hodnoty)
        N�hoda = Hodnoty(St�elec.Value)
    End Function

    Function Dvojn�hoda(ByRef Hod1 As Object, _
                        ByRef Hod2 As Object, _
                        ByRef Pravd�podobnost As Object) As Object
        Dim St�elec As New GenerN
        Dim St�ela As Object
        St�elec.Largest = 100
        St�ela = St�elec.Value
        If St�ela <= Pravd�podobnost * 100 Then
            Dvojn�hoda = Hod1
        Else
            Dvojn�hoda = Hod2
        End If
    End Function

    Function Rozptyl(ByRef Hod As Object, ByRef PlusMinus As Object) As Object
        Dim St�elec As New GenerN
        St�elec.Smallest = -PlusMinus
        St�elec.Largest = PlusMinus
        Rozptyl = Hod + St�elec.Value
    End Function

    Function Men��(ByRef Hod1 As Object, ByRef Hod2 As Object) _
      As Object
        Men�� = IIf(Hod1 < Hod2, Hod1, Hod2)
    End Function

    Function V�t��(ByRef Hod1 As Object, ByRef Hod2 As Object) _
      As Object
        V�t�� = IIf(Hod1 > Hod2, Hod1, Hod2)
    End Function

    Public Sub Ulo�Nastaven�(ByVal Kl�� As String, _
                             ByVal Hod As String)
        SaveSetting(My.Application.Info.AssemblyName, _
                    "Nastaven�", Kl��, Hod)
    End Sub


    Function Fasis(ByVal Hod As Object, ByVal Krok As Object, _
                   ByVal ParamArray Barvy() As Object) As Object
        Dim ZbV, Za�, Kon, ZbM As Integer
        Dim Po� As Object
        If frmV�b�r.chbKrok.Checked Then Krok = modSP.Krok
        If frmV�b�r.chbPou��t�k�lu.Checked Then
            Po� = frmV�b�r.opBarvy.Po�et
        Else
            Po� = UBound(Barvy) + 1
        End If
        If Spektrum = TypSpektra.spZrcadlov� Then
            Hod = System.Math.Abs(Hod)
        End If
        ZbV = Fix(ModuloMagnum(Hod, Krok * Po�))
        ZbM = ModuloMagnum(Hod, Krok)
        Za� = ZbV \ Krok
        Kon = ZbV \ Krok + 1
        Dim V�slZa�, V�slKon As Integer
        Dim V�slPod�l As Single
        ' pokud je vybr�no spojit� spektrum
        If (Hod < 0) And frmV�b�r.opSpektrum(0).Checked Then
            If Za� < 0 Then
                V�slZa� = Po� - 1 - Math.Abs(Kon)
                V�slKon = Po� - 1 - Math.Abs(Za�)
            Else
                V�slZa� = 0
                V�slKon = Po� - 1 - Math.Abs(Za�)
            End If
        Else ' zrcadlov� spektrum
            If System.Math.Abs(Kon) < Po� Then
                V�slZa� = Math.Abs(Za�)
                V�slKon = Kon
            Else
                V�slZa� = Math.Abs(Za�)
                V�slKon = 0
            End If
        End If
        V�slPod�l = Math.Abs(ZbM / Krok)
        Dim Za�B, KonB As Integer
        ' pou��t p�eddefinovanou �k�lu
        If Not Pou��t�k�lu Then
            Za�B = ColorTranslator.ToOle(Barvy(V�slZa�))
            KonB = ColorTranslator.ToOle(Barvy(V�slKon))
        Else ' pou��t �k�lu z vybran�ch barev v kontrolce
            Za�B = U�ivatelsk�Barvy(V�slZa�)
            KonB = U�ivatelsk�Barvy(V�slKon)
        End If
        Return Grad(Za�B, KonB, V�slPod�l)
    End Function

    ' Fasis comprimata
    Function FasisC(ByVal Hod As Object, ByVal Krok As Object, ByVal ParamArray Barvy() As Object) As Object
        On Error Resume Next
        Dim ZbV, Za�, Kon, ZbM As Integer
        Dim Po�, Pod�l, IntBar As Object 'Interval barvy
        Dim I As Short
        If Pou��tKrok Then Krok = modSP.Krok
        If Pou��t�k�lu Then
            Po� = frmV�b�r.opBarvy.Po�et
        Else
            Po� = Barvy.Length
        End If
        IntBar = Krok / Po�
        If Spektrum = TypSpektra.spZrcadlov� Then
            Hod = System.Math.Abs(Hod)
        End If
        ZbV = Fix(ModuloMagnum(Hod, Krok))
        ZbM = ModuloMagnum(Hod, IntBar)
        Za� = ZbV \ IntBar
        Kon = ZbV \ IntBar + 1
        Pod�l = ZbM / IntBar
        Dim V�slZa�, V�slKon As Integer
        Dim V�slPod�l As Integer
        ' pokud je vybr�no spojit� spektrum a Hod < 0
        If (Hod < 0) And Spektrum = TypSpektra.spSpojit� Then
            If Za� < 0 Then
                V�slZa� = Po� - 1 - Math.Abs(Kon)
                V�slKon = Po� - 1 - Math.Abs(Za�)
            Else
                V�slZa� = 0
                V�slKon = Po� - 1 - Math.Abs(Za�)
            End If
        Else ' pokud je vybr�no zrcadlov� spektrum nebo Hod >= 0
            If System.Math.Abs(Kon) < Po� Then
                V�slZa� = System.Math.Abs(Za�)
                V�slKon = Kon
            Else
                V�slZa� = Math.Abs(Za�)
                V�slKon = 0
            End If
        End If
        V�slPod�l = Math.Abs(Pod�l)
        Dim Za�B, KonB As Integer
        ' pou��t p�eddefinovanou �k�lu
        If Not Pou��t�k�lu Then
            Za�B = ColorTranslator.ToOle(Barvy(V�slZa�))
            KonB = ColorTranslator.ToOle(Barvy(V�slKon))
        Else ' pou��t �k�lu z vybran�ch barev v kontrolce
            Za�B = U�ivatelsk�Barvy(V�slZa�)
            KonB = U�ivatelsk�Barvy(V�slKon)
        End If
        Return Grad(Za�B, KonB, V�slPod�l)
    End Function

    Function Grad(ByVal Za�B As Integer, ByVal KonB As Integer, _
                  ByVal Ratio As Single) As Integer
        Dim �, Z, M As Integer
        Dim �D�l, ZD�l, MD�l As Short
        �D�l = (CShort(�erven�(KonB)) - CShort(�erven�(Za�B)))
        ZD�l = (CShort(Zelen�(KonB)) - CShort(Zelen�(Za�B)))
        MD�l = (CShort(Modr�(KonB)) - CShort(Modr�(Za�B)))
        � = �erven�(Za�B) + Ratio * �D�l
        Z = Zelen�(Za�B) + Ratio * ZD�l
        M = Modr�(Za�B) + Ratio * MD�l
        Return RGB(�, Z, M)
        'Debug.Print Ratio
    End Function

    Function Epidemia(Optional ByRef Ohniska As Dru�ina = Nothing, _
      Optional ByRef Mutace As TypP�enosu = 0, _
      Optional ByRef Soused� As TypSoused� = 0, _
      Optional ByRef Po�ad�Sous As Po�ad�Soused� = _
        Po�ad�Soused�.psN�hodn�, _
      Optional ByVal Po�etSous As Object = Nothing, _
      Optional ByRef SmRozptyl As TSmRozptyl = TSmRozptyl.sr360, _
      Optional ByRef PovolitP�ekr�v�n� As Boolean = False, _
      Optional ByRef Po�Oh As Object = Nothing, _
      Optional ByRef HodOh As Object = Nothing, _
      Optional ByRef N�hHodOh As Boolean = True, _
      Optional ByRef Po�etVrstev As Integer = 0, _
      Optional ByRef Vy�istit As Boolean = True) As Dru�ina

        Dim Epid As New Epidemia

        With Epid
            .Ohniska = Ohniska
            .Mutace = Mutace
            .Soused� = Soused�
            .Po�ad�Sous = Po�ad�Sous
            .Po�etSous = Po�etSous
            '.SmRozptyl = SmRozptyl
            .PovolitP�ekr�v�n� = PovolitP�ekr�v�n�
            .Po�Oh = Po�Oh
            .HodOh = HodOh
            .N�hHodOh = N�hHodOh
            .Po�etVrstev = Po�etVrstev
            .Vy�istit = Vy�istit
        End With

        Epid.Spus�()

        Epidemia = Epid.Naka�en�

    End Function

    Public Function EpidemiaSm�rov�(ByRef Ohniska As Dru�ina, _
      ByRef Mutace As TypP�enosu, _
      ByRef Po�ad�Sous As Po�ad�Soused�, _
      ByRef Po�etSous As Object, _
      ByRef PovolitP�ekr�v�n� As Boolean, _
      ByRef Po�Oh As Object, ByRef HodOh As Object, _
      ByRef N�hHodOh As Boolean, ByRef Po�etVrstev As Integer, _
      ByRef Vy�istit As Boolean, _
      ByVal ParamArray Sm�ry() As Object) As Dru�ina

        Dim Epid As New Epidemia

        With Epid
            .Sm�rov� = True
            .Sm�ry = De�ifrujPoleSm�r�(Sm�ry)
            .Ohniska = Ohniska
            .Mutace = Mutace
            .Po�ad�Sous = Po�ad�Sous
            .Po�etSous = Po�etSous
            .PovolitP�ekr�v�n� = PovolitP�ekr�v�n�
            .Po�Oh = Po�Oh
            .HodOh = HodOh
            .N�hHodOh = N�hHodOh
            .Po�etVrstev = Po�etVrstev
            .Vy�istit = Vy�istit
        End With

        Epid.Spus�()

        Return Epid.Naka�en�

    End Function

    Function Linia( _
      Optional ByRef X As Object = Nothing, _
      Optional ByRef Y As Object = Nothing, _
      Optional ByRef Hodnota As Object = Nothing, _
      Optional ByRef D�lka As Object = Nothing _
    ) As Dru�ina

        Dim Epid As New Epidemia

        With Epid
            .LinX = X
            .LinY = Y
            .HodOh = Hodnota
            .Po�etVrstev = D�lka
            .Linia = True
        End With

        Epid.Spus�()

        Linia = Epid.Naka�en�

    End Function


    Sub P�ipravMapu(Optional ByRef NechatStarou As Boolean = False)
        Static Pov�ce As Boolean
        �ik.Pic = frmFrakt�ln�k.obrFr
        If (Not NechatStarou) Or (Not Pov�ce) Then �ik.Size()
        If Not Pov�ce Then Pov�ce = Not Pov�ce
    End Sub



    Function EpFas(ByRef Z As Double) As Object
        EpFas = Fasis(Z, 100, _
            System.Drawing.ColorTranslator.ToOle( _
                System.Drawing.Color.Blue), _
            System.Drawing.ColorTranslator.ToOle( _
                System.Drawing.Color.Lime), _
            System.Drawing.ColorTranslator.ToOle( _
                System.Drawing.Color.Yellow), _
            System.Drawing.ColorTranslator.ToOle( _
                System.Drawing.Color.White))
    End Function


    Sub Z�loha()
        Dim BMP As New Bitmap(frmFrakt�ln�k.obrFr.ClientSize.Width, frmFrakt�ln�k.ClientSize.Height)
        Dim Gr As Graphics = Graphics.FromImage(BMP)
        Gr.DrawImage(frmFrakt�ln�k.obrFr.Z�sobn�k, New Point(0, 0))
        BMP.Save(My.Application.Info.DirectoryPath & "\Obr�zky\z�loha.png", System.Drawing.Imaging.ImageFormat.Png)
    End Sub



    Public Sub ObnovNastaven�()
        With frmV�b�r
            .chbVy�istit.Checked = DejNastaven�("�istit", �istit)
            .chbKrok.Checked = DejNastaven�("Pou��tKrok", Pou��tKrok)
            .chbOkraj.Checked = DejNastaven�("P�idatOkraj", P�idatOkraj)
            .chbPou��t�k�lu.Checked = DejNastaven�("Pou��t�k�lu", Pou��t�k�lu)
            .chbVyst�edit.Checked = DejNastaven�("Vyst�edit", Vyst�edit)
            '.p�epZmen�it = DejNastaven�("Zmen�it", Zmen�it)
            .opSpektrum(0).Checked = DejNastaven�("Spektrum", Spektrum) = 0
            .opSpektrum(1).Checked = DejNastaven�("Spektrum", Spektrum) = 1
            .�ouKrok.Hodnota = DejNastaven�("Krok", Krok)
            .�ouPo�et.Hodnota = DejNastaven�("Po�etBarev", Po�etBarev)
        End With
        BarvaR�me�ku = BarvaR�me�ku
        Vn�j��R�me�ek = Vn�j��R�me�ek
    End Sub

    Public Function CVlEp(ByRef Ep As Epidemia) As VlEp
        With CVlEp
            .Dla�dice = Ep.Dla�dice
            .HodOh = Ep.HodOh
            .Linia = Ep.Linia
            .LinX = Ep.LinX
            .LinY = Ep.LinY
            .Mutace = Ep.Mutace
            .N�hHodOh = Ep.N�hHodOh
            .Nav�zatNaMinulou = Ep.Nav�zatNaMinulou
            .Po�etSous = Ep.Po�etSous
            .Po�etVrstev = Ep.Po�etVrstev
            .Po�Oh = Ep.Po�Oh
            .Po�ad�Sous = Ep.Po�ad�Sous
            .PovolitP�ekr�v�n� = Ep.PovolitP�ekr�v�n�
            .P�idatR�m = Ep.P�idatR�m
            .Sm�rov� = Ep.Sm�rov�
            .Sm�ry = Ep.Sm�ry
            .SmRozptyl = Ep.SmRozptyl
            .Soused� = Ep.Soused�
            .VracetJenPoslVr = Ep.VracetJenPoslVr
            .Vy�istit = Ep.Vy�istit
            '.VyhnoutSeNakreslen�m = Ep.VyhnoutSeNakreslen�m
        End With
    End Function


    Public Function CEp(ByRef Vl As VlEp) As Epidemia
        CEp = New Epidemia
        With CEp
            .Dla�dice = Vl.Dla�dice
            .HodOh = Vl.HodOh
            .Linia = Vl.Linia
            .LinX = Vl.LinX
            .LinY = Vl.LinY
            .Mutace = Vl.Mutace
            .N�hHodOh = Vl.N�hHodOh
            .Nav�zatNaMinulou = Vl.Nav�zatNaMinulou
            .Po�etSous = Vl.Po�etSous
            .Po�etVrstev = Vl.Po�etVrstev
            .Po�Oh = Vl.Po�Oh
            .Po�ad�Sous = Vl.Po�ad�Sous
            .PovolitP�ekr�v�n� = Vl.PovolitP�ekr�v�n�
            .P�idatR�m = Vl.P�idatR�m
            .Sm�rov� = Vl.Sm�rov�
            .Sm�ry = Vl.Sm�ry
            .SmRozptyl = Vl.SmRozptyl
            .Soused� = Vl.Soused�
            .VracetJenPoslVr = Vl.VracetJenPoslVr
            .Vy�istit = Vl.Vy�istit
            '.VyhnoutSeNakreslen�m = Vl.VyhnoutSeNakreslen�m
        End With
    End Function

    Function De�ifrujPoleSm�r�(Sm�ry As Object()) As TOsmism�r()
        If Sm�ry.Length = 1 AndAlso IsArray(Sm�ry(0)) Then
            Return Sm�ry(0)
        Else
            Dim V�sl As TOsmism�r()
            Dim I As Integer
            ReDim V�sl(UBound(Sm�ry))
            For I = 0 To UBound(Sm�ry)
                V�sl(I) = Sm�ry(I)
            Next
            Return V�sl
        End If
    End Function
End Module